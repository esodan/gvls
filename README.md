![logo](GVls_Logo.png "GVls Logo")
# Introduction
GVls is a Software Development Kit (SDK) for Language Server Protocol (LSP) Specification
implementations.

Not just that, GVls provides a generic library `libgvo` to add serialization to/from
Variant/Json for GObject classes. You don't need to walk across the Variant/Json
tree to access properties you are interested to update your object, `libgvo` works for you.

GVls can used to access any LSP server for any programming language, by
implementing a Generic Client.

GVls has a Vala Language Client and Server implementation, you can use it in your
favorite Editor supporting a Client conneting to the server, or use the native Vala LSP
client.

# SDK for LSP server/client development

GVls provides a generic set of GObject based interfaces and classes to implement
Language Server Protocol Clients and Servers, for any kind of language.

# Documentation

You can browse [documentation](https://esodan.pages.gitlab.gnome.org/gvls) 
generated from latest sources.

# Features

## Variant/JSON GObject serialization

`libgvo` is a Variant/JSON to/from GObject serialization framewoark
with a set of classes and an interface called `GVo.Object` helping
classes to serialize itself to/from `GLib.Variant`, using
class's properties' nickname as the name used when serialized
to JSON files. Vala syntax makes really easy to define
which properties you want to serialize.

Since 21.0 release this [libgvo is located in its own repository](https://gitlab.gnome.org/esodan/libgvo)

## Project Management for Editors and Vala LSP

- [X] Holds project's root directory
- [X] Support Meson Build System initialization
- [X] Initialize an LSP Server as a subprocess
- [X] Connect automatically a Client to a Server
- [X] Configure a Server from Meson's instrospection information

## Gtk Widgets for Vala LSP

- [X] GtkSourceView's custom widget (see issue #140)
- [X] GtkSourceView completion provider (see issue #140)
- [X] ProjectManager: automatic connection to servers
- [X] ProjectManager: catched completion lists for `Gtk.SourceView`
- [X] ProjectManager: configure the server you want to use

### Editors plugins
- [X] GNOME Builder (maintained)
- [x] Microsoft Visual Studio Code (external plugin)
- [ ] GEdit plugin (fix required - issue #117, #140)

## LSP Server for Vala LSP
- [X] Linter diagnostics
- [x] Completion
- [x] Completion Resolve
- [x] Diagnostics
- [x] Workspace Symbol
- [x] Symbol location
- [x] Symbol definition
- [x] Signature Help
- [x] Document's symbols
- [x] Hover
- [x] Document Syncronization: didOpen
- [x] Document Syncronization: didChange
- [x] Document Syncronization: didSave
- [x] Configuration provided by Client on initialization
- [x] Configuration change notification from Client
- [x] Configuration request to Client
- [x] Automatic configuration using [Meson](https://mesonbuild.com/) introspection
- [X] Symbol References
- [ ] Rename
- [ ] Prepare Rename
- [X] Formating (See issue #6]
- [X] Range Formatting (See issue #6]
- [ ] On-type formatting
- [ ] Folding Range
- [ ] Selection Range
- [ ] Code Lens (#8)
- [ ] Code Lens Resolve
- [ ] Code Actions (#135)
- [ ] Code Actions Resolve
- [ ] Document Link (#134)
- [ ] Document Link Resolve
- [ ] Document Color
- [X] Documentation Provider from valadoc.org and from a database from GIR files
- [X] Symbol Database Provider

### Documentation from valadoc.org

By default and if network is available, `GLsv` can query documentation from [valadoc.org](https://valadoc.org/) if you added the correct
package using `--pkg` in your meson build information.

### Documentation from GIR

`GIR` is a format used by `GObject Introspection`, saving meta data including documentation about symbols in a
namespace. If you compile `GLsv` with the option `-Ddb=true` you also will get a possibility to extract documentation
from installed GIR files.

### Symbol Database

Use `-Ddb=true` to enable Database symbol support using SQLite through `VDA`

## LSP Client for Vala LSP
- [x] Completion
- [x] Completion Resolve
- [x] Diagnostics
- [x] Workspace Symbol
- [x] Symbol location
- [x] Symbol definition
- [x] Signature Help
- [x] Document's symbols
- [x] Hover
- [x] Document Syncronization: didOpen
- [x] Document Syncronization: didChange
- [x] Document Syncronization: didSave
- [x] Configuration provided by Client on initialization
- [x] Configuration change notification from Client
- [x] Configuration request to Client
- [x] Automatic configuration using [Meson](https://mesonbuild.com/) introspection
- [x] Symbol References
- [ ] Rename
- [ ] Prepare Rename
- [X] Formating
- [X] Range Formatting
- [ ] On-type formatting
- [ ] Folding Range
- [ ] Selection Range
- [ ] Code Lens (#8)
- [ ] Code Lens Resolve
- [ ] Code Actions (#135)
- [ ] Code Actions Resolve
- [ ] Document Link (#134)
- [ ] Document Link Resolve
- [ ] Document Color


# Vala LSP Server Configuration

Server configuration is a required step in order to provide reliable diagnostics,
because that will help to know all targets, its ources and its compile options.

[Meson](https://mesonbuild.com/) provides `JSON` files with introspection
about all configuration required by the server. There are two options in GVls,
`compile_commands.json` and `meson-info/intro-targets.json`, one or both can be
provided by the client at initialization or configuration request.

Meson's introspection information will be monitored to catch changes, so the server will
automatically configure itself, if the above information is auto-detected
in the project's root folder provided by the client at initialization time.

The client can provide the Meson's introspection information at initialization
time and should provide any configuraiton change to the server. GNOME Builder's
plugin for GVls works this way, because the information could be generated
using flatpak sanboxed development environment.

[Server Configuration Specification](https://wiki.gnome.org/Projects/Vala/LanguageServerSpecifications)
is located at Vala's Wiki.

## Automatic Server Configuration

If the client doesn't support or provide Server configuration, server can detect
if a [Meson Build System](https://mesonbuild.com/) exists and use it to generate
a build dir called by default `gvlsbuild`, to take its configuration from.


# Installation

## Dependencies 

### GObject serialization to JSON

`libvo` requires:

* meson
* ninja
* glib-2.0
* gio-2.0
* gee-0.8

### SDK for LSP Specifiation

`libgvls` requires:

* meson
* ninja
* glib-2.0
* gio-2.0
* gee-0.8
* json-glib-1.0
* jsonrpc-glib-1.0
* [libgvo](https://gitlab.gnome.org/esodan/libgvo) >= 2.0.0.1

If you want to build just `libvo` use `-Dlibgvls=false` option when compiling.

### Vala LSP Client/Server implementation

`libglsv` requires:

* meson
* ninja
* glib-2.0
* gio-2.0
* gee-0.8
* json-glib-1.0
* jsonrpc-glib-1.0
* libsoup-2.4
* libvala, you can select which version to use or is autodetected
* [gxml-0.20](https://gitlab.gnome.org/GNOME/gxml) >= 0.20.1
* [libgvo](https://gitlab.gnome.org/esodan/libgvo) >= 2.0.0.1
* `libgvls`

If you want to build just `libgvls` use `-Dlsp=false` option when compiling,
that will build required library `libvo`.

If you want Dabase support you will need the following dependencies and use
`-Ddb=true`:

* [gcalc-2](https://gitlab.gnome.org/GNOME/gnome-calculator) >= 3.38 from GNOME Calculator
* sqlite3
* [libvda](https://gitlab.gnome.org/esodan/libvda) >= 1.0.8
* [girp](https://gitlab.gnome.org/esodan/girp) >= 1.0.1

### Vala LSP Client/Server implementation for GTK based Editors

`libgvlsui` requires:

* meson
* ninja
* glib-2.0
* gio-2.0
* gee-0.8
* json-glib-1.0
* jsonrpc-glib-1.0
* libvala, you can select which version to use or is autodetected
* gtk+-3.0
* [libgvo](https://gitlab.gnome.org/esodan/libgvo) >= 2.0.0.1
* `libgvls`
* `libglsv`

If you want to build just `libglsv` use `-Dui=false` option when compiling,
that will build required library `libvo` and `libgvls`.

### Gtk Widtgets

`libgvlsui` requires:

* meson
* ninja
* glib-2.0
* gio-2.0
* gee-0.8
* json-glib-1.0
* jsonrpc-glib-1.0
* gtk+-3.0
* libvala, you can select which version to use or is autodetected
* gtk+-3.0
* [libgvo](https://gitlab.gnome.org/esodan/libgvo) >= 2.0.0.1
* `libgvls`
* `libglsv`

### GEdit Plugin:

* [libgvo](https://gitlab.gnome.org/esodan/libgvo) >= 2.0.0.1
* `libgvls`
* `libglsv`
* libpeas-1.0
* gedit

## Compile
For standard compilation and installation using `/usr` prefix, use:

```
meson _build --prefix=/usr
cd _build
ninja
ninja install
```

If you plan to disable documentation generation use `-Ddocs=false` as
an option to `meson` command.

To disable GObject Introspection bindings use `-Dintrospection=false`


If you don't have all dependencies installed, GVLS can get them using Meson's projects, in order to do so force meson configuration to use subprojects for both GVLS and its VDA subproject, running:

```
$ meson -Dsubprojects=true -Dlibvda:subprojects=true -Dgirp:subprojects=true _build
```

In above command GVLS's `subprojects` option is set to true. If VDA is not installed and want to use it as a subproject, above command also enable VDA as a subproject and, if its dependencies are not installed, the command enables VDA's `subprojects` option to true in order to archive that.

## Testing

GVls has unit test enable by default. To enable GUI tests, you should use
`-Dui-test-interactive=false -Dsubprojects=true -Ddisable-ui-tests=false`,
that will build GtkTester as subproject and will run all GUI tests without
stop until finish. If you want to interact with the GUI Tets, you should use
`-Dui-test-interactive=true`.

# Connect to Vala LSP Server

## Using GNOME Builder

GNOME Builder has a native plugin for GVls and use it by default when you open
a Vala source code. Connection is performed by using stdin/stdout server.

## Using Microsoft Visual Studio Code

VSCode requires a native plugin, that connects to GVls server using both TCP/IP
or standard Input/Output, see next sections.

One of the plugins developed for VSCode supporting GVls as a server is the one
from Princeton Ferro (ID: prince781.vala), the problem is that you should not
install [VLS](https://github.com/benwaffle/vala-language-server), because that
plugin use it by default.

## Standard Intput/Output

Standard Input/Output (stdin/stdout) is a very popular method to connect to
LSP servers, GVls provides an executable called `org.gnome.gvls.stdio.Server`,
you can use for. You can have as many as servers you want to initate, because
they are execute in a subprocess to avoid interfare with main IED. Once installed
you should start it as a subprocess:

```vala
var launcher = new SubprocessLauncher (SubprocessFlags.STDIN_PIPE|SubprocessFlags.STDOUT_PIPE);
// Provide a valid PATH to server
string[] sp_args = {"org.gnome.gvls.stdio.Server"};
Subprocess sp = null;
try {
    sp = launcher.spawnv (sp_args);
} catch (GLib.Error e) {
    warning ("Error launching server subprocess: %s", e.message);
}
var sout = sp.get_stdin_pipe (); // Write to
var sin = sp.get_stdout_pipe (); // Read from
// You can use your own client or use the one provided by GVls
var client = new GLsv.ClientStream.from_streams (sin, sout);
```

## TCP/IP connection

You can connect to a server using TCP/IP at port `1024`, using localhost or a
host's address when `org.gnome.gvls.inet.Server` is running.

`GVls.ClientInetLocal` initialize a `localhost` connection to `GVls.ServerInetLocal`
without any extra configuration.

```vala
// On server side
var server = new GLsv.ServerInetLocal ();
server.run ();

// On client side
var client = new GLsv.ClientInetLocal ();
```
`GVls.ServerInetManager` hear for incoming connections from clients and connect
data flow to a `GVls.ServerInet` per client connection request.

```vala
// On server side create a Server Manager, will start a new server per
// incoming connection
var manager = new GLsv.ServerInetManager ();

// On client side start a client and will try to connect to manager
var client = new GLsv.ClientInet ();
```

## Debuging Client/Server requests/responses

When running Builder use `JSONRPC_DEBUG=1 gnome-builder` so you can see all input-output messages from/go cliente and server.
