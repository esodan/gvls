/* glsvui-project-manager.vala
 *
 * Copyright 2021 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GVls;
using Gtk;

/**
 * Project manager to create sourceview
 */
public class GLsvui.ProjectManager : GLib.Object
{
    GLsv.ProjectManager _manager;
    Gee.HashMap<string,GVo.Container> _document_symbols;
    Gee.HashMap<string,GVo.Container> _completions;
    Gee.HashSet<Gtk.SourceView> _views;
    uint timed_id = -1;
    bool pushing = false;
    bool _autoconfigure = true;

    /**
     * Project manager
     */
    public GLsv.ProjectManager manager {
        get { return _manager; }
    }

    construct {
        _document_symbols = new Gee.HashMap<string,GVo.Container> ();
        _completions = new Gee.HashMap<string,GVo.Container> ();
        _views = new Gee.HashSet<Gtk.SourceView> ();
        timed_id = Timeout.add (100, push_document_changes);
    }

    /**
     * Creates a new project manager
     */
    public ProjectManager (GLib.File root, GVls.BuildSystem bs) {
        _manager = new GLsv.ProjectManager (root, bs);
        _manager.server_autoconfigure (true);
    }

    ~ProjectManager () {
        if (timed_id != -1) {
            var source = MainContext.@default ().find_source_by_id (timed_id);
            if (source != null) {
                source.destroy ();
            }
        }
    }


    private bool push_document_changes () {
        if (pushing) {
            return true;
        }

        foreach (Gtk.SourceView v in _views) {
            var changes = v.get_buffer ().get_data<GVo.Container> ("gvls-changes");
            string uri = v.get_buffer ().get_data<string> ("gvls-uri");

            if (changes == null) {
                continue;
            }

            if (changes.get_n_items () == 0) {
                continue;
            }

            pushing = true;
            GVo.Container current = changes;
            var n = new GVo.ContainerHashList.for_type (typeof (TextDocumentContentChangeEventInfo));
            v.get_buffer ().set_data ("gvls-changes", n);

            manager.client.document_change.begin (uri, current, (obj, res)=>{
                try {
                    manager.client.document_change.end (res);
                } catch (GLib.Error e) {
                    warning ("Error while pushing changes to the server: %s", e.message);
                }
            });
        }

        pushing = false;

        return true;
    }

    /**
     * Creates a new project manager using Meson Build System
     */
    public ProjectManager.for_meson (GLib.File root) {
        GVls.BuildSystem bs = new GLsv.BuildSystemMesonVala (root);
        _manager = new GLsv.ProjectManager (root, bs);
    }

    /**
     * Attach an {@link GLsvui.CompletionProvider}
     * with a given {@link Gtk.SourceView}, used to
     * show the contents of the given file
     */
    public void set_completion_provider (Gtk.SourceView view, GLib.File file) {
        GLsvui.SourceViewInitialize.initialize (view, this, file);
        _views.add (view);
    }

    /**
     * Creates a new {@link GLsvui.SourceView}
     * filling it with the contents of given file
     */
    public Gtk.SourceView create_source_view (GLib.File file) {
        var sv = new GLsvui.SourceView (this);
        sv.open_file.begin (file);
        return sv;
    }

    /**
     * Takes a given {@link Gtk.SourceView}
     * fill it with the contents of given file
     * and sync the server buffers
     */
    public void open (Gtk.SourceView view, GLib.File file) {
        if (view is GLsvui.SourceView) {
            ((GLsvui.SourceView) view).open_file.begin (file);
        } else {
            open_document (view);
        }
    }

    /**
     * Takes a given {@link Gtk.SourceView}'s buffer text
     * and use it as a file contents already loaded to
     * sync the server buffers.
     *
     * Make sure you have called {@link GLsvui.ProjectManager.set_completion_provider}
     * on given {@link Gtk.SourceView} widget and that the contents
     * of the associated file has been already loaded in the buffer
     */
    public void open_document (Gtk.SourceView view) {
        if (manager.client == null) {
            debug ("No Client was set");
            return;
        }
        var buf = view.get_buffer ();
        string uri = buf.get_data<string> ("gvls-uri");
        if (uri == null) {
            return;
        }
        GLsvui.ProjectManager manager = buf.get_data<GLsvui.ProjectManager> ("gvls-manager");
        if (manager == null) {
            warning ("No manager was set to source view");
            return;
        }

        manager.manager.client.document_open.begin (uri, buf.text, (obj, res)=> {
            try {
                manager.manager.client.document_open.end (res);
            } catch (GLib.Error e) {
                warning (_("Error while opening file: %s: %s"), uri, e.message);
            }
        });
    }
    /**
     * Find stored document symbols for given uri
     */
    public GVo.Container get_document_symbols (string uri) {
        return _document_symbols.get (uri);
    }
    /**
     * Updates document symbols for a given uri from
     * server
     */
    public async GVo.Container request_document_symbols (string uri) throws GLib.Error
        requires (manager != null)
    {
        var c = yield manager.client.document_symbols (uri);
        _document_symbols.set (uri, c);
        return c;
    }
    /**
     * Request to the server a completion list for a given
     * symbol's name in order to update the cache
     */
    public async GVo.Container?
    request_completion_items (string uri, string symbol) throws GLib.Error
        requires (manager != null)
    {
        var p = new SourcePosition ();
        var o = yield manager.client.completion (uri, symbol, p, null, null);
        GVo.Container c = null;
        if (o is CompletionList) {
            c = ((GVls.CompletionList) o).items;
        }

        if (o is GVo.Container) {
            c = (GVo.Container) o;
        }

        if (c == null) {
            return null;
        }

        if (c.get_n_items () == 0) {
            return c;
        }

        _completions.set (uri + ":" + symbol, c);

        return c;
    }
    /**
     * Find a word at given position in a {@link Gtk.TextBuffer}
     * associated to a given {@link Gtk.SourceView} and calculates
     * a cached completion list as {@link Gtk.SourceCompletionProposal}
     * items.
     *
     * If found word has no a cached list, it is requested to the server
     * and cached in a asynchonous operation out of this method. Then
     * return an empty list.
     *
     * Given URI should match given initialized source view, if not just
     * warns and returns.
     *
     * Usin this method to access cached completion list for a given
     * {@link Gtk.SourceView}
     */
    public async GLib.List<Gtk.SourceCompletionProposal>
    completion_items_at (string uri, GVls.SourcePosition pos, Gtk.SourceView view) throws GLib.Error
    {
        var proposals = new GLib.List<Gtk.SourceCompletionProposal> ();
        GVo.Container syms = null;

        var obj = yield manager.client.completion (uri, "", pos, null, null);
        if (obj is GVo.Container) {
            syms = (GVo.Container) obj;
        } else if (obj is GVls.CompletionList) {
            syms = ((GVls.CompletionList) obj).items;
        }

        if (syms == null) {
            return proposals;
        }

        for (int i = 0; i < syms.get_n_items (); i++) {
            GVls.CompletionItem item = syms.get_item (i) as GVls.CompletionItem;
            if (item == null) {
                continue;
            }

            var ci = new GLsvui.SourceCompletionProposal (item);
            proposals.append (ci);
        }

        return proposals;
    }
    /**
     * Calculate completion items for a given symbol's name at a given
     * file's URI. If no completions are cached for symbol a new
     * request is started to the server.
     */
    public GLib.List<Gtk.SourceCompletionProposal>
    completion_items (string uri, string symbol) {
        var proposals = new GLib.List<Gtk.SourceCompletionProposal> ();

        GVo.Container c = _completions.get (uri + ":" + symbol);
        if (c != null) {
            for (int i = 0; i < c.get_n_items (); i++) {
                GVls.CompletionItem item = c.get_item (i) as GVls.CompletionItem;
                if (item == null) {
                    continue;
                }

                var ci = new GLsvui.SourceCompletionProposal (item);
                proposals.append (ci);
            }

            return proposals;
        }

        request_completion_items.begin (uri, symbol);

        return proposals;
    }

    /**
     * Clean up cached of {@link Gtk.SourceCompletionProposal} items for
     * a given {@link Gtk.SourceView} and file's URI
     */
    public void remove_completions (Gtk.SourceView view, string uri) {
        string vuri = null;
        Gtk.TextBuffer buffer = view.get_buffer ();
        if (view is GLsvui.SourceView) {
            vuri = ((GLsvui.SourceView) view).uri;
        } else {
            vuri = buffer.get_data<string> ("gvls-uri");
        }

        if (uri != vuri) {
            warning (_("Provided source view widget has not been initialized: no URI was set or match with given one"));
            return;
        }

        foreach (string key in _completions.keys) {
            if (uri in key) {
                _completions.unset (key);
            }
        }
    }

    /**
     * Update cache of {@link Gtk.SourceCompletionProposal} items for
     * a given {@link Gtk.SourceView} and file's URI.
     *
     * This is an asynchronous operation. Make sure to update
     * document's symbols using {@link request_document_symbols}.
     *
     * This operation will iterate over all symbols in the document
     * and request a list of completion items for each one.
     */
    public async void
    update_completions_for_symbols (Gtk.SourceView view, string uri) throws GLib.Error
    {
        remove_completions (view, uri);
        var c = _document_symbols.get (uri);
        if (c == null) {
            return;
        }

        for (int i = 0; i < c.get_n_items (); i++) {
            GVls.DocumentSymbol s = c.get_item (i) as GVls.DocumentSymbol;
            if (s == null) {
                continue;
            }

            yield request_completion_items (uri, s.name);
        }
    }

    /**
     * For the given symbol's name, removes cached of {@link Gtk.SourceCompletionProposal}
     * items for a given {@link Gtk.SourceView} and file's URI
     */
    public void remove_completion (Gtk.SourceView view, string uri, string symbol) {
        string vuri = null;
        Gtk.TextBuffer buffer = view.get_buffer ();
        if (view is GLsvui.SourceView) {
            vuri = ((GLsvui.SourceView) view).uri;
        } else {
            vuri = buffer.get_data<string> ("gvls-uri");
        }

        if (uri != vuri) {
            warning (_("Provided source view widget has not been initialized: no URI was set or match with given one"));
            return;
        }

         _completions.unset (uri + ":" + symbol);
    }
    /**
     * Query a symbol based on its name.
     *
     * If name provides more than one just the first
     * provided is returned.
     */
    public async GVls.SymbolInformation? request_symbol (string name) throws GLib.Error
    {
        var c = yield manager.client.symbol (name);
        if (c.get_n_items () == 0) {
            return null;
        }

        return c.get_item (0) as GVls.SymbolInformation;
    }
    /**
     * Enable server autoconfigure by default
     */
    public void
    server_autoconfigure (bool auto) {
        _autoconfigure = auto;
    }
}
