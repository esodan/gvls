/* gvls-gedit-plugin.vala
 *
 * Copyright 2018-2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gee;

public class GVlsEdit.Window : Peas.ExtensionBase, Gedit.WindowActivatable {
    private ulong cn = 0;
    private uint timeout_id = -1;

    public GVlsp.ProjectManager manager { get; set; }

    public Gedit.Window window { construct; owned get; }

    ~Window () {
        if (timeout_id != -1) {
            var source = MainContext.@default ().find_source_by_id (timeout_id);
            if (source != null) {
                source.destroy ();
            }

        }
    }

    public void activate () {
        cn = window.tab_added.connect ((tab)=>{
            var doc = tab.get_document ();
            doc.loaded.connect (()=>{
                init_doc (doc);
            });
        });
        timeout_id = GLib.Timeout.add (500, update_symbols);
    }
    public void deactivate () {
        window.disconnect (cn);
        if (timeout_id != -1) {
            var source = GLib.MainContext.@default ().find_source_by_id (timeout_id);
            if (source != null) {
              source.destroy ();
            }
        }

        var m = window.get_data<GVlsp.ProjectManager> ("gvls-manager");
        if (m != null) {
            m.client.server_exit.begin ();
        }
    }
    public void update_state () {
    }
    private void init_doc (Gedit.Document doc) {
        var manager = window.get_data<GVlsp.ProjectManager> ("gvls-manager");

        var tab = Gedit.Tab.get_from_document (doc);
        var view = tab.get_view ();
        var prov = view.get_data<GVlsui.CompletionProvider> ("gvls-provider");
        if (prov == null) return;
        prov.uri = doc.get_file ().location.get_uri ();
        manager.client.document_open.begin (doc.get_file ().location.get_uri (),
                                    view.buffer.text, (obj,res)=>{
            try {
                manager.client.document_open.end (res);
                view.set_data<bool> ("gvls-view-dirty", false);
            } catch (GLib.Error e) {
                warning ("Error while open document notification: %s", e.message);
            }
        });
    }

    private bool update_symbols () {
        var view = window.get_active_view ();
        if (view == null) {
            return true;
        }

        var prov = view.get_data<GVlsui.CompletionProvider> ("gvls-provider");
        if (prov == null) {
            return true;
        }

        bool dirty = view.get_data<bool> ("gvls-view-dirty");
        if (!dirty) {
            return true;
        }


        return true;
    }
}
public class GVlsEdit.View : Peas.ExtensionBase, Gedit.ViewActivatable
{

    public Gedit.View view {
        owned get; construct;
    }

    public void activate () {
    }

    public void deactivate () {
    }
}

/*
* Plugin config dialog
*/
public class GVlsEdit.Config : Peas.ExtensionBase, PeasGtk.Configurable
{
    private Gtk.Widget configuration = null;
    private Gtk.Entry epath;

    public Gtk.Widget create_configure_widget ()
    {
        configuration = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
        var l = new Gtk.Label ("GVls Gedit 3.0 Vala Language Completion Plugin ");
        ((Gtk.Box) configuration).add (l);
        var b1 = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
        ((Gtk.Box) configuration).add (b1);
        var l1 = new Gtk.Label ("LSP Server Path:");
        epath = new Gtk.Entry ();
        epath.text = "org.gnome.gvls.stdio.Server";
        var fc = new Gtk.FileChooserButton ("Choose", Gtk.FileChooserAction.OPEN);
        b1.add (l1);
        b1.add (epath);
        b1.add (fc);
        fc.file_set.connect (()=>{
            epath.text = fc.get_file ().get_uri ();
        });
        return configuration;
    }

    class MessageServerPath : Gedit.Message {
        public MessageServerPath () {
            Object (method: "server_path_change", object_path: "/gvls/lsp/server");
        }
    }
}

/*
* Plugin config dialog
*/
public class GVlsEdit.App : Peas.ExtensionBase, Gedit.AppActivatable
{
    GLib.SimpleAction root_action;

    public Gedit.App app { owned get; construct; }


    public void activate ()
    {
        root_action = new GLib.SimpleAction ("app.select-root", null);
        root_action.activate.connect (()=>{
            Gtk.Window window = null;
            var w = app.active_window;
            var manager = w.get_data<GVlsp.ProjectManager> ("gvls-manager");
            if (manager != null) {
                window = (Gtk.Window) app.create_window (w.get_screen ());
            } else {
                window = w;
            }
            var dlg = new Gtk.FileChooserDialog (_("Select Project's Root Folder"),
                                                (Gtk.Window) window,
                                                Gtk.FileChooserAction.SELECT_FOLDER,
                                                _("_Cancel"), Gtk.ResponseType.CANCEL,
                                                _("_Select"), Gtk.ResponseType.ACCEPT,
                                                null);
            var res = dlg.run ();
            if (res == Gtk.ResponseType.ACCEPT) {
                var f = dlg.get_file ();
                var mesonf = GLib.File.new_for_uri (f.get_uri () + "/meson.build");
                if (!mesonf.query_exists ()) {
                    var mdlg = new Gtk.MessageDialog ((Gtk.Window) window,
                                                Gtk.DialogFlags.MODAL,
                                                Gtk.MessageType.INFO,
                                                Gtk.ButtonsType.OK,
                                                _("Currently just meson Build Systema is supported"));
                    mdlg.run ();
                    mdlg.destroy ();
                    return;
                }

                var bs = new GVlsp.BuildSystemMesonVala (f);
                var m = new GVlsp.ProjectManager (f, bs);
                m.initialize_stdio.begin ();
                window.set_data<GVlsp.ProjectManager> ("gvls-manager", m);
            }

            dlg.destroy ();

        });
        var mx = extend_menu ("app-commands-section");
        var item= new GLib.MenuItem (_("Open Root Folder"), "app.select-root");
        mx.append_menu_item (item);
    }
    public void deactivate ()
    {
        // FIXME: Remove a "Set Workspace folder" from menu
    }
}

[ModuleInit]
public void peas_register_types (GLib.TypeModule module)
{
  var objmodule = module as Peas.ObjectModule;

  // Register my plugin extension
  objmodule.register_extension_type (typeof (Gedit.ViewActivatable), typeof (GVlsEdit.View));
  objmodule.register_extension_type (typeof (Gedit.WindowActivatable), typeof (GVlsEdit.Window));
  // Register my config dialog
  objmodule.register_extension_type (typeof (PeasGtk.Configurable), typeof (GVlsEdit.Config));
  // Register my config dialog
  objmodule.register_extension_type (typeof (Gedit.AppActivatable), typeof (GVlsEdit.App));
}


