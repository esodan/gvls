/* glsvui-sourceview-initialize.vala
 *
 * Copyright 2021 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GVls;
using GLsvui;
using Gtk;
using Pango;

/**
 * This class provides a set of methods to implement same
 * {@link GLsvui.SourceView} features on {@link Gtk.SourceView}
 * based widgets.
 */
public class GLsvui.SourceViewInitialize : Gtk.SourceView
{
    /**
     * Takes a {@link Gtk.SourceView} and the document symbols
     * to highligth the given symbol
     */
    public static async void highligth_symbol (Gtk.SourceView view,
                                                Gtk.TextBuffer buffer,
                                                DocumentSymbol sym)
    {
        Gtk.TextBuffer buf = view.get_buffer ();
        if (buf == null) {
            return;
        }

        var table = buf.get_tag_table ();
        if (table.lookup ("keyword") == null) {
            return;
        }

        if (table.lookup ("type") == null) {
            return;
        }

        if (table.lookup ("property") == null) {
            return;
        }

        if (table.lookup ("variable") == null) {
            return;
        }

        if (table.lookup ("text") == null) {
            return;
        }

        int line_start = sym.range.start.line + 1;
        int char_start = sym.range.start.character + 1;
        int line_end = sym.range.end.line + 1;
        int char_end = sym.range.end.character + 1;
        Gtk.TextIter start;
        Gtk.TextIter end;
        buffer.get_iter_at_line_offset (out start,
                                        line_start,
                                        char_start);
        buffer.get_iter_at_line_offset (out end,
                                        line_end,
                                        char_end);
        switch (sym.kind) {
            case GVls.SymbolKind.KEY:
                buf.apply_tag_by_name ("keyword", start, end);
                break;
            case GVls.SymbolKind.CLASS:
            case GVls.SymbolKind.INTERFACE:
            case GVls.SymbolKind.STRUCT:
            case GVls.SymbolKind.ENUM:
            case GVls.SymbolKind.NAMESPACE:
                buf.apply_tag_by_name ("type", start, end);
                break;
            case GVls.SymbolKind.PROPERTY:
                buf.apply_tag_by_name ("property", start, end);
                break;
            case GVls.SymbolKind.VARIABLE:
                buf.apply_tag_by_name ("variable", start, end);
                break;
            case GVls.SymbolKind.METHOD:
                buf.apply_tag_by_name ("method", start, end);
                break;
            case GVls.SymbolKind.STRING:
                buf.apply_tag_by_name ("text", start, end);
                break;
            case GVls.SymbolKind.OBJECT:
            case GVls.SymbolKind.FIELD:
            case GVls.SymbolKind.ENUM_MEMBER:
            case GVls.SymbolKind.OPERATOR:
            case GVls.SymbolKind.CONSTANT:
            case GVls.SymbolKind.BOOLEAN:
            case GVls.SymbolKind.UNKNOWN:
            case GVls.SymbolKind.CONSTRUCTOR:
            case GVls.SymbolKind.TYPE_PARAMETER:
            case GVls.SymbolKind.PACKAGE:
            case GVls.SymbolKind.FUNCTION:
            case GVls.SymbolKind.ARRAY:
            case GVls.SymbolKind.NULL:
            case GVls.SymbolKind.EVENT:
            case GVls.SymbolKind.MODULE:
            case GVls.SymbolKind.FILE:
            case GVls.SymbolKind.NUMBER:
                break;
        }
    }

    /**
     * Takes a {@link Gtk.SourceView} to push all changes in the document
     * to the server in order to synchronize buffers contents.
     */
    public static bool push_document_changes (Gtk.SourceView view) {
        var buf = view.get_buffer () as SourceBuffer;
        if (buf == null) {
            return true;
        }

        GLsvui.ProjectManager manager = null;

        bool lsp_sync_in_progress = buf.get_data<bool> ("gvls-sync-in-progress");
        if (lsp_sync_in_progress) {
            return true;
        }

        string uri = null;

        if (view is GLsvui.SourceView) {
            GLsvui.SourceView sv = (GLsvui.SourceView) view;
            manager = sv.manager;
            uri = sv.uri;
        } else {
            manager = buf.get_data<GLsvui.ProjectManager> ("gvls-manager");
            uri = buf.get_data<string> ("gvls-uri");
        }

        GVo.Container changes = buf.get_data<GVo.Container>("gvls-changes");
        if (changes == null) {
            return true;
        }

        if (changes.get_n_items () != 0) {
            GVo.Container current_changes = changes;

            buf.set_data<GVo.Container> ("gvls-changes", new GVo.ContainerHashList.for_type (typeof (TextDocumentContentChangeEventInfo)));
            lsp_sync_in_progress = true;
            buf.set_data<bool> ("gvls-sync-in-progress", lsp_sync_in_progress);
            manager.manager.client.document_change.begin (uri, current_changes, (obj, res)=>{
                try {
                    manager.manager.client.document_change.end (res);
                } catch (GLib.Error e) {
                    warning ("Error while pushing changes to the server: %s", e.message);
                }

            });
        }

        return true;
    }

    /**
     * Takes a {@link Gtk.SourceView} based widget and initialize it in order to
     * add the futures of a {@link GLsvui.SourceView}
     */
    public static void
    initialize (Gtk.SourceView view, GLsvui.ProjectManager manager, GLib.File file)
    {
        var buf = view.get_buffer () as SourceBuffer;
        if (buf == null) {
            buf = new SourceBuffer (null);
            view.set_buffer (buf);
        }

        string uri = file.get_uri ();

        var prov = new GLsvui.CompletionProvider (manager, view);
        prov.uri = uri;
        try {
            view.get_completion ().add_provider (prov);
            view.get_completion ().auto_complete_delay = 500;
        } catch (GLib.Error e) {
            warning (_("Error while initialize source view completion provider: %s"), e.message);
        }

        GVo.Container changes = new GVo.ContainerHashList.for_type (typeof (TextDocumentContentChangeEventInfo));

        buf.set_data<string> ("gvls-uri", uri);
        buf.set_data<GVo.Container>("gvls-changes", changes);
        buf.set_data<bool>("gvls-opening", false);
        buf.set_data<bool>("gvls-sync-in-progress", false);
        buf.set_data<bool>("gvls-document-symbol-in-progress", false);
        buf.set_data<GLsvui.ProjectManager>("gvls-manager", manager);

        buf.delete_range.connect ((start, end)=>{
            var pstart = new SourcePosition.from_values (start.get_line (), start.get_line_offset ());
            var pend = new SourcePosition.from_values (end.get_line (), end.get_line_offset ());
            var change = new TextDocumentContentChangeEventInfo ();
            change.range.start = pstart;
            change.range.end = pend;
            change.text = null;
            GLsvui.SourceViewInitialize.add_change (view, change);
        });
        buf.insert_text.connect ((ref pos, text)=>{
            bool opening = buf.get_data<bool>("gvls-opening");
            if (opening) {
                buf.set_data<bool>("gvls-opening", false);
                return;
            }

            var pstart = new SourcePosition.from_values (pos.get_line (), pos.get_line_offset ());
            var pend = new SourcePosition.from_values (pos.get_line (), pos.get_line_offset ());
            var change = new TextDocumentContentChangeEventInfo ();
            change.range.start = pstart;
            change.range.end = pend;
            change.text = text;
            GLsvui.SourceViewInitialize.add_change (view, change);
        });
    }


    /**
     * Add changes to given {@link Gtk.SourceView} in order to
     * push them to the server
     */
    public static void add_change (Gtk.SourceView view, GVls.TextDocumentContentChangeEventInfo change) {
        var changes = view.get_buffer ().get_data<GVo.Container>("gvls-changes");

        debug ("Added change");
        changes.add (change);
    }

    /**
     * Takes a {@link Gtk.SourceView} and the document symbols
     * to highligth them according with its type.
     */
    // private static async void syntax_highlight (Gtk.SourceView view)
    // {
    //     Gtk.TextBuffer buf = view.get_buffer ();
    //     if (buf == null) {
    //         return;
    //     }

    //     GVlsp.ProjectManager manager = null;
    //     bool lsp_sync_in_progress = buf.get_data<bool> ("gvls-sync-in-progress");
    //     bool lsp_document_symbols_in_progress = buf.get_data<bool> ("gvls-document-symbol-in-progress");
    //     string uri = null;

    //     if (view is GLsvui.SourceView) {
    //         GLsvui.SourceView sv = (GLsvui.SourceView) view;
    //         manager = sv.manager;
    //         uri = sv.uri;
    //     } else {
    //         manager = buf.get_data<GVlsp.ProjectManager> ("gvls-manager");
    //         uri = buf.get_data<string> ("gvls-uri");
    //     }

    //     if (manager == null) {
    //         return;
    //     }

    //     if (manager.client == null) {
    //         return;
    //     }

    //     if (uri == null) {
    //         return;
    //     }

    //     if (lsp_sync_in_progress) {
    //         return;
    //     }

    //     if (lsp_document_symbols_in_progress) {
    //         return;
    //     }

    //     try {
    //         lsp_document_symbols_in_progress = true;
    //         buf.set_data<bool> ("gvls-document-symbol-in-progress", lsp_document_symbols_in_progress);

    //         GVo.Container syms = yield manager.client.document_symbols (uri);
    //         lsp_document_symbols_in_progress = false;
    //         buf.set_data<bool> ("gvls-document-symbol-in-progress", lsp_document_symbols_in_progress);

    //         for (int j = 0; j < syms.get_n_items (); j++) {
    //             DocumentSymbol sym = syms.get_item (j) as DocumentSymbol;
    //             if (sym == null) {
    //                 continue;
    //             }

    //             if (sym.kind == GVls.SymbolKind.UNKNOWN) {
    //                 continue;
    //             }

    //             if (sym.range.start.line == -1) {
    //                 continue;
    //             }

    //             yield highligth_symbol (view, view.get_buffer (), sym);
    //         }
    //     } catch (GLib.Error e) {
    //         warning ("Error highlighting syntax: %s", e.message);
    //     }
    // }
}

