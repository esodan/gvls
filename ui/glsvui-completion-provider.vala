/* glsvui-completion-provider.vala
 *
 * Copyright 2018, 2021 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GVls;
using Gtk;

public class GLsvui.CompletionProvider : GLib.Object, Gtk.SourceCompletionProvider
{
    /**
     * Emited in order to request a synchronization with the server
     * before to request a completion.
     *
     * Returns true if the syncronization was started
     */
    public signal bool request_buffer_syncronization ();

    // Last items retrieved from the server
    private GLib.List<Gtk.SourceCompletionProposal> _proposals = null;
    private weak GLsvui.ProjectManager _manager;
    private weak Gtk.SourceView _view;
    private bool in_progress = false;
    /**
     * Project Manager this provider is connected to
     */
    public GLsvui.ProjectManager manager { get { return _manager; } }
    /**
     * Current document URI in use
     */
    public string uri { get; set; }
    /**
     * Current requested items from server
     */
    public GLib.List<Gtk.SourceCompletionProposal> proposals { get { return _proposals; } }

    // Gtk.SourceCompletionProvider
    internal string get_name () { return "GVls"; }

    public CompletionProvider (GLsvui.ProjectManager manager, Gtk.SourceView view) {
        _manager = manager;
        _view = view;
    }

    internal void populate (Gtk.SourceCompletionContext context) {
        var p = new GLib.List<Gtk.SourceCompletionProposal> ();
        if (in_progress) {
            debug ("Waiting to fish completion request");
            context.add_proposals (this, p, false);
            return;
        }

        if (uri == null) {
            return;
        }

        var pos = new GVls.SourcePosition.from_values (context.iter.get_line (), context.iter.get_line_offset () - 1);
        in_progress = true;
        _manager.completion_items_at.begin (uri, pos, _view, (o, r)=>{
            try {
                in_progress = false;
                _proposals = _manager.completion_items_at.end (r);
                context.add_proposals (this, proposals, true);
            } catch (GLib.Error e) {
                warning ("Error while getting server proposals: %s", e.message);
            }
        });

        context.add_proposals (this, p, false);
    }
}



public class GLsvui.SourceCompletionProposal : GLib.Object, Gtk.SourceCompletionProposal
{
    private GVls.CompletionItem _item;
    private GLib.Icon _icon;
    private GLib.Icon _icon_current;

    construct  {
      _icon = new ThemedIcon ("network-transmit");
    }
    public SourceCompletionProposal (GVls.CompletionItem item) {
      _item = item;
    }
    public unowned GLib.Icon? get_gicon () {
        if (_icon_current != null) {
            return _icon_current;
        }
      switch (_item.kind) {
        case GVls.CompletionItemKind.CLASS:
          _icon_current = new FileIcon (File.new_for_uri("resource:///gvls/class.svg"));
          break;
        case GVls.CompletionItemKind.INTERFACE:
          _icon_current = new FileIcon (File.new_for_uri("resource:///gvls/interface.svg"));
          break;
        // case CompletionItemKind.NAMESPACE:
        //   _icon_current = new FileIcon (File.new_for_uri("resource:///gvls/namespace.svg"));
        //   break;
        case GVls.CompletionItemKind.STRUCT:
          _icon_current = new FileIcon (File.new_for_uri("resource:///gvls/struct.svg"));
          break;
        case GVls.CompletionItemKind.ENUM:
          _icon_current = new FileIcon (File.new_for_uri("resource:///gvls/enum.svg"));
          break;
        case GVls.CompletionItemKind.ENUM_MEMBER:
          _icon_current = new FileIcon (File.new_for_uri("resource:///gvls/enumvalue.svg"));
          break;
        case GVls.CompletionItemKind.METHOD:
          _icon_current = new FileIcon (File.new_for_uri("resource:///gvls/method.svg"));
          break;
        // case GVls.CompletionItemKind.VARIABLE:
        //   _icon_current = new FileIcon (File.new_for_uri("resource:///gvls/property.svg"));
        //   break;
        case GVls.CompletionItemKind.PROPERTY:
          _icon_current = new FileIcon (File.new_for_uri("resource:///gvls/property.svg"));
          break;
        default:
          _icon_current = _icon;
          break;
      }
      return _icon_current;
    }
    public unowned string? get_icon_name () {
      return "symbol";
    }
    public string? get_info () { return _item.detail; }
    public string get_label () {
      return _item.label;
    }
    public string get_markup () {
      // FIXME: markup text according with its type
      return  _item.label;
    }
    public string get_text () {
      string str =  _item.label;
      switch (_item.kind) {
        case GVls.CompletionItemKind.CLASS:
        case GVls.CompletionItemKind.INTERFACE:
        case GVls.CompletionItemKind.STRUCT:
        case GVls.CompletionItemKind.ENUM:
        case GVls.CompletionItemKind.ENUM_MEMBER:
        case GVls.CompletionItemKind.VARIABLE:
        case GVls.CompletionItemKind.PROPERTY:
        case GVls.CompletionItemKind.UNKNOWN:
        case GVls.CompletionItemKind.KEYWORD:
        case GVls.CompletionItemKind.FUNCTION:
        case GVls.CompletionItemKind.SNIPPED:
        case GVls.CompletionItemKind.TYPE_PARAMETER:
        case GVls.CompletionItemKind.MODULE:
        case GVls.CompletionItemKind.REFERENCE:
        case GVls.CompletionItemKind.VALUE:
        case GVls.CompletionItemKind.CONSTANT:
        case GVls.CompletionItemKind.TEXT:
        case GVls.CompletionItemKind.EVENT:
        case GVls.CompletionItemKind.CONSTRUCTOR:
        case GVls.CompletionItemKind.COLOR:
        case GVls.CompletionItemKind.FOLDER:
        case GVls.CompletionItemKind.OPERATOR:
        case GVls.CompletionItemKind.FIELD:
        case GVls.CompletionItemKind.FILE:
        case GVls.CompletionItemKind.UNIT:
          break;
        case GVls.CompletionItemKind.METHOD:
          str += " ()";
          break;
      }
      return str;
    }
}

