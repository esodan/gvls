/* glsvui-sourceview.vala
 *
 * Copyright 2018-2021 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GVls;
using GLsvui;
using Gtk;
using Pango;

public class GLsvui.SourceView : Gtk.SourceView
{
    private string _uri = "";
    private uint timed_id = -1;
    private uint timed_highligth_id = -1;
    private Cancellable cancellable = new Cancellable ();
    private GLsvui.ProjectManager _manager = null;
    private GVo.Container changes = new GVo.ContainerHashList.for_type (typeof (TextDocumentContentChangeEventInfo));
    private GLsvui.CompletionProvider prov = null;
    private bool lsp_sync_in_progress = false;
    private bool lsp_document_symbols_in_progress = false;
    private bool opening = false;

    public string uri {
        get { return _uri; }
    }

    public GLsvui.ProjectManager manager {
        get { return _manager; }
    }

    construct {
        editable = true;
        cursor_visible = true;
        monospace = true;
        show_line_numbers = true;
        indent_on_tab = true;
        indent_width = 4;
        tab_width = 4;
        completion.auto_complete_delay = 500;
        var buf = get_buffer () as SourceBuffer;
        if (buf == null) {
            buf = new SourceBuffer (null);
            set_buffer (buf);
        }
        buf.delete_range.connect ((start, end)=>{
            var pstart = new SourcePosition.from_values (start.get_line (), start.get_line_offset ());
            var pend = new SourcePosition.from_values (end.get_line (), end.get_line_offset ());
            var change = new TextDocumentContentChangeEventInfo ();
            change.range.start = pstart;
            change.range.end = pend;
            change.text = null;
            changes.add (change);
        });
        buf.insert_text.connect ((ref pos, text)=>{
            if (opening) {
                opening = false;
                return;
            }

            var pstart = new SourcePosition.from_values (pos.get_line (), pos.get_line_offset ());
            var pend = new SourcePosition.from_values (pos.get_line (), pos.get_line_offset ());
            var change = new TextDocumentContentChangeEventInfo ();
            change.range.start = pstart;
            change.range.end = pend;
            change.text = text;
            changes.add (change);
        });
        buf.highlight_matching_brackets = true;
        buf.highlight_syntax = true;
        buf.create_tag ("bold", "weight", Pango.Weight.BOLD);
        buf.create_tag ("type", "weight", Pango.Weight.BOLD, "foreground", "#204a87");
        buf.create_tag ("keyword", "weight", Pango.Weight.BOLD, "foreground", "#a40000");
        buf.create_tag ("text", "weight", Pango.Weight.NORMAL, "foreground", "#729fcf");
        buf.create_tag ("number", "weight", Pango.Weight.BOLD, "foreground", "#ad7fa8");
        buf.create_tag ("method", "weight", Pango.Weight.BOLD, "foreground", "#729fcf");
        buf.create_tag ("property", "weight", Pango.Weight.BOLD, "foreground", "#BC1F51");
        buf.create_tag ("variable", "weight", Pango.Weight.BOLD, "foreground", "#A518B5");
        var lm = Gtk.SourceLanguageManager.get_default ();
        var lang = lm.get_language ("vala");
        if (lang != null) {
            buf.language = lang;
        } else {
            warning ("Vala language is not defined");
        }
    }

    public SourceView (GLsvui.ProjectManager manager) {
        _manager = manager;
        prov = new GLsvui.CompletionProvider (manager, this);
        try {
            get_completion ().add_provider (prov);
        } catch (GLib.Error e) {
            message (_("Error adding provider for GLsvui.SourceView: %s"), e.message);
        }
        timed_id = Timeout.add (100, push_document_changes);
    }
    ~SourceView () {
        if (timed_id != -1) {
            var source = MainContext.@default ().find_source_by_id (timed_id);
            if (source != null) {
                source.destroy ();
            }
        }
        if (timed_highligth_id != -1) {
            var source = MainContext.@default ().find_source_by_id (timed_highligth_id);
            if (source != null) {
                source.destroy ();
            }
        }
    }

    public async void open_file (GLib.File file) throws GLib.Error {
        if (_manager == null) {
            return;
        }

        if (_manager.manager.client == null) {
            throw new SourceViewError.BUFFER_ERROR (_("No client was set"));
        }
        if (get_buffer () == null) {
            return;
        }
        if (!file.query_exists()) {
            return;
        }

        _uri = file.get_uri ();
        prov.uri = _uri;
        var ostream = new MemoryOutputStream.resizable ();
        ostream.splice (file.read (), OutputStreamSpliceFlags.CLOSE_SOURCE, cancellable);
        var t = (string) ostream.get_data ();;
        if (t != null) {
        opening = true;
        get_buffer ().text = t;
        _manager.manager.client.document_open.begin (uri, t, (obj, res)=> {
            try {
                manager.manager.client.document_open.end (res);
                syntax_highlight.begin ();
            } catch (GLib.Error e) {
                warning ("Error while opening file: %s: %s", uri, e.message);
            }
        });
        }
    }
    private bool push_document_changes () {
        if (lsp_sync_in_progress) {
            return true;
        }
        if (changes.get_n_items () != 0) {
            GVo.Container current_changes = changes;
            changes = new GVo.ContainerHashList.for_type (typeof (TextDocumentContentChangeEventInfo));
            lsp_sync_in_progress = true;
            manager.manager.client.document_change.begin (uri, current_changes, (obj, res)=>{
                try {
                    manager.manager.client.document_change.end (res);
                    lsp_sync_in_progress = false;
                } catch (GLib.Error e) {
                    warning ("Error while pushing changes to the server: %s", e.message);
                }
            });
        }
        return true;
    }
    public bool apply_doc_syntax_highlight ()
    {
        syntax_highlight.begin ();
        return true;
    }
    public async void syntax_highlight ()
    {
        if (_manager == null) {
            return;
        }

        if (_manager.manager.client == null) {
            return;
        }

        if (lsp_sync_in_progress) {
            return;
        }

        if (lsp_document_symbols_in_progress) {
            return;
        }

        try {
            lsp_document_symbols_in_progress = true;

            GVo.Container syms = yield _manager.manager.client.document_symbols (uri);
            lsp_document_symbols_in_progress = false;
            for (int j = 0; j < syms.get_n_items (); j++) {
                DocumentSymbol sym = syms.get_item (j) as DocumentSymbol;
                if (sym == null) {
                    continue;
                }

                if (sym.kind == GVls.SymbolKind.UNKNOWN) {
                    continue;
                }

                if (sym.range.start.line == -1) {
                    continue;
                }

                yield highligth_symbol (buffer, sym);
            }
        } catch (GLib.Error e) {
            warning ("Error highlighting syntax: %s", e.message);
        }
    }
    private async void highligth_symbol (Gtk.TextBuffer buffer,
                                DocumentSymbol sym)
    {
        var table = get_buffer ().get_tag_table ();
        if (table.lookup ("keyword") == null) {
            return;
        }

        if (table.lookup ("type") == null) {
            return;
        }

        if (table.lookup ("property") == null) {
            return;
        }

        if (table.lookup ("variable") == null) {
            return;
        }

        if (table.lookup ("text") == null) {
            return;
        }

        int line_start = sym.range.start.line + 1;
        int char_start = sym.range.start.character + 1;
        int line_end = sym.range.end.line + 1;
        int char_end = sym.range.end.character + 1;
        Gtk.TextIter start;
        Gtk.TextIter end;
        buffer.get_iter_at_line_offset (out start,
                                        line_start,
                                        char_start);
        buffer.get_iter_at_line_offset (out end,
                                        line_end,
                                        char_end);
        if (cancellable.is_cancelled ()) {
            return;
        }
        switch (sym.kind) {
            case GVls.SymbolKind.KEY:
                get_buffer ().apply_tag_by_name ("keyword", start, end);
                break;
            case GVls.SymbolKind.CLASS:
            case GVls.SymbolKind.INTERFACE:
            case GVls.SymbolKind.STRUCT:
            case GVls.SymbolKind.ENUM:
            case GVls.SymbolKind.NAMESPACE:
                get_buffer ().apply_tag_by_name ("type", start, end);
                break;
            case GVls.SymbolKind.PROPERTY:
                get_buffer ().apply_tag_by_name ("property", start, end);
                break;
            case GVls.SymbolKind.VARIABLE:
                get_buffer ().apply_tag_by_name ("variable", start, end);
                break;
            case GVls.SymbolKind.METHOD:
                get_buffer ().apply_tag_by_name ("method", start, end);
                break;
            case GVls.SymbolKind.STRING:
                get_buffer ().apply_tag_by_name ("text", start, end);
                break;
            case GVls.SymbolKind.OBJECT:
            case GVls.SymbolKind.FIELD:
            case GVls.SymbolKind.ENUM_MEMBER:
            case GVls.SymbolKind.OPERATOR:
            case GVls.SymbolKind.CONSTANT:
            case GVls.SymbolKind.BOOLEAN:
            case GVls.SymbolKind.UNKNOWN:
            case GVls.SymbolKind.CONSTRUCTOR:
            case GVls.SymbolKind.TYPE_PARAMETER:
            case GVls.SymbolKind.PACKAGE:
            case GVls.SymbolKind.FUNCTION:
            case GVls.SymbolKind.ARRAY:
            case GVls.SymbolKind.NULL:
            case GVls.SymbolKind.EVENT:
            case GVls.SymbolKind.MODULE:
            case GVls.SymbolKind.FILE:
            case GVls.SymbolKind.NUMBER:
                break;
        }
    }
}


public errordomain GLsvui.SourceViewError {
  BUFFER_ERROR
}

