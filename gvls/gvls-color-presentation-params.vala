/* gvls-color-presentation-params.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.ColorPresentationParams : GLib.Object {
	/**
	 * The text document.
	 */
	public abstract TextDocumentIdentifier text_document { get; set; }

	/**
	 * The color information to request presentations for.
	 */
	public abstract Color color { get; set; }

	/**
	 * The range where the color would be inserted. Serves as a context.
	 */
	public abstract Range range { get; set; }
}
