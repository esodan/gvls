/* gvls-text-document-did-change.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Document change notification params
 */
public interface GVls.DidChangeTextDocumentParams : GLib.Object {
  /**
   * The document that did change. The version number points
   * to the version after all provided content changes have
   * been applied.
   */
  public abstract VersionedTextDocumentIdentifier text_document { get; set; }

  /**
   * The actual content changes. The content changes describe single state changes
   * to the document. So if there are two content changes c1 and c2 for a document
   * in state S then c1 move the document to S' and c2 to S ' '
   *
   * Container elements are of type {@link TextDocumentContentChangeEvent}
   */
  public abstract GVo.Container content_changes { get; }
}

