/* gvls-target-manager.vala
 *
 * Copyright 2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public errordomain GVls.TargetManagerError {
    INVALID_CONFIGURATION_ERROR
}

/**
 * A target to be built in a project
 */
[Version (since="0.16.0")]
public interface GVls.TargetManager : GLib.Object
{
    /**
     * Emited when diagnostics has done.
     */
    public signal void did_diagnostics ();
    /**
     * List of targets in present workspace
     * as a {@link GVo.Container} of {@link Target}
     */
    public abstract GVo.Container                    targets { get; }
    /**
     * Common {@link GVls.TargetManagerConfiguration}
     */
    public abstract TargetManagerConfiguration   configuration { get; set; }
    /**
     * Common {@link GVls.DocumentationProvider} to provide symbol's
     * documentation
     */
    public abstract DocumentationProvider   documentation_provider { get; set; }
    /**
     * Target shared {@link GVls.SymbolDbProvider} to provide targets symbols
     */
    public abstract SymbolDbProvider   symbol_db_provider { get; set; }
    /**
     * System shared {@link GVls.SymbolDbProvider} to provide symbol
     */
    public abstract SymbolDbProvider   system_symbol_db_provider { get; set; }
    /**
     * Flag set to TRUE if there are a project building
     * work in progress; useful to avoid running multiple
     * works
     */
    public abstract bool                        building { get; }
    /**
     * Reset configuration and targets
     */
    public abstract void reset ();

    /**
    * Search a target with the given name
    */
    public virtual GVls.Target? target_by_name (string name)
    {
        for (int i = 0; i < targets.get_n_items (); i++) {
            GVls.Target t = targets.get_item (i) as GVls.Target;
            if (t == null) {
                continue;
            }

            if (t.name == name) {
                return t;
            }
        }
        return null;
    }
    /**
    * Search a target requiring the given {@link GLib.File}
    */
    public virtual GVls.Target? target_by_file (GLib.File file)
    {
        for (int i = 0; i < targets.get_n_items (); i++) {
            GVls.Target t = targets.get_item (i) as GVls.Target;
            if (t == null) {
                continue;
            }

            if (t.requires_file (file)) {
                return t;
            }
        }
        return null;
    }
    public virtual GVls.Target? target_by_uri (string uri)
    {
        GLib.File f = GLib.File.new_for_uri (uri);
        return target_by_file (f);
    }
    /**
     * Executes {@link Target.run_diagnostics} on each managed
     * {@link Target}, reports are saved at {@link Target.diagnostics_report}
     */
    public virtual async void run_diagnostics () throws GLib.Error {
        for (int i = 0; i < targets.get_n_items (); i++) {
            GVls.Target t = targets.get_item (i) as GVls.Target;
            if (t == null) {
                continue;
            }

            t.run_diagnostics.begin ();
        }

        did_diagnostics ();
    }
    /**
     * Executes {@link Target.run_diagnostics} on each managed
     * {@link Target}, all reports are saved at {@link Target.diagnostics_report}
     */
    public virtual void run_diagnostics_sync () throws GLib.Error {
        for (int i = 0; i < targets.get_n_items (); i++) {
            GVls.Target t = targets.get_item (i) as GVls.Target;
            if (t == null) {
                continue;
            }

            t.run_diagnostics_sync ();
        }

        did_diagnostics ();
    }
    /**
     * Creates a new target with given, if any, list of files.
     *
     * If the target already exists, no one will be added
     * and a null value is returned.
     */
    public abstract GVls.Target
    add_target (string target, GLib.ListModel? files = null);
    
    /**
     * Creates a new target, add it and setup with the given
     * configuration
     */
    public abstract GVls.Target add_target_configuration (string target,
                                        TargetConfiguration conf);
    /**
     * Apply meson configuration
     */
    public abstract void apply_configuration () throws GLib.Error;

    /**
     * Search all symbol's references with the given name
     */
    public virtual GVo.Container symbol_references (string uri, GVls.Position position)
    {
        return new GVo.ContainerHashList ();
    }

    /**
     * Looks at the given {@link GVls.Position} at given
     * file at given URI, to resolve a symbol.
     */
    public virtual async GVls.Symbol?
    resolve_symbol_position (string uri, GVls.Position position) {
        return resolve_symbol_position_sync (uri, position);
    }

    /**
     * Looks at the given {@link GVls.Position} at given
     * file at given URI, to resolve a symbol.
     */
    public virtual async GVls.Symbol?
    resolve_symbol_position_word (string word, string uri, GVls.Position position) {
        return resolve_symbol_position_word_sync (word, uri, position);
    }

    /**
     * Looks at the given {@link GVls.Position} at given
     * file at given URI, to resolve a symbol.
     */
    public virtual async GVls.Symbol?
    resolve_symbol (string name) {
        return resolve_symbol_sync (name);
    }

    /**
     * Looks at the given {@link GVls.Position} at given
     * file at given URI, to resolve a symbol.
     */
    public virtual GVls.Symbol?
    resolve_symbol_position_sync (string uri, GVls.Position position) { return null; }

    /**
     * Looks at the given {@link GVls.Position} at given
     * file at given URI, to resolve a symbol.
     */
    public virtual GVls.Symbol?
    resolve_symbol_position_word_sync (string word, string uri, GVls.Position position) { return null; }

    /**
     * Looks at the given {@link GVls.Position} at given
     * file at given URI, to resolve a symbol.
     */
    public virtual GVls.Symbol?
    resolve_symbol_sync (string name) { return null; }

    /**
     * Query cache for completions
     */
    public virtual async void update_cache () throws GLib.Error
    {
        update_cache_sync ();
    }
    /**
     * Query cache for completions, synchronically
     */
    public abstract void update_cache_sync () throws GLib.Error;
    /**
     * Query cache for completions
     */
    public virtual async GVls.CompletionList
    query_completion_cache (string name)
    {
        return query_completion_cache_sync (name);
    }
    /**
     * Query cache for completions, synchronically
     */
    public abstract GVls.CompletionList
    query_completion_cache_sync (string name);
    /**
     * Query cache for completions
     */
    public virtual async void
    create_completion_cache (string name, GVls.CompletionList list)
    {
        create_completion_cache_sync (name, list);
    }
    /**
     * Query cache for completions, synchronically
     */
    public abstract void
    create_completion_cache_sync (string name, GVls.CompletionList list);
    /**
     * Query a documentation string using given string
     * to find a symbol in, if present, the {@link GVls.SymbolDbProvider}
     */
    public abstract async string
    query_documentation (string str) throws GLib.Error;

    /**
     * Query a completion list using given string
     * using, if present, the {@link GVls.SymbolDbProvider}
     */
    public abstract async GVls.CompletionList
    query_completion_list (string str) throws GLib.Error;

    /**
     * Query a completion list at given URI and position
     * using, if present, the {@link GVls.SymbolDbProvider}
     */
    public abstract async GVls.CompletionList
    query_completion_list_at (string uri, GVls.Position pos) throws GLib.Error;
    /**
     * Query a {@link GVls.Hover} list using given string
     * to find a symbol and its hover description, including
     * its documentation from, if present, the {@link GVls.SymbolDbProvider}
     */
    public abstract async GVls.Hover
    query_hover (string str) throws GLib.Error;

    /**
     * Creates a target with just one file
     */
    public abstract GVls.Target
    add_target_for_file (GLib.File file);
    /**
     * Removes a {@link Target} using
     * the given file's URI
     */
    public abstract void
    remove_target_for_file (string uri);
}
