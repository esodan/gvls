/* gvls-completion-context.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Contains additional information about the context in which a completion request is triggered.
 */
public interface GVls.CompletionContext : GLib.Object, GVo.Object {
  /**
   * How the completion was triggered.
   */
  [Description (nick="triggerKind")]
  public abstract CompletionTriggerKind trigger_kind { get; set; }

  /**
   * The trigger character (a single character) that has trigger code complete.
   * Is undefined if `triggerKind !== CompletionTriggerKind.TriggerCharacter`
   */
  [Description (nick="triggerCharacter")]
  public abstract string? trigger_character { get; set; }

  /**
   * Word context, is a word calculated from client, used to calculate completion.
   *
   * This is a GVls extension to specification.
   */
  [Description (nick="word")]
  public abstract string? word { get; set; }
}
