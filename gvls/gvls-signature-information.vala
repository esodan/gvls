/* gvls-signature-information.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Represents the signature of something callable. A signature
 * can have a label, like a function-name, a doc-comment, and
 * a set of parameters.
 */
public interface GVls.SignatureInformation : GLib.Object
{
    /**
     * The label of this signature. Will be shown in
     * the UI.
     */
    [Description(nick="label")]
    public abstract string label { get; set; }

    /**
     * The human-readable doc-comment of this signature. Will be shown
     * in the UI but can be omitted.
     */
    [Description(nick="documentation")]
    public abstract MarkupContent documentation { get; set; }

    /**
     * The parameters of this signature of the type {@link ParameterInformation}.
     */
    [Description(nick="parameters")]
    public abstract GVo.Container parameters { get; set; }
}

