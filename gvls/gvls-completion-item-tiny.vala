/* gvls-gcompletion-item-tiny.vala
 *
 * Copyright 2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Small footprint for {@link GVls.CompletionItem}, with minimal
 * information
 */
public class GVls.CompletionItemTiny : CompletionItemInfo
{
    construct {
        additional_text_edits = null;
        commit_characters = null;
        detail = null;
        documentation = null;
        deprecated = null;
        preselect = null;
        sort_text = null;
        filter_text = null;
        insert_text_format = null;
        text_edit = null;
        command = null;
        insert_text = null;
    }

    public CompletionItemTiny.for_symbol (GVls.Symbol sym) {
        label = sym.name;
        detail = sym.full_name;
        kind = CompletionItemKind.from_symbol_kind (sym.symbol_kind);
        set_id (GLib.Uuid.string_random ());
    }

    public CompletionItemTiny.with_data (string label,
                                         string detail,
                                         CompletionItemKind kind)
    {
        this.label = label;
        this.detail = detail;
        this.kind = kind;
        set_id (GLib.Uuid.string_random ());
    }
}

