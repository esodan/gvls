/* gvls-documentation-provider.vala
 *
 * Copyright 2021 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Documentation Provider
 */
public interface GVls.DocumentationProvider : GLib.Object {
    /**
     * Object to cancel operations on this provider
     */
    public abstract GLib.Cancellable cancellable { get; set; }
    /**
     * Documentation Transformer, used to addapt documentation to
     * the one expected by a client
     */
    public abstract GVls.DocumentationTransformer transformer { get; construct set; }
    /**
     * Query provider for given symbol's documentation
     */
    public virtual async string
    query_documentation (string name,
                        GVls.SymbolProvider? provider) throws GLib.Error
    {
        return query_documentation_sync (name, provider);
    }
    /**
     * Query provider for given symbol's documentation, synchronically
     */
    public abstract string
    query_documentation_sync (string name,
                        GVls.SymbolProvider? provider) throws GLib.Error;
    /**
     * Query provider for given symbol's documentation
     */
    public virtual async string
    query_documentation_at (string uri, GVls.Position pos,
                                GVls.SymbolProvider provider) throws GLib.Error
    {
        return query_documentation_at_sync (uri, pos, provider);
    }
    /**
     * Query provider for given symbol's documentation, synchronically
     */
    public abstract string
    query_documentation_at_sync (string uri, GVls.Position pos,
                                GVls.SymbolProvider provider) throws GLib.Error;
}

public errordomain GVls.DocumetationProviderError {
    INVALID_URI_ERROR,
    INVALID_FILE_ERROR,
    SYMBOL_NOT_FOUND_ERROR,
    INVALID_INPUT_ERROR,
    QUERY_ERROR
}

/**
 * A documentation transformer, used to addapt a text
 * provided from a {@link GVls.DocumentationTransformer}
 * to a text using the conventions supported by the Client
 */
public interface GVls.DocumentationTransformer : GLib.Object
{
    /**
     * Transform given documentation to an specific
     * format supported by the client.
     *
     * Input text, will depent on the {@link GVls.DocumentationProvider}
     * in use.
     */
    public abstract string transform (string txt) throws GLib.Error;
    /**
     * Transform given documentation given in XML format to an specific
     * format supported by the client.
     *
     * Input text, will depent on the {@link GVls.DocumentationProvider}
     * in use.
     */
    public abstract string transform_xml (string txt) throws GLib.Error;
    /**
     * Transform given documentation given as a HTML code to an specific
     * format supported by the client.
     *
     * Input text, will depent on the {@link GVls.DocumentationProvider}
     * in use.
     */
    public abstract string transform_html (string txt) throws GLib.Error;
}
