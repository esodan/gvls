/* gvls-gtext-document-client-capabilities.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GLib;

public class GVls.TextDocumentClientCapabilitiesInfo : GLib.Object, GVo.Object, TextDocumentClientCapabilities {
    [Description (nick="synchronization")]
    internal GVls.TextDocumentClientCapabilities.Synchronization synchronization { get; set; }
    [Description (nick="completion")]
    internal GVls.TextDocumentClientCapabilities.Completion completion  { get; set; }
    [Description (nick="hover")]
    internal GVls.TextDocumentClientCapabilities.Hover hover { get; set; }
    [Description (nick="signatureHelp")]
    internal GVls.TextDocumentClientCapabilities.SignatureHelp signature_help { get; set; }
    [Description (nick="references")]
    internal GVls.TextDocumentClientCapabilities.References references { get; set; }
    [Description (nick="documentHighlight")]
    internal GVls.TextDocumentClientCapabilities.DocumentHighlight document_highlight { get; set; }
    [Description (nick="documentSymbol")]
    internal GVls.TextDocumentClientCapabilities.DocumentSymbol document_symbol { get; set; }
    [Description (nick="formatting")]
    internal GVls.DocumentFormattingClientCapabilities formatting { get; set; }
    [Description (nick="rangeFormatting")]
    internal GVls.DocumentRangeFormattingClientCapabilities range_formatting { get; set; }
    [Description (nick="onTypeFormatting")]
    internal GVls.TextDocumentClientCapabilities.OnTypeFormatting on_type_formatting { get; set; }
    [Description (nick="definition")]
    internal GVls.TextDocumentClientCapabilities.Definition definition { get; set; }
    [Description (nick="typeDefinition")]
    internal GVls.TextDocumentClientCapabilities.TypeDefinition type_definition { get; set; }
    [Description (nick="implementation")]
    internal GVls.TextDocumentClientCapabilities.Implementation implementation { get; set; }
    [Description (nick="codeAction")]
    internal GVls.TextDocumentClientCapabilities.CodeAction code_action { get; set; }
    [Description (nick="codeLens")]
    internal GVls.TextDocumentClientCapabilities.CodeLens code_lens { get; set; }
    [Description (nick="documentLink")]
    internal GVls.TextDocumentClientCapabilities.DocumentLink document_link { get; set; }
    [Description (nick="colorProvider")]
    internal GVls.TextDocumentClientCapabilities.ColorProvider color_provider { get; set; }
    [Description (nick="rename")]
    internal GVls.TextDocumentClientCapabilities.Rename rename { get; set; }
    [Description (nick="publishDiagnostics")]
    internal GVls.TextDocumentClientCapabilities.PublishDiagnostics publish_diagnostics { get; set; }
    [Description (nick="foldingRange")]
    internal GVls.TextDocumentClientCapabilities.FoldingRange folding_range { get; set; }
    construct {
        document_symbol = new TextDocumentClientCapabilities.DocumentSymbol ();
        synchronization = new TextDocumentClientCapabilities.Synchronization ();
        completion = new TextDocumentClientCapabilities.Completion ();
        hover = new TextDocumentClientCapabilities.Hover ();
        signature_help = new TextDocumentClientCapabilities.SignatureHelp ();
        references = new TextDocumentClientCapabilities.References ();
        document_highlight = new TextDocumentClientCapabilities.DocumentHighlight ();
        document_symbol = new TextDocumentClientCapabilities.DocumentSymbol ();
        formatting = new DocumentFormattingClientCapabilities ();
        range_formatting = new DocumentRangeFormattingClientCapabilities ();
        on_type_formatting = new TextDocumentClientCapabilities.OnTypeFormatting ();
        definition = new TextDocumentClientCapabilities.Definition ();
        type_definition = new TextDocumentClientCapabilities.TypeDefinition ();
        implementation = new TextDocumentClientCapabilities.Implementation ();
        code_action = new TextDocumentClientCapabilities.CodeAction ();
        code_lens = new TextDocumentClientCapabilities.CodeLens ();
        document_link = new TextDocumentClientCapabilities.DocumentLink ();
        color_provider = new TextDocumentClientCapabilities.ColorProvider ();
        rename = new TextDocumentClientCapabilities.Rename ();
        publish_diagnostics = new TextDocumentClientCapabilities.PublishDiagnostics ();
        folding_range = new TextDocumentClientCapabilities.FoldingRange ();
    }
}

