/* gvls-code-lens-params-info.vala
 *
 * Copyright 2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * A code lens represents a command that should be shown along with
 * source text, like the number of references, a way to run tests, etc.
 *
 * A code lens is _unresolved_ when no command is associated to it. For performance
 * reasons the creation of a code lens and resolving should be done in two stages.
 */
public class GVls.CodeLensParamsInfo : GLib.Object,
                                    WorkDoneProgressParams,
                                    PartialResultParams,
                                    CodeLensParams,
                                    GVo.Object
{
	/**
	 * The range in which this code lens is valid. Should only span a single line.
	 */
    [Description (nick="textDocument")]
	internal TextDocumentIdentifier text_document { get; set; }

    [Description (nick="workDoneToken")]
	internal Variant? work_done_token { get; set; }

    [Description (nick="partialResultToken")]
	internal Variant? partial_result_token { get; set; }

	construct {
	    text_document = new GVls.TextDocumentIdentifierInfo ();
	    string id = GLib.Uuid.string_random ();
	    var s = new GVo.String ();
	    s.@value = id;
	    work_done_token = s.to_variant ();
	    partial_result_token = s.to_variant ();
	}
}
