/* gvls-gtext-document-did-change.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Implementation of {@link DidChangeTextDocumentParams}
 */
public class GVls.DidChangeTextDocumentParamsInfo : GLib.Object, DidChangeTextDocumentParams, GVo.Object {
    GVo.Container _content_changes;

    [Description (nick="textDocument")]
    internal VersionedTextDocumentIdentifier text_document { get; set; }

    [Description (nick="contentChanges")]
    internal GVo.Container content_changes { get { return _content_changes; } }

    construct {
        text_document = new VersionedTextDocumentIdentifierInfo ();
        text_document.version = 0;
        _content_changes = new GVo.ContainerHashList.for_type (typeof (TextDocumentContentChangeEventInfo));
    }

    public DidChangeTextDocumentParamsInfo (GVo.Container? changes) {
        if (changes != null && !changes.object_type.is_a (typeof (TextDocumentContentChangeEventInfo))) {
            warning ("Provided changes objects are not a set of TextDocumentContentChangeEvent events");
        }

        if (changes != null) {
            _content_changes = changes;
        }
    }
}

public class GVls.VersionedTextDocumentIdentifierInfo :
                TextDocumentIdentifierInfo,
                VersionedTextDocumentIdentifier,
                GVo.Object
{
    [Description (nick="version")]
    internal int version { get; set; }
}

public class GVls.TextDocumentContentChangeEventInfo :
                    GLib.Object,
                    GVo.Object,
                    TextDocumentContentChangeEvent
{

    [Description (nick="range")]
    internal Range? range { get; set; }
    [Description (nick="rangeLength")]
    internal int range_length { get { return 0; } } // FIXME:
    [Description (nick="text")]
    internal string text  { get; set; }

    construct {
        range = new SourceRange ();
        text = "";
    }
}

