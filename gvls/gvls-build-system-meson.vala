/* gvls-project-manager.vala
 *
 * Copyright 2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Meson's implementation of {@link GVls.BuildSystem}
 */
public class GVls.BuildSystemMeson : GLib.Object,
                                     GVls.BuildSystem
{
    protected GLib.Variant _configuration = null;
    protected GLib.File _root = null;
    protected GVo.Container _options;
    protected GLib.File _build_commands = null;
    protected GLib.File _targets_intropection = null;
    protected string _command = null;

    internal string name {
        get {
            return "meson";
        }
    }

    public virtual GLib.Variant configuration {
        get {
            return _configuration;
        }
    }

    internal GLib.File root {
        get {
            return _root;
        }
    }

    internal GVo.Container options {
        get {
            return _options;
        }
    }

    internal string intializer_command {
        get {
            return _command;
        }
    }

    internal string builddir {
        get;
        set;
        default = "gvlsbuild";
    }

    /**
     * A {@link GLib.File} for 'compile_commands.json'
     */
    public GLib.File build_commands { get { return _build_commands; } }

    /**
     * A {@link GLib.File} for 'meson-info/intro-targets.json'
     */
    public GLib.File targets_intropection { get { return _targets_intropection; } }

    construct {
        _options = new GVo.ContainerHashList.for_type (typeof (GVo.String));
        _command = "meson";
    }

    /**
     * Creates a new {@link GVls.BuildSystemMeson} using
     * given file as the project's root
     */
    public BuildSystemMeson (GLib.File file) {
        _root = file;
    }
    /**
     * Set a new path to find 'meson' command
     */
    internal void set_command_path (string cmd) {
        _command = cmd;
    }

    internal void initialize_sync () {
        try {
            GLib.File fhd = GLib.File.new_for_path (GLib.Environment.get_home_dir ());
            string[] es = GLib.Environ.get ();
            string env = GLib.Environ.get_variable (es, "GVLS_CONFIG");
            if (env != null) {
                fhd = GLib.File.new_for_path (env);
            }

            var gvlsd = GLib.File.new_for_uri (fhd.get_uri () + "/.gvls");
            if (!gvlsd.query_exists ()) {
                if (env != null) {
                    gvlsd.make_directory ();
                } else {
                    gvlsd = GLib.File.new_for_uri (GLib.Environment.get_user_data_dir () + "/gvls");

                    if (!gvlsd.query_exists ()) {
                        gvlsd.make_directory ();
                    }
                }
            }

            var pd = GLib.File.new_for_uri (gvlsd.get_uri () + "/" + _root.get_basename ());
            if (!pd.query_exists ()) {
                pd.make_directory ();
            }

            var d = GLib.File.new_for_uri (pd.get_uri () + "/gvlsbuild");
            if (d.query_exists ()) {
                add_option ("setup");
                add_option ("--wipe");
            }

            // Ejecute a subprocess to run 'meson' command
            var launcher = new GLib.SubprocessLauncher (SubprocessFlags.STDIN_PIPE|SubprocessFlags.STDOUT_PIPE);
            launcher.set_cwd (_root.get_path ());
            string[] sp_args = {_command};
            for (int i = 0; i < options.get_n_items (); i++) {
                var o = options.get_item (i) as GVo.String;
                if (o == null) {
                    continue;
                }

                sp_args += o.@value;
            }

            sp_args += d.get_path ();

            var subprocess = launcher.spawnv (sp_args);

            if (!subprocess.wait_check (null)) {
                message (_("meson configuration fails"));
                return;
            }

            if (!d.query_exists ()) {
                warning (_("Meson configuration directory was not created"));
            }

            _build_commands = GLib.File.new_for_uri (d.get_uri () + "/compile_commands.json");
            if (!_build_commands.query_exists ()) {
                message (_("Meson Build System: 'compile_commands.json' file doesn't exists at: %s"),
                        _build_commands.get_uri ());

                var launcher2 = new GLib.SubprocessLauncher (SubprocessFlags.STDIN_PIPE|SubprocessFlags.STDOUT_PIPE);
                launcher2.set_cwd (d.get_path ());

                string[] sp2_args = {_command, "--reconfigure"};

                for (int i = 0; i < options.get_n_items (); i++) {
                    var o = options.get_item (i) as GVo.String;
                    if (o == null) {
                        continue;
                    }

                    sp2_args += o.@value;
                }

                sp2_args += d.get_path ();

                var subprocess2 = launcher2.spawnv (sp2_args);

                if (subprocess2.wait_check (null)) {
                    message (_("meson re-configuration fails"));
                    return;
                }

                if (!_build_commands.query_exists ()) {
                    message (_("Meson re-configuration fails to generate meson introspection"));
                    return;
                }
            }

            _targets_intropection = GLib.File.new_for_uri (d.get_uri () + "/meson-info/intro-targets.json");
            if (!_targets_intropection.query_exists ()) {
                message (_("Meson Build System: 'intro-targets.json' file doesn't exists"));
                return;
            }
        } catch (GLib.Error e) {
            message (_("Error initializaing Meson Build System: %s"), e.message);
        }
    }

    internal async void cleanup () throws GLib.Error {}
    /**
     * Remove recursively directory.
     */
    public async void remove_directory (GLib.File dir) throws GLib.Error
    {
        remove_directory_sync (dir);
    }
    public void remove_directory_sync (GLib.File dir) throws GLib.Error
    {
        FileEnumerator files = null;
        files = dir.enumerate_children ("standard::*",
                                        FileQueryInfoFlags.NONE,
                                        null);

        FileInfo info;
        while ((info = files.next_file (null)) != null) {
            if (info.get_file_type () == FileType.DIRECTORY) {
                var fd = dir.resolve_relative_path (info.get_name ());
                if (fd.query_exists ()) {
                    remove_directory_sync (fd);
                }
            }

            var fl = dir.resolve_relative_path (info.get_name ());

            if (fl.query_exists ()) {
                fl.@delete ();
            }
        }

        dir.@delete ();
    }
}

