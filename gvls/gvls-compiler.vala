/* gvls-server-rpc.vala
 *
 * Copyright 2018,2019,2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GVls;


[Version(since="0.14.0")]
public interface GVls.Compiler : GLib.Object
{
    /**
     * Diagnostics reports
     */
    public abstract GVls.Report report { get; set; }

    /**
     * A target used to get compilation configuration
     */
    public abstract GVls.Target target { get; construct set; }

	public virtual async GVls.SymbolProvider run_target () throws GLib.Error
	{
	    return run (null);
	}

	public virtual async GVls.SymbolProvider
	run_target_uri (string uri) throws GLib.Error
	{
	    return run (uri);
	}

	public virtual GVls.SymbolProvider run_target_sync () throws GLib.Error
	{
	    return run (null);
	}

	public abstract GVls.SymbolProvider run (string? uri) throws GLib.Error;
}
