/* gvls-client.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A client connected to a Language Server Protocol service
 */
public interface GVls.Client : GLib.Object
{
  /**
   * Emmited once a completion has been completed
   * providing a list of completion items returned
   * by the server
   */
  public signal void did_completion (GLib.ListModel list);
  /**
   * Notified to the server the client has been
   * initialized
   */
  [Version (since="0.14.0")]
  public signal void notified_initialized ();
  /**
   * Server initialization has been performed
   */
  public signal void did_initialization ();
  /**
   * Diagnostics messages has been receibed
   */
  public signal void diagnostics (GVls.PublishDiagnosticsParams dgs);
  /**
   * Notified to the server the configuration has been changed
   */
  public signal void notified_configuration_change ();
  /**
   * Responded a request from the client about current
   * configuration
   */
  public signal void configuration_sent ();
  /**
   * Request to show a message to a user
   */
  public signal void show (string type, string msg);
  /**
   * Request to show a message to a user
   */
  public signal void progress (GLib.Variant token, GLib.Variant progress);

  public abstract GVls.ClientCapabilities? capabilities { get; set; }
  public abstract GVls.ServerCapabilities? server_capabilities { get; set; }
  public abstract GVls.Workspace          workspace { get; set; }

  public abstract bool                    initialized { get; internal set; }
  public abstract GLib.Cancellable        cancellable { get; set; }
 
  /**
   * Tokens of requested work to the server
   */
  [Version (since="21")]
  public abstract
  GLib.HashTable<uint,WorkDoneProgress>    work_done_tokens { get; set; }
  [Version (since="0.16")]
  public abstract GVo.Container            result_tokens { get; set; }
  [Version (since="0.16")]
  public abstract GVo.Container            result_diagnostics { get; set; }
  /**
   * Configuration to be sent to the server, serialized to a
   * {@link GLib.Variant}
   */
  [Version (since="0.16")]
  public abstract GLib.Variant?            configuration { get; set; }
  /**
   * Configure client to enable or disable use of {@link GLib.Variant}
   * to communicate with the server.
   */
  [Version (since="0.16")]
  public abstract bool                    use_gvariant { get; set; }
 
  /**
   * This client capabilities reported to the server
   */
  [Version (since="21")]
  public abstract string                   language_id { get; construct set; }

  /**
   * Set the workspace path in current file system to the server
   *
   * @param uri URI for workspace's folder
   */
  public abstract async void
  initialize (string? uri) throws GLib.Error;

  /**
   * Send ''initialized'' notification to the server
   */
  [Version (since="0.14.0")]
  public abstract async void notify_initialized () throws GLib.Error;
  /**
   * Request the server to start a shutdown process.
   *
   * Call this request before {@link server_exit}
   */
  public abstract async string server_shutdown () throws GLib.Error;
  /**
   * Notify the server to exit its process
   *
   * Call this request after {@link server_shutdown} request
   */
  public abstract async void server_exit () throws GLib.Error;
  /**
   * Notify the server to open a new file to parse and provides symbols
   *
   * @param doc_uri URI for document to open
   * @param contents document's contents
   */
  public abstract async void document_open (string doc_uri, string? contents) throws GLib.Error;

  /**
   * Query symbols on the server
   *
   * Provides a {@link GVo.Container} with elements of
   * type {@link GVls.SymbolInformation}, empty on fail.
   *
   * @param symbol a string with the symbols's name to query
   */
  public abstract async GVo.Container symbol (string symbol) throws GLib.Error;
  /**
   * Execute a completion request on given document for a given context.
   *
   * Returned {@link GLib.Object} could be a {@link GVo.Container} having {@link GVls.CompletionItem} items
   * or a {@link CompletionList}
   *
   * @param position the row and character in the source code
   * @param partial_result_token a token to be used by on partial results
   * @param work_done_token the token to be used by the server for work progress
   * @param context a {@link GVls.CompletionContext} to provide aditional information
   * @param position the row and character in the source code
   *
   * @return A new {@link GLib.Object}
   */
   public abstract async GLib.Object
   completion (string uri,
               GVls.Position position,
               GLib.Variant? partial_result_token = null,
               GLib.Variant? work_done_token = null,
               GVls.CompletionContext? context = null)
               throws GLib.Error;
  /**
   * Find all symbols for the given URI. Returns a {@link GVo.Container}
   * with a list of {@link DocumentSymbol} objects in the document.
   */
  public abstract async GVo.Container document_symbols (string uri) throws GLib.Error;
  /**
   * Notify the server for changes in the file to re-parse and provides symbols
   *
   * @param doc_uri URI for document to open
   * @param changes a list of changes in the document of the type {@link TextDocumentContentChangeEvent}
   */
  public abstract async void document_change (string doc_uri, GVo.Container changes) throws GLib.Error;


    /**
     * Recieve diagnostics notifications and notify it,
     * saves theme at {@link result_diagnostics}
     */
    [Version (since="0.16")]
    public virtual async void               received_diagnostics (GVls.PublishDiagnosticsParams d)
    {
        var ns = new GVo.NamedString ();
        ns.name = d.uri;
        result_diagnostics.add (d);
        diagnostics (d);
    }
    [Version (since="0.16")]
    public virtual async void               show_message (string type, string msg) throws GLib.Error
    {
        show (type, msg);
    }
	public virtual async void             change_workspace_folders (GVo.Container added, GVo.Container removed) throws GLib.Error {}
	/**
	 * Notify to he server any configuration change using 'workspace/didChangeConfiguration' id.
	 */
	public virtual async void             change_configuration (GLib.Variant settings) throws GLib.Error {}
	public virtual async void             change_watched_files (GVo.Container files) throws GLib.Error {}
	public virtual async string           execute_command (string cmd, GVo.Container @params) throws GLib.Error { return ""; }
	public virtual async void             will_save (string uri, GVls.TextDocumentSaveReason reason) throws GLib.Error {}
    [Version (since="0.16")]
	public virtual async void             did_save (string uri, string contents) throws GLib.Error {}
	public virtual async GVo.Container?       will_save_wait_until (string uri, GVls.TextDocumentSaveReason reason) throws GLib.Error { return null; }
	public virtual async void             document_close (string uri) throws GLib.Error {}

	public virtual async CompletionItem?  completion_resolve (string uri,
	                                    GVls.CompletionItem item) throws GLib.Error
	{
	    return null;
	}

	public virtual async Hover?           hover (string uri,
                                    GVls.Position position,
                                    GLib.Variant? work_done_token)
                                    throws GLib.Error
	{
	    return null;
  }
	
	public virtual async SignatureHelp?   signature_help (string uri,
                                    GVls.Position position,
                                    GLib.Variant? work_done_token,
                                    GVls.SignatureHelpContext? context = null)
                                    throws GLib.Error
    {
        return null;
    }
	
	public virtual async GVo.Container?       declaration (string uri, Position pos) throws GLib.Error { return null; }
	public abstract async GVo.Container?      definition (string uri, Position pos) throws GLib.Error;
	public virtual async GVo.Container?       type_definition (string uri, Position pos) throws GLib.Error { return null; }
	public virtual async GVo.Container?       implementation (string uri, Position pos) throws GLib.Error { return null; }
	public virtual async GVo.Container?       references (string uri, Position pos, bool decl,
	                                                    GLib.Variant? work_done_token,
	                                                    GLib.Variant? partial_result_token)
	                                                    throws GLib.Error
	                                            { return null; }
	public virtual async GVo.Container?       document_highlight (string uri, Position pos) throws GLib.Error { return null; }
	public virtual async GVo.Container?       code_action (string uri, Range range, GVo.Container context) throws GLib.Error { return null; }

	public virtual async GVo.Container?       code_lens (string uri, GLib.Variant? work_done_token) throws GLib.Error { return null; }

	public virtual async GVls.CodeLens?        code_lens_resolve (string uri, GVls.TextDocumentClientCapabilities.CodeLens code_lens) throws GLib.Error { return null; }

	public virtual async GVo.Container?       document_link (string uri, GLib.Variant? work_done_token) throws GLib.Error { return null; }

	public virtual async DocumentLink?    document_link_resolve (string uri, GLib.Variant? work_done_token) throws GLib.Error { return null; }

	public virtual async GVo.Container?       document_color (string uri) throws GLib.Error { return null; }
	public virtual async GVo.Container?       color_presentation (string uri, Color color, Range range) throws GLib.Error { return null; }
	/**
	 * Request format all file's content, returning
	 * a list of {@link GVls.TextEdit} object as
	 * a {@link GVo.Container} to apply over the
	 * file's content at the range indicated by
	 * {@link GVls.TextEdit} objects
	 */
	public virtual async GVo.Container?       request_format (string uri, GVls.FormattingOptions options, GLib.Variant? work_done_token) throws GLib.Error { return null; }
	/**
	 * Request format file's content at given
	 * {@link GVls.Range}, returning
	 * a list of {@link GVls.TextEdit} object as
	 * a {@link GVo.Container} to apply over the
	 * file's content at the range indicated by
	 * {@link GVls.TextEdit} objects
	 */
	public virtual async GVo.Container?       request_range_format (string uri, Range range, FormattingOptions options, GLib.Variant? work_done_token) throws GLib.Error { return null; }
	public virtual async GVo.Container?       request_on_type_format (string uri, Position pos, string text, FormattingOptions options) throws GLib.Error { return null; }
	public virtual async Variant?         rename (string uri, string new_name, Position pos, GLib.Variant? work_done_token) throws GLib.Error { return null; }
	public virtual async Variant?         prepare_rename (string uri, Position pos) throws GLib.Error { return null; }
	public virtual async GVo.Container?       folding_range (string uri) throws GLib.Error { return null; }

}

public errordomain GVls.ClientError {
  CONNECTION_ERROR,
  UNINITIALIZED_ERROR,
  INVALID_URI_ERROR,
  BAD_RESPONSE_ERROR,
  SERVER_SHUTTINGDOWN_ERROR,
  SERVER_EXIT_ERROR
}

 /**
  * Work done progress traker
  */
public class GVls.WorkDoneProgress : GLib.Object
{
    public GLib.Variant token { get; internal set; }
    public GLib.Variant progress { get; set; }
    public string description { get; set; }

    public WorkDoneProgress (GLib.Variant token) {
        this.token = token;
    }

    public string progress_to_string () {
        if (progress.is_of_type (GLib.VariantType.STRING)) {
            return progress.get_string (null);
        }

        if (progress.is_of_type (GLib.VariantType.INT16)) {
            return progress.get_int16 ().to_string ();
        }

        if (progress.is_of_type (GLib.VariantType.INT32)) {
            return progress.get_int32 ().to_string ();
        }

        if (progress.is_of_type (GLib.VariantType.INT64)) {
            return progress.get_int64 ().to_string ();
        }

        return "";
    }

    public static uint hash (GLib.Variant token) {
        if (token.is_of_type (GLib.VariantType.STRING)) {
            return token.get_string (null).hash ();
        }

        if (token.is_of_type (GLib.VariantType.INT16)) {
            return token.get_int16 ().to_string ("%d").hash ();
        }

        if (token.is_of_type (GLib.VariantType.INT32)) {
            return token.get_int32 ().to_string ("%d").hash ();
        }

        if (token.is_of_type (GLib.VariantType.INT64)) {
            return token.get_int64 ().to_string ("%l").hash ();
        }

        return 0;
    }
}

