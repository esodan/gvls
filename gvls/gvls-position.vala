/* gvls-position.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.Position : GLib.Object, GVo.Object {
  /**
   * Line position in a document (zero-based).
   */
  [Description (nick="line")]
  public abstract int line { get; set; }

  /**
   * Character offset on a line in a document (zero-based). Assuming that the line is
   * represented as a string, the `character` value represents the gap between the
   * `character` and `character + 1`.
   *
   * If the character value is greater than the line length it defaults back to the
   * line length.
   */
  [Description (nick="character")]
  public abstract int character { get; set; }
  /**
   * Creates a string representation
   */
  public virtual string to_string () {
      return "%d.%d".printf (line, character);
  }
}
