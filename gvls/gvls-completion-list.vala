/* gvls-completion-list.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Represents a collection of [completion items](#CompletionItem) to be presented
 * in the editor.
 */
public interface GVls.CompletionList : GLib.Object, GVo.Object {
  /**
   * This list it not complete. Further typing should result in recomputing
   * this list.
   */
  [Description (nick="isIncomplete")]
  public abstract bool is_incomplete { get; set; }

  /**
   * The completion items of type {@link CompletionItem}
   */
  [Description (nick="items")]
  public abstract GVo.Container items { get; }
}

