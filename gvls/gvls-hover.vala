/* gvls-hover.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The result of a hover request.
 */
public interface GVls.Hover : GLib.Object, GVo.Object
{
    /**
     * The hover's content as a [@link MarkedString],
     * a colection of {@link MarkedString} objects,
     * or {@link MarkupContent}
     */
    [Description(nick="contents")]
    [Version(since="0.16")]
    public abstract GLib.Variant contents { get; set; }

    /**
     * An optional range is a range inside a text document
     * that is used to visualize a hover, e.g. by changing the background color.
     */
    [Description(nick="range")]
    public abstract Range? range { get; set; }

    /**
     * Provides a string version of a hover
     */
    [Version(since="0.16")]
    public string to_string () {
        string s = "";
        if (contents == null) {
            return s;
        }
        var m = new MarkupContentInfo ();
        m.parse_variant (contents);
        if (m.kind.@value == MarkupKind.UNKNOWN) {
            var ms = new MarkedString ();
            ms.parse_variant (contents);
            return ms.value;
        }
        return m.value;
    }
}
