/* gvls-completion-params-info.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class GVls.CompletionParamsInfo : GLib.Object,
                                      GVo.Object,
                                      TextDocumentPositionParams,
                                      WorkDoneProgressParams,
                                      PartialResultParams,
                                      CompletionParams
{
    [Description(nick="context")]
    internal CompletionContext context { get; set; }

    [Description(nick="textDocument")]
    internal TextDocumentIdentifier text_document { get; set; }
    [Description(nick="position")]
    internal Position position { get; set; }

    [Description (nick="workDoneToken")]
	internal GLib.Variant? work_done_token { get; set; }

    [Description (nick="partialResultToken")]
	internal GLib.Variant? partial_result_token { get; set; }

    construct {
        context = new CompletionContextInfo ();
        text_document = new TextDocumentIdentifierInfo ();
        position = new SourcePosition ();
        partial_result_token = new GVo.String.for_string ("").to_variant ();
    }
}
