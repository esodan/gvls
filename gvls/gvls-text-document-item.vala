/* gvls-text-document-item.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.TextDocumentItem : GLib.Object, GVo.Object {
  public abstract GLib.File file { get; set; }
  /**
   * The text document's URI.
   */
  [Description (nick="uri")]
  public abstract string uri { owned get; set; }

  /**
   * The text document's language identifier.
   */
  [Description (nick="languageId")]
  public abstract string language_id { get; set; }

  /**
   * The version number of this document (it will increase after each
   * change, including undo/redo).
   */
  [Description (nick="version")]
  public abstract int version { get; set; }

  /**
   * The content of the opened text document.
   */
  [Description (nick="text")]
  public abstract string text { get; set; }
}

