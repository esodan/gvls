/* gvls-signature-help-options.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Signature help options.
 */
public interface GVls.SignatureHelpOptions : GLib.Object, GVo.Object {
    /**
     * The characters that trigger signature help
     * automatically.
     *
     * As set of {@link GVo.String} objects
     */
    [Description (nick="triggerCharacters")]
    public abstract GVo.Container trigger_characters { get; set; }
    /**
	 * List of characters that re-trigger signature help.
	 *
	 * These trigger characters are only active when signature help is already showing. All trigger characters
	 * are also counted as re-trigger characters.
	 *
	 * A list of {@link GVo.String} objects
	 *
	 * @since 3.15.0
	 */
    [Description (nick="retriggerCharacters")]
    [Version(since="0.16")]
	public abstract GVo.Container  retrigger_characters { get; set; }
}

