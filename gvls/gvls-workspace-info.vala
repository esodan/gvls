/* gvls-gworkspace.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GLib;
using GVls;

public class GVls.WorkspaceInfo : Object, GVo.ContainerHashable, GVls.Workspace
{
  GVo.Container _folders = new GVo.ContainerHashList ();
  GVo.Container _whatched_files = new GVo.ContainerHashList ();
  public GVo.Container folders { get { return _folders; } }
  public GVo.Container whatched_files { get { return _whatched_files; } }

  public uint hash () { return GLib.str_hash ("@workspace@-" + GLib.Uuid.string_random ()); }
  public bool equal (Object obj) {
    if (!(obj is GVls.Workspace)) {
        return false;
    }

    if (obj is GVo.ContainerHashable) {
        if (((GVo.ContainerHashable) obj).hash () == hash ()) {
            return true;
        }
    }

    return false;
  }
}
