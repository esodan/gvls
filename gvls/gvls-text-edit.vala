/* gvls-text-edit.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.TextEdit : GLib.Object, GVo.Object {
  /**
   * The range of the text document to be manipulated. To insert
   * text into a document create a range where start === end.
   */
  [Description (nick="range")]
  public abstract Range range { get; set; }

  /**
   * The string to be inserted. For delete operations use an
   * empty string.
   */
  [Description (nick="newText")]
  public abstract string new_text { get; set; }
}
