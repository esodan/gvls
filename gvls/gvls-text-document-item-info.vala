/* gvls-gtext-document-item.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class GVls.TextDocumentItemInfo : GLib.Object,
                                    GVo.Object,
                                    TextDocumentItem
{
    public GLib.File file { get; set; }

    [Description (nick="uri")]
    internal string uri {
        owned get {
            if (file == null) {
                return "";
            }

            return file.get_uri ();
        }
        set {
            file = GLib.File.new_for_uri (value);
        }
    }
    [Description (nick="languageId")]
    internal string language_id { get; set; }
    [Description (nick="version")]
    internal int version { get; set; }
    [Description (nick="text")]
    internal string text { get; set; }
}

