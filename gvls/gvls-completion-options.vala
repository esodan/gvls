/* gvls-completion-options.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Completion options.
 */
public interface GVls.CompletionOptions : GLib.Object, GVo.Object {
  /**
   * The server provides support to resolve additional
   * information for a completion item.
   */
  [Description (nick="resolveProvider")]
  public abstract bool resolve_provider { get; set; }

  /**
   * The characters that trigger completion automatically.
   *
   * As set of {@link GVo.String} objects
   */
  [Description (nick="triggerCharacters")]
  public abstract GVo.Container trigger_characters { get; set; }
}

