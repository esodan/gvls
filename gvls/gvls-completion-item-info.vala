/* gvls-completion-item-info.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class GVls.CompletionItemInfo : GLib.Object,
                                    GVo.Object,
                                    GVo.ContainerHashable,
                                    GVls.CompletionItem,
                                    GVo.Comparable,
                                    GVo.StringComparable
{
    [Description (nick="label")]
    internal string label { get; set; }
    [Description (nick="kind")]
    internal CompletionItemKind kind { get; set; }
    [Description (nick="tags")]
    internal GVo.Container tags { get; set; }
    [Description (nick="detail")]
    internal string? detail { get; set; }
    [Description (nick="documentation")]
    internal MarkupContent? documentation { get; set; }
    [Description (nick="deprecated")]
    internal GVo.Boolean deprecated { get; set; }
    [Description (nick="preselect")]
    internal GVo.Boolean preselect { get; set; }
    [Description (nick="sortText")]
    internal string? sort_text { get; set; }
    [Description (nick="filterText")]
    internal string? filter_text { get; set; }
    [Version (deprecated=true)]
    [Description (nick="insertText")]
    internal string? insert_text { get; set; }
    [Description (nick="insertTextFormat")]
    internal InsertTextFormatObject? insert_text_format { get; set; }
    [Description (nick="textEdit")]
    internal TextEdit? text_edit { get; set; }
    [Description (nick="additionalTextEdits")]
    internal GVo.Container additional_text_edits { get; set;  }
    [Description (nick="commitCharacters")]
    internal GVo.Container commit_characters { get; set; }
    [Description (nick="command")]
    internal Command? command { get; set; }
    [Description (nick="data")]
    internal GLib.Variant data { get; set; }

    construct {
        additional_text_edits = new GVo.ContainerHashList ();
        commit_characters = new GVo.ContainerHashList ();
        documentation = new MarkupContentInfo ();
        insert_text_format = new GVls.InsertTextFormatObject (InsertTextFormat.PLAIN_TEXT);
        text_edit = new TextEditInfo ();
    }

    public CompletionItemInfo.for_symbol (GVls.Symbol sym) {
        label = sym.name;
        kind = CompletionItemKind.from_symbol_kind (sym.symbol_kind);
        documentation.kind = new GVls.MarkupKindObject (GVls.MarkupKind.MARKDOWN);
        documentation.@value = sym.hover_markup ().to_string ();
        set_id (sym.full_name);
        detail = sym.detail;
        setup_insert_text ();
    }

    public CompletionItemInfo.with_data (string label, CompletionItemKind kind,
                                    string detail,
                                    GVls.MarkupKind doc_kind,
                                    string doc)
    {
        this.label = label;
        this.kind = kind;
        this.detail = detail;
        this.documentation.kind = new GVls.MarkupKindObject (doc_kind);
        this.documentation.@value = doc;
        set_id (GLib.Uuid.string_random ());
        setup_insert_text ();
    }

    internal void setup_insert_text () {
        switch (kind) {
            case GVls.CompletionItemKind.METHOD:
                text_edit.new_text = label + " ()";
                break;
            default:
                text_edit.new_text = label;
                break;
        }
    }
    // ContainerHashable
    internal uint hash () {
        return GLib.str_hash (label);
    }
    internal bool equal (GLib.Object obj) {
        if (!(obj is CompletionItem)) {
            return false;
        }

        return ((CompletionItem) obj).label == label;
    }

    // StringComparable
    internal string get_comparable_string () { return label; }
}

