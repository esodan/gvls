/* gvls-did-save-text-document-params.vala
 *
 * Copyright 2018-2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Parameters for `textDocument/didSave` notification
 */
public interface GVls.DidSaveTextDocumentParams : GLib.Object {
    /**
     * The document that was saved.
     */
    [Description (nick="textDocument")]
    public abstract TextDocumentIdentifier text_document { get; set; }

    /**
     * Optional the content when saved. Depends on the includeText value
     * when the save notification was requested.
     */
    [Description (nick="text")]
    public abstract string? text { get; set; }
}

