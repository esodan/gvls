/* gvls-vala-configuration.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GLib;

/**
 * Target Manager configuration used to configure the compilation
 * of targets in the project
 */
[Version (since="0.16.0")]
public interface GVls.TargetManagerConfiguration : Object,
                                                GVls.Configuration
{
    /**
     * Add current's Vala compiler's default VAPI dirs
     */
    public abstract bool default_vapi_dirs { get; set; }
    /**
     * Scan workspace's folders for Vala sources and
     * VAPI files
     */
    public abstract bool scan_workspace { get; set; }
    /**
     * Add namespaces declared in source
     * code with using directive
     */
    public abstract bool add_using_namespaces { get; set; }
    /**
     * Library's URI for VAPI directroy
     *
     * Vala compiler's default directory for VAPI
     */
    public abstract string library_vapidir { get; set; }
    /**
     * Generic System's URI for VAPI directroy
     *
     * Default directory for generic VAPI files on system
     */
    public abstract string system_vapidir { get; set; }
    /**
     * Additional options list
     *
     * Container's items are {@link GVo.NamedString}
     * kind.
     */
    public abstract GVo.Container options { get; set; }

    /**
     * Add an option to {@link options}
     */
    public virtual void add_option (string name, string val) {
        var p = new GVo.NamedString.from_values (name, val);
        options.add (p);
    }
    /**
     * Initialize defaults
     */
    public abstract void initialize_defaults ();
}


