/* gvls-gtext-document-sync-options.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class GVls.TextDocumentSyncOptionsInfo : GLib.Object, GVo.Object, TextDocumentSyncOptions {
    [Description (nick="openClose")]
    internal bool open_close { get; set; }
    [Description (nick="change")]
    internal TextDocumentSyncKind change { get; set; }
    [Description (nick="willSave")]
    internal bool will_save { get; set; }
    [Description (nick="willSaveWaitUntil")]
    internal bool will_save_wait_until { get; set; }
    [Description (nick="save")]
    internal SaveOptions save { get; set; }

    construct {
        save = new SaveOptionsInfo ();
    }
}

