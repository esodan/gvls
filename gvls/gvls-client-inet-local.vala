/* gvlsp-client-inet-local.vala
 *
 * Copyright 2018-2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Jsonrpc;
using GLib;
using GVls;

/**
 * An {@link ClientJsonrpc} to connect to servers running over local TCP/IP
 * at address 127.0.0.1.
 */
public class GVls.ClientInetLocal : GVls.ClientJsonrpc {
  uint16 port = 1024;
  /**
   * Creates a new {@link ClientInetLocal} and initalize the local TCP/IP
   * connection to a LSP accepting connections on port 1024; scan connections
   * up to 1030
   */
  public ClientInetLocal () {
    SocketClient client = new SocketClient ();
    InetAddress address;
    InetSocketAddress socket_address;
    SocketConnection conn = null;
    while (conn == null) {
      address = new InetAddress.from_bytes ({127, 0, 0, 1}, SocketFamily.IPV4);
      socket_address = new InetSocketAddress (address, port);
      try {
        conn = client.connect (socket_address);
      } catch {}
      if (conn != null) {
        break;
      }
      port++;
      if (port > 1030) {
        break;
      }
    }
    if (conn == null) {
      warning ("No connection to the server. Aborted");
    }
    accept_io_stream (conn);
  }
}
