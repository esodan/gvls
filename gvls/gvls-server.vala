/* gvls-server-rpc.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public errordomain GVls.ServerError {
    INVALID_LIBRARY_VAPIDIR_ERROR,
    INVALID_SYSTEM_VAPIDIR_ERROR
}

/**
 * Base interface for al Language Server implementations
 */
public interface GVls.Server : GLib.Object
{
    /**
    * Holds information about the current workspace used by the server
    */
    public abstract GVls.Workspace            workspace { get; }
    /**
    * Indicates if the server has been initialized by a client
    */
    public abstract bool                      initialized { get; set; }
    /**
    * Provides the server capabilites
    */
    public abstract GVls.ServerCapabilities   capabilities { get; }
    /**
    * Provides the client capabilites connected to the server
    */
    public abstract GVls.ClientCapabilities   client_capabilities { get; set; }
    /**
    * Cancel responses and on going activities in the server
    */
    public abstract GLib.Cancellable?         cancellable { get; set; }
    /**
    * The server has started a process of shut down
    */
    public abstract bool                      shutteddown { get; }
    /**
     * Tracks work done progress requests tokens
     */
    [Version(since="0.16.0")]
    public abstract GVo.Container                 work_tokens { get; set; }
    /**
     * Tracks partial result requests tokens
     */
    [Version(since="0.16.0")]
    public abstract GVo.Container                 result_tokens { get; set; }
    /**
     * Target manager for current project,
     * introspected using a supported build
     * system information.
     */
    [Version(since="0.16.0")]
    public abstract TargetManager             target_manager { get; set; }
    /**
     * Target manager for per file opened.
     *
     * The file may is not part of the current
     * project and if so, it is parsed individually
     * in order to produce information for symbol
     * and documentation generation in a fast way.
     *
     * A target is created per file, without
     * any build information so any atempt
     * to build it will try to do its best
     * for symbol resolution and documentation.
     */
    [Version(since="21.0")]
    public abstract TargetManager             file_target_manager { get; set; }
    /**
    * Emitted when a 'initialize' notification is sent
    * from the client and processed by the server
    */
    public signal void did_initialization ();
    /**
    * Emitted when a 'shutdown' request
    * is sent from the client and processed by the server
    */
    public signal void shutdown ();
    /**
    * Emitted when a 'exit' notification
    * is sent from the client and processed by the server
    */
    public signal void exit ();
    /**
    * Emitted when a '$/cancelRequest' request
    * is sent from the client and processed by the server
    */
    public signal void cancel ();
    /**
    * Emitted when a 'textDocument/didOpen' notification
    * is sent from the client and was processed by the server
    */
    public signal void did_open_document (string uri);
    /**
    * Emitted when a 'textDocument/didChange' notification
    * is sent from the client and was processed by the server
    * with the URI of the changed file's contents
    */
    public signal void did_change (string uri);
    /**
    * Emitted when a 'textDocument/didSave' notification
    * is sent from the client and was processed by the server
    */
    [Version (since="0.16.0")]
    public signal void did_save (string uri);
    /**
    * Emitted when a 'textDocument/didClose' notification
    * is sent from the client and was processed by the server
    */
    public signal void did_close ();
    /**
    * Emitted when a 'textDocument/completion' method
    * was requested from the client and was processed by the server
    */
    public signal void did_completion ();
    /**
    * Emitted when a 'textDocument/completion' method
    * was requested from the client and was processed by the server
    */
    [Version (since="0.16")]
    public signal void did_completion_resolve (CompletionItem item);
    /**
    * Emitted when a 'workspace/symbol' method
    * was requested from the client and was processed by the server
    */
    public signal void did_workspace_symbol ();
    /**
    * Emitted when a 'textDocument/documentSymbol' method
    * was requested from the client and was processed by the server
    */
    public signal void did_document_symbols ();
    /**
    * Emitted when a 'textDocument/publishDiagnostics' notification
    * was sent from the server to the client
    */
    public signal void did_diagnostics ();
    /**
    * Emitted when a 'textDocument/didSave' notification is sent
    * from the client and was processed by the server
    */
    public signal void will_save ();
    /**
    * Emitted when a 'textDocument/willSaveWaitUntil' notification
    * is sent from the client and was processed by the server
    */
    public signal void will_save_wait_until ();
    /**
    * Emitted when the scan operation for vala sources,
    * has been completed.
    */
    public signal void scan_completed ();
    /**
    * Emitted when configuration was finished
    */
    public signal void did_configuration ();
    /**
    * Emitted when configuration was changed from the client
    * or when the server is configured based on configuration
    * sent by the client after the server request it.
    */
    public signal void did_changed_configuration ();
    /**
    * Emitted when the server has started request for configuration
    * from the client.
    */
    [Version (since="0.14.0")]
    public signal void did_requested_configuration ();
    /**
    * Emitted when request textDocument/documentLink is completed
    */
    [Version (since="0.16.0")]
    public signal void did_document_link (GVo.Object work_token, GVo.Object response_token);
    /**
    * Emitted when request textDocument/codeLens is completed
    */
    [Version (since="0.16.0")]
    public signal void did_code_lens (GVo.Object work_token, GVo.Object response_token);
    
    /**
    * Emitted when request textDocument/codeLens is completed
    */
    [Version (since="0.16.0")]
    public signal void did_definition (GLib.Variant response);
    /**
    * Emitted when request textDocument/signatureHelp is completed
    */
    [Version (since="0.16.0")]
    public signal void did_signature_help ();
}
