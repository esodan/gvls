/* gvls-initialize-params-info.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GLib;

public class GVls.InitializeParamsInfo : GLib.Object,
                                        GVo.Object,
                                        InitializeParams
{
  
    [Description (nick="processId")]
    internal int process_id { get; set; }

    [Version (deprecated=true)]
    [Description (nick="rootPath")]
    internal string? root_path { get; set; }
    [Description (nick="rootUri")]
    internal string root_uri { get; set; }
    [Description (nick="initializationOptions")]
    internal GLib.Variant initialization_options { get; set; }
    [Description (nick="capabilities")]
    internal ClientCapabilities capabilities { get; set; }
    [Description (nick="trace")]
    internal string? trace { get; set; }
    [Description (nick="workspaceFolders")]
    internal GVo.Container? workspace_folders { get; set; }

    construct {
        process_id = 1;
        root_path = null;
        root_uri = "file:///stdin";
        capabilities = new ClientCapabilitiesInfo ();
        workspace_folders = new GVo.ContainerHashList ();
    }
}

