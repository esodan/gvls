/* gvls-signature-help-trigger-kind.vala
 *
 * Copyright 2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * How a signature help was triggered.
 *
 * @since 3.15.0
 */
[Version(since="0.16")]
public enum GVls.SignatureHelpTriggerKind
{
    /**
     * Unknown
     */    
    UNKNOWN,
	/**
	 * Signature help was invoked manually by the user or by a command.
	 */
	INVOKED,
	/**
	 * Signature help was triggered by a trigger character.
	 */
	TRIGGER_CHARACTER,
	/**
	 * Signature help was triggered by the cursor moving or by the document content changing.
	 */
	CONTENT_CHANGE
}

