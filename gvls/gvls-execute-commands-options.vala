/* gvls-execute-commands-options.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Execute command options.
 */
public interface GVls.ExecuteCommandOptions : GLib.Object,
                                            WorkDoneProgressOptions,
                                            GVo.Object
{
  /**
   * The commands to be executed on the server
   *
   * Container holding items of type {@link GVo.String}
   */
  [Description (nick="commands")]
  [Version (since="0.16")]
  public abstract GVo.Container commands { get; set; }
}

