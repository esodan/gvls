/* gvls-gclient-capabilities.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GLib;

public class GVls.ClientCapabilitiesInfo : GLib.Object,
                                        GVo.Object,
                                        ClientCapabilities
{
    [Description (nick="workspace")]
    internal WorkspaceClientCapabilities? workspace { get; set; }
    [Description (nick="textDocument")]
    internal TextDocumentClientCapabilities text_document { get; set; }
    [Description (nick="experimental")]
    internal GLib.Variant experimental { get; set; }
    [Description (nick="general")]
    internal ClientCapabilities.General? general { get; set; }
    construct {
        text_document = new TextDocumentClientCapabilitiesInfo ();
        workspace = new WorkspaceClientCapabilitiesInfo ();
        workspace.symbol = new WorkspaceClientCapabilities.Symbol ();
        general = new ClientCapabilities.General ();
    }
}

