/* gvls-markup-content.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A `MarkupContent` literal represents a string value which content is interpreted base on its
 * kind flag. Currently the protocol supports `plaintext` and `markdown` as markup kinds.
 *
 * If the kind is `markdown` then the value can contain fenced code blocks like in GitHub issues.
 * See [[https://help.github.com/articles/creating-and-highlighting-code-blocks/#syntax-highlighting]]
 *
 * Here is an example how such a string can be constructed using JavaScript / TypeScript:
 * {{{ts
 * let markdown: MarkdownContent = {
 *  kind: MarkupKind.Markdown,
 *	value: [
 *		'# Header',
 *		'Some text',
 *		'```typescript',
 *		'someCode();',
 *		'```'
 *	].join('\n')
 * };
 * }}}
 *
 * *Please Note* that clients might sanitize the return markdown. A client could decide to
 * remove HTML from the markdown to avoid script execution.
 */
public interface GVls.MarkupContent : GLib.Object, GVo.Object {
  /**
   * The type of the Markup from: {@link MarkupKind} namespace
   */
    [Description(nick="kind")]
    [Version (since="0.16")]
    public abstract MarkupKindObject kind { get; set; }

  /**
   * The content itself
   */
    [Description(nick="value")]
    public abstract string @value { get; set; }
}
