/* gvls-folding-range-kind.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public enum GVls.FoldingRangeKind {
  UNKNOWN,
	/**
	 * Folding range for a comment
	 */
	COMMENT,
	/**
	 * Folding range for a imports or includes
	 */
	IMPORTS,
	/**
	 * Folding range for a region (e.g. `#region`)
	 */
	REGION;

	public string to_string () {
	  switch (this) {
	    case COMMENT:
	      return "comment";
	    case IMPORTS:
	      return "imports";
	    case REGION:
	      return "region";
	    case UNKNOWN:
	      return "unknown";
	  }
	  return "";
	}

	public static FoldingRangeKind from_string (string str) {
	  switch (str) {
	    case "comment":
	      return COMMENT;
	    case "imports":
	      return IMPORTS;
	    case "region":
	      return REGION;
	  }
	  return UNKNOWN;
	}
}
