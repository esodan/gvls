/* gvls-versioned-text-document-identifier.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.VersionedTextDocumentIdentifier : GLib.Object, TextDocumentIdentifier {
  /**
   * The version number of this document. If a versioned text document identifier
   * is sent from the server to the client and the file is not open in the editor
   * (the server has not received an open notification before) the server can send
   * `null` to indicate that the version is known and the content on disk is the
   * truth (as speced with document content ownership).
   *
   * The version number of a document will increase after each change, including
   * undo/redo. The number doesn't need to be consecutive.
   */
  public abstract int version { get; set; }
}

