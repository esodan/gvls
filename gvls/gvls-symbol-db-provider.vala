/* gvlsp-documentation-db.vala
 *
 * Copyright 2021 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Symbol Database Provider.
 *
 * Database should provide basic information about the system
 * installed symbols or the current project
 */
public interface GVls.SymbolDbProvider : GLib.Object
{
    public abstract async void initialize_db () throws GLib.Error;
    /**
     * Query a symbol's documentation in the database with the given
     * fully qualified name
     */
    public abstract async string query_documentation (string full_name, string? uri, GLib.Cancellable? cancellable) throws GLib.Error;
    /**
     * Creates a {@link GVls.CompletionList} with all symbol's name in the database with the given
     * string
     */
    public abstract async GVls.CompletionList query_completion_list (string name, string? uri, GLib.Cancellable? cancellable = null) throws GLib.Error;
    /**
     * Query a symbol's {@link GVls.Hover} in the database with the given
     * fully qualified name
     */
    public virtual async GVls.Hover? query_hover (string full_name, string? uri, GLib.Cancellable? cancellable) throws GLib.Error { return null; }
    /**
     * Query a symbol in the database using provided string,
     * returning a list of symbols in the workspace as a list
     * of {@link SymbolInformation} objects.
     *
     * Objects should have given string as part of its name and,
     * if given, should be in the path directory or file represented
     * by given URI.
     */
    public virtual async GVo.Container query_symbols (string query, string? uri, GLib.Cancellable? cancellable) throws GLib.Error { return new GVo.ContainerHashList (); }
    /**
     * Query a symbol in the database with a full name as provided string
     * and, if given, the symbol should be declared in the path directory
     * or file represented by given URI.
     */
    public virtual async GVls.Symbol? query_symbol (string full_name, string? uri, GLib.Cancellable? cancellable) throws GLib.Error { return null; }
    /**
     * Query a symbol in the database at given file's URI and position
     */
    public virtual async GVls.Symbol? query_symbol_at (string uri, GVls.Position position, GLib.Cancellable? cancellable) throws GLib.Error { return null; }
    /**
     * Query a symbol in the database at given file's URI, with the
     * given name and relative to given position.
     *
     * Local variables, method's paramenters and properties are
     * specially sensitive to the position they are used
     * in order to identify the correct symbol. 
     */
    public virtual async GVls.Symbol? query_symbol_relative (string name, string uri, GVls.Position position, GLib.Cancellable? cancellable) throws GLib.Error { return null; }
    /**
     * Provides the symbols in a document as a list of {@link GVls.DocumentSymbol} objects
     */
    public virtual async GVo.Container query_document_symbols (string uri, GLib.Cancellable? cancellable) throws GLib.Error { return new GVo.ContainerHashList (); }
    /**
     * Save a symbol's information into the database.
     *
     * If the symbol already exists, this method will fail
     */
    public virtual async void push_symbol (GVls.Symbol symbol, GLib.Cancellable? cancellable) throws GLib.Error {}
    /**
     * Removes all symbols related to given URI from
     * the database
     */
    public virtual async void remove_document_symbols (string uri, GLib.Cancellable? cancellable) throws GLib.Error {}
}


public errordomain GVls.SymbolDbProviderError
{
    INVALID_SOURCE_FILE_ERROR,
    INVALID_DATABASE_URI_ERROR
}
