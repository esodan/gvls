/* gvls-gdocument-on-type-formatting-options.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class GVls.DocumentOnTypeFormattingOptionsInfo : GLib.Object,
                                         GVo.Object,
                                         DocumentOnTypeFormattingOptions
{
  [Description (nick="firstTriggerCharacter")]
  internal string first_trigger_character { get; set; }
  [Description (nick="moreTriggerCharacter")]
  internal GVo.Container more_trigger_character { get; set; }

  construct {
      more_trigger_character = new GVo.ContainerHashList.for_type (typeof (GVo.String));
  }
}

