/* gvls-client-capabilities-regular-expression.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.RegularExpressionsClientCapabilities :
                                                            GLib.Object,
                                                            GVo.Object
{
    /**
     * The engine's name.
     */
    [Description (nick="engine")]
    public abstract string? engine { get; set; }

    /**
     * The engine's version.
     */
    [Description (nick="version")]
    public abstract string version { get; set; }
}
