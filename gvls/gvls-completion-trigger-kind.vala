/* gvls-completion-trigger-kind.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * How a completion was triggered
 */
[Flags]
public enum GVls.CompletionTriggerKind {
  UNKNOWN,
  /**
   * Completion was triggered by typing an identifier (24x7 code
   * complete), manual invocation (e.g Ctrl+Space) or via API.
   */
  INVOKED,

  /**
   * Completion was triggered by a trigger character specified by
   * the `triggerCharacters` properties of the `CompletionRegistrationOptions`.
   */
  TRIGGER_CHARACTER,

  /**
   * Completion was re-triggered as the current completion list is incomplete.
   */
  TRIGGER_FOR_INCOMPLETE_COMPLETIONS;

  public string to_string () {
    string str = "";
    var a = new Gee.ArrayList<string> ();
    if (INVOKED in this) {
      a.add ("Invoked");
    }
    if (TRIGGER_CHARACTER in this) {
      a.add ("TriggerCharacter");
    }
    if (TRIGGER_FOR_INCOMPLETE_COMPLETIONS in this) {
      a.add ("TriggerForIncompleteCompletions");
    }
    for (int i = 0; i < a.size; i++) {
      str += a.get (i);
      if (i+1 < a.size) {
        str += "|";
      }
    }
    return str;
  }
  public GLib.Variant to_variant () {
    return new GLib.Variant.int64 (this);
  }
  public static CompletionTriggerKind parse_variant (GLib.Variant v) {
    return (CompletionTriggerKind) v.get_int64 ();
  }
}
