/* gvls-client-capabilities.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.ClientCapabilities : GLib.Object, GVo.Object {
  /**
   * Workspace specific client capabilities.
   */
  [Description (nick="workspace")]
  public abstract WorkspaceClientCapabilities? workspace { get; set; }

  /**
   * Text document specific client capabilities.
   */
  [Description (nick="textDocument")]
  public abstract TextDocumentClientCapabilities text_document { get; set; }

  /**
   * Experimental client capabilities.
   */
  [Description (nick="experimental")]
  public abstract GLib.Variant experimental { get; set; }

    /**
	 * General client capabilities.
	 *
	 * @since 3.16.0
	 */
    [Version(since="21.0")]
    [Description (nick="general")]
    public abstract General? general { get; set; }
	public class General : GLib.Object, GVo.Object
    {
        /**
		 * Client capability that signals how the client
		 * handles stale requests (e.g. a request
		 * for which the client will not process the response
		 * anymore since the information is outdated).
		 *
		 * @since 3.17.0
		 */
        [Description (nick="staleRequestSupport")]
		public StaleRequestSupport? stale_request_support { get; set; }

		public class StaleRequestSupport : GLib.Object, GVo.Object
		{
			/**
			 * The client will actively cancel the request.
			 */
            [Description (nick="cancel")]
			public bool cancel { get; set; }

			/**
			 * The list of requests for which the client
			 * will retry the request if it receives a
			 * response with error code `ContentModified`
			 *
			 * The container should have an array of {@link GVo.String}
			 * objects
			 */
            [Description (nick="retryOnContentModified")]
			public GVo.Container retry_on_content_modified { get; set; }

			construct {
			    retry_on_content_modified = new GVo.ContainerHashList.for_type (typeof (GVo.String));
			}
		}

		/**
		 * Client capabilities specific to regular expressions.
		 *
		 * @since 3.16.0
		 */
        [Description (nick="regularExpressions")]
		public RegularExpressionsClientCapabilities? regular_expressions { get; set; }

		/**
		 * Client capabilities specific to the client's markdown parser.
		 *
		 * @since 3.16.0
		 */
        [Description (nick="markdown")]
		public MarkdownClientCapabilities markdown { get; set; }

		/**
		 * The position encodings supported by the client. Client and server
		 * have to agree on the same position encoding to ensure that offsets
		 * (e.g. character position in a line) are interpreted the same on both
		 * side.
		 *
		 * To keep the protocol backwards compatible the following applies: if
		 * the value 'utf-16' is missing from the array of position encodings
		 * servers can assume that the client supports UTF-16. UTF-16 is
		 * therefore a mandatory encoding.
		 *
		 * If omitted it defaults to ['utf-16'].
		 *
		 * Implementation considerations: since the conversion from one encoding
		 * into another requires the content of the file / line the conversion
		 * is best done where the file is read which is usually on the server
		 * side.
		 *
		 * This is an array of {@link GVo.String} objects, representing a LSP
		 * Specification PositionEncodingKind, valid values are: 'utf-32', 'utf-16',
		 * 'utf-8'; each value formatted in UTF-32, UTF-16 and UTF-8 respectivally.
		 *
		 * @since 3.17.0
		 */
        [Description (nick="positionEncodings")]
		public GVo.Container position_encodings { get; set; }

		construct {
		    position_encodings = new GVo.ContainerHashList.for_type (typeof (GVo.String));
		}
    }
}
