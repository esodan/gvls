/* gvls-publish-diagnostics.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.PublishDiagnosticsParams : GLib.Object, GVo.Object {
	/**
	 * The URI for which diagnostic information is reported.
	 */
	public abstract string uri { get; set; }

	/**
	 * An array of diagnostic information items of type {@link Diagnostic}
	 */
	public abstract GVo.Container diagnostics { get; set; }
}


public interface GVls.Diagnostic : GLib.Object, GVo.Object {
  /**
   * The range at which the message applies.
   */
  public abstract Range range { get; set; }

  /**
   * The diagnostic's severity. Can be omitted. If omitted it is up to the
   * client to interpret diagnostics as error, warning, info or hint.
   */
  public abstract DiagnosticSeverity severity { get; set; }

  /**
   * The diagnostic's code, which might appear in the user interface.
   */
  public abstract string code { get; set; }

  /**
   * A human-readable string describing the source of this
   * diagnostic, e.g. 'typescript' or 'super lint'.
   */
  public abstract string source { get; set; }

  /**
   * The diagnostic's message.
   */
  public abstract string message { get; set; }

  /**
   * An array of related diagnostic information, e.g. when symbol-names within
   * a scope collide all definitions can be marked via this property.
   *
   * Container has items of type {@link DiagnosticRelatedInformation}
   */
  public abstract GVo.Container related_information { get; set; }

  /**
   * Create a string representation of a diagnostics
   */
  public virtual string to_string () {
      return code + ":'" + message + "'"
            + ":" + range.to_string () + ":"
            + ":" + severity.to_string () + ":";
  }
}

public enum GVls.DiagnosticSeverity {
  UNKNOWN,
  /**
   * Reports an error.
   */
  ERROR,
  /**
   * Reports a warning.
   */
  WARNING,
  /**
   * Reports an information.
   */
  INFORMATION,
  /**
   * Reports a hint.
   */
  HINT;

  public string to_string () {
    string str = "";
    switch (this) {
      case ERROR:
        str = "Error";
        break;
      case WARNING:
        str = "Warning";
        break;
      case INFORMATION:
        str = "Information";
        break;
      case HINT:
        str = "Hint";
        break;
      case UNKNOWN:
        str = "Unknown";
        break;
    }
    return str;
  }
}

/**
 * Represents a related message and source code location for a diagnostic. This should be
 * used to point to code locations that cause or related to a diagnostics, e.g when duplicating
 * a symbol in a scope.
 */
public interface GVls.DiagnosticRelatedInformation : GLib.Object, GVo.Object {
  /**
   * The location of this related diagnostic information.
   */
  public abstract Location location { get; set; }

  /**
   * The message of this related diagnostic information.
   */
  public abstract string message { get; set; }
}

