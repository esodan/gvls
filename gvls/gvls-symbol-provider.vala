/* gvls-symbol-resolution-provider.vala
 *
 * Copyright 2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public errordomain GVls.SymbolProviderError {
    FILE_NOT_FOUND_ERROR
}

/**
 * Seeker for symbols in a set of {@link SymbolResolver} objects
 * or a database
 */
[Version (since="0.16.0")]
public interface GVls.SymbolProvider : GLib.Object
{
    /**
     * Resolvers in the provider
     */
    public abstract weak Target            target { get; }
    /**
     * Resolvers in the provider
     */
    public abstract GVo.Container              resolvers { get; set; }
    /**
     * Add a {@link GVls.SymbolResolver}
     */
    public abstract void
    add_resolver (SymbolResolver resolver);
    /**
     * Add a package from its name, as a new {@link GVls.SymbolResolver}
     */
    public abstract async void add_package (string name) throws GLib.Error;
    /**
     * Add a package from its name, as a new {@link GVls.SymbolResolver}
     */
    public abstract void add_package_sync (string name) throws GLib.Error;
    /**
     * Add source file, as a new {@link GVls.SymbolResolver}
     */
    public abstract async void add_file (GLib.File file) throws GLib.Error;
    /**
     * Add source file, as a new {@link GVls.SymbolResolver}
     */
    public abstract void add_file_sync (GLib.File file) throws GLib.Error;
    /**
     * Provides a hash table list of symbols in a given namespace
     */
    public abstract GVo.Container namespace_symbols (string name);
    /**
     * Add a set of {@link GVls.SymbolResolver}
     */
    public abstract async void add_resolvers (GLib.ListModel resolvers) throws GLib.Error;
    /**
     * Add synchronous a set of {@link GVls.SymbolResolver}
     */
    public abstract void add_resolvers_sync (GLib.ListModel resolvers) throws GLib.Error;
    /**
     * Check if there are a {@link GVls.SymbolResolver} for given
     * {@link GLib.File}
     */
    public abstract bool has_resolver (GLib.File file);
    /**
     * Search for a {@link SymbolResolver} with the given URI
     */
    public abstract GVls.SymbolResolver? resolver_by_uri (string uri);
    /**
     * Resolves a {@link GVls.Symbol} located at given
     * position using {@link GVls.Symbol.scope_range}
     */
    public abstract async Symbol? resolve_symbol_scope (string name,
                                                    string? uri,
                                                    GVls.Position? pos);
    /**
     * Resolves synchronously a {@link GVls.Symbol} located at given
     * position
     * 
     * Accepts names in the form '''symbol1.child''', returning 
     * child of symbol1, if no child with given name is found,
     * returns symbol1 if found.
     */
    public abstract Symbol? resolve_symbol_scope_sync (string name,
                                                    string? uri,
                                                    GVls.Position? pos);
    /**
     * Resolves synchronously a {@link GVls.Symbol} located at given
     * position
     *
     * Accepts names in the form '''symbol1.child''', returning
     * child of symbol1, if no child with given name is found,
     * returns symbol1 if found and initialize out parameters to
     * unsed string when searching and the word used on search.
     */
    public abstract Symbol?
    resolve_symbol_scope_sync_rest (string? name,
                                    string? uri,
                                    GVls.Position? pos,
                                    out string? rword,
                                    out string? rest);
    /**
     * Resolve a symbol with the given name or partial name,
     * URI and position. If no uri or possition are provided,
     * will search in all resolvers.
     */
    public abstract async Symbol? resolve_symbol (string name, string? uri);
    /**
     * Resolve a symbol with the given name or partial name,
     * URI and position. If no uri or possition are provided,
     * will search in all resolvers.
     */
    public abstract Symbol? resolve_symbol_sync (string name, string? uri);
    /**
     * Resolve a set of symbols related to the given name or partial name.
     */
    public abstract async GVo.Container resolve_symbols (string name);
    /**
     * Resolve synchronously a set of symbols related to the given name or partial name,
     * URI and position. If no URI or position is given, search along
     * on all other resolvers
     */
    public abstract GVo.Container resolve_symbols_sync (string name);
    /**
     * Resolve a set of symbols related to the given name or partial name,
     * URI, position and {@link Symbol.scope_range}. If no URI or position
     * is given, search along on all other resolvers
     */
    public abstract async GVo.Container resolve_symbols_scope (string name,
                                                    string? uri,
                                                    GVls.Position? pos);
    /**
     * Resolve synchronously a set of symbols related to the given name or partial name,
     * at URI and position
     */
    public abstract GVo.Container resolve_symbols_scope_sync (string name,
                                                    string? uri,
                                                    GVls.Position? pos);

    /**
     * Create a {@link CompletionList} for the given word, URI and
     * position
     */
    public abstract async CompletionList create_completion_information_list (string uri,
                                                                    Position pos);

    /**
     * Create a {@link CompletionList} for the given word, URI and
     * position
     */
    public abstract CompletionList
    create_completion_information_list_sync (string uri,
                                            Position pos);
    
    /**
     * Resolve a symbol at URI and position
     */
    public abstract async GVls.Symbol? resolve_symbol_position (string uri,
                                                            GVls.Position pos);
    /**
     * Resolve a symbol at URI and position
     */
    public abstract GVls.Symbol? resolve_symbol_position_sync (string uri,
                                                            GVls.Position pos);

    /**
     * From a given resolver's symbols at URI, update symbol database
     */
    [Version (since="20.0")]
    public abstract async void update_database_symbols (string uri) throws GLib.Error;
    /**
     * Given a variables's 'symbol, a file URI and a position in it, calculates
     * variable's type.
     */    
    public abstract GVls.Symbol? calculate_variable_type (GVls.Symbol sym,
                                                string? uri,
                                                Position? pos);

    /**
     * Calculates a method's name at given position,
     * useful for signature help
     */
    public virtual string calculate_position_method (string uri,
                                    ref GVls.Position position)
    {
        unowned string content = target.file_contents (uri);
        if (content == null || content == "") {
            return "";
        }
        unichar c = '\0';
        int index = 0;
        int l = 0;
        while (l < position.line) {
            if (content.get_next_char (ref index, out c)) {
                if (c == '\n') {
                    l++;
                }
            } else {
                break;
            }
        }
        int p = 0;
        while (p < position.character) {
            if (content.get_next_char (ref index, out c)) {
                if (c == '\n') {
                    break;
                }
            }
            p++;
        }
        
        
        GLib.StringBuilder sb = new GLib.StringBuilder ("");
        
        int pindex = index;
        bool started = false;
        while (content.get_prev_char (ref pindex, out c)) {
            if (c == '\n'
                || c == ','
                || c == ';'
                || c == ':'
                || c == ')'
                || c == ']'
                || c == '['
                || c == '+'
                || c == '-')
            {
                break;    
            }


            if (c.isspace () && !started) {
                position.character--;
                continue;
            }
            
            if (c.isspace () && started) {
                break;
            }
            
            if (c.isalnum () || c == '_') {
                started = true;
            }

            if (started) {
                sb.prepend_unichar (c);
            }
            
        }
        
        return sb.str;
    }

    /**
     * Detects a word at given position and file's URI
     */
    public virtual string
    calculate_position_word (string uri,
                            GVls.Position position,
                            out GVls.Range range)
    {
        range = new GVls.SourceRange ();
        range.start = new SourcePosition.from_values (position.line, position.character);
        range.end = new SourcePosition.from_values (position.line, position.character);

        string content = target.file_contents (uri);
        if (content == null || content == "") {
            return "";
        }

        Gee.ArrayList<int> rows = new Gee.ArrayList<int> ();

        unichar c = '\0';
        int index = 0;
        int l = 0;
        int lc = 0;
        while (l < position.line) {
            if (content.get_next_char (ref index, out c)) {
                if (c == '\n') {
                    l++;
                    rows.add (lc);
                    lc = 0;
                }
            } else {
                break;
            }
            lc++;
        }

        int p = 0;
        while (p < position.character) {
            if (content.get_next_char (ref index, out c)) {
                if (c == '\n') {
                    break;
                }
            }
            p++;
        }

        string word = "";

        int findex = index;
        while (content.get_next_char (ref findex, out c)) {
            if (c.isspace () || c == '\n'
                || c == '.'
                || c == ','
                || c == ';'
                || c == ':'
                || c == ')'
                || c == '('
                || c == ']'
                || c == '['
                || c == '>'
                || c == '<'
                || c == '='
                || c == '?'
                || c == '+'
                || c == '-')
            {
                break;    
            }

            word += c.to_string ();
            range.end.character++;
        }

        int pindex = index;
        while (content.get_prev_char (ref pindex, out c)) {
            if (word.has_prefix (".") && (c.isspace () || c == '\n')) {
                if (c == '\n') {
                    range.start.line--;
                    range.start.character = rows.get (range.start.line);
                }
                continue;
            } else if (c.isspace () || c == '\n') {
                break;
            }

            if (c == ','
                || c == ';'
                || c == ':'
                || c == ')'
                || c == '('
                || c == ']'
                || c == '['
                || c == '>'
                || c == '<'
                || c == '='
                || c == '?')
            {
                break;    
            }

            word = c.to_string () + word;
            range.start.character--;
        }
        
        if (word.has_suffix (".")) {
            word = word.substring (0, word.length - 1);
            range.end.character--;
        }

        return word;
    }
    
    /**
     * Calculate the definition of a symbol at specified location
     */
    public virtual GVls.Symbol?
    resolve_symbol_definition (string uri, GVls.Position pos) {
        return null;
    }

    // static methods

    public static GVls.SymbolInformationInfo? keyword (string key)
    {
        GVls.SymbolInformationInfo sym = new GVls.SymbolInformationInfo ();
        sym.kind = GVls.SymbolKind.KEY;
        switch (key) {
            case "abstract":
                sym.name = "abstract";
                break;
            case "as":
                sym.name = "as";
                break;
            case "async":
                sym.name = "async";
                break;
            case "base":
                sym.name = "base";
                break;
            case "break":
                sym.name = "break";
                break;
            case "case":
                sym.name = "case";
                break;
            case "catch":
                sym.name = "catch";
                break;
            case "class":
                sym.name = "class";
                break;
            case "const":
                sym.name = "const";
                break;
            case "construct":
                sym.name = "construct";
                break;
            case "continue":
                sym.name = "continue";
                break;
            case "default":
                sym.name = "default";
                break;
            case "delegate":
                sym.name = "delegate";
                break;
            case "delete":
                sym.name = "delete";
                break;
            case "do":
                sym.name = "do";
                break;
            case "dynamic":
                sym.name = "dynamic";
                break;
            case "else":
                sym.name = "else";
                break;
            case "enum":
                sym.name = "enum";
                break;
            case "ensures":
                sym.name = "ensures";
                break;
            case "errordomain":
                sym.name = "errordomain";
                break;
            case "extern":
                sym.name = "extern";
                break;
            case "false":
                sym.name = "false";
                sym.kind = GVls.SymbolKind.CONSTANT;
                break;
            case "finaly":
                sym.name = "finaly";
                break;
            case "for":
                sym.name = "for";
                break;
            case "foreach":
                sym.name = "foreach";
                break;
            case "get":
                sym.name = "get";
                break;
            case "if":
                sym.name = "if";
                break;
            case "in":
                sym.name = "in";
                break;
            case "inline":
                sym.name = "inline";
                break;
            case "interface":
                sym.name = "interface";
                break;
            case "internal":
                sym.name = "internal";
                break;
            case "is":
                sym.name = "is";
                break;
            case "lock":
                sym.name = "lock";
                break;
            case "namespace":
                sym.name = "namespace";
                break;
            case "new":
                sym.name = "new";
                break;
            case "null":
                sym.name = "null";
                sym.kind = GVls.SymbolKind.NULL;
                break;
            case "out":
                sym.name = "out";
                break;
            case "override":
                sym.name = "override";
                break;
            case "owned":
                sym.name = "owned";
                break;
            case "params":
                sym.name = "params";
                break;
            case "private":
                sym.name = "private";
                break;
            case "protected":
                sym.name = "protected";
                break;
            case "public":
                sym.name = "public";
                break;
            case "ref":
                sym.name = "ref";
                break;
            case "requires":
                sym.name = "requires";
                break;
            case "return":
                sym.name = "return";
                break;
            case "sealed":
                sym.name = "sealed";
                break;
            case "set":
                sym.name = "set";
                break;
            case "signal":
                sym.name = "signal";
                break;
            case "sizeof":
                sym.name = "sizeof";
                break;
            case "static":
                sym.name = "static";
                break;
            case "struct":
                sym.name = "struct";
                break;
            case "switch":
                sym.name = "switch";
                break;
            case "this":
                sym.name = "this";
                break;
            case "throw":
                sym.name = "throw";
                break;
            case "throws":
                sym.name = "throws";
                break;
            case "true":
                sym.name = "true";
                break;
            case "try":
                sym.name = "try";
                break;
            case "typeof":
                sym.name = "typeof";
                break;
            case "unowned":
                sym.name = "unowned";
                break;
            case "using":
                sym.name = "using";
                break;
            case "var":
                sym.name = "var";
                break;
            case "virtual":
                sym.name = "virtual";
                break;
            case "void":
                sym.name = "void";
                break;
            case "volatile":
                sym.name = "volatile";
                break;
            case "weak":
                sym.name = "weak";
                break;
            case "while":
                sym.name = "while";
                break;
            case "yield":
                sym.name = "yield";
                break;
            default:
                return null;
        }

        return sym;
    }

    /**
     * Calculates a word at given position and string,
     * taking all on left and rigth.
     */
    public static string
    calculate_position_word_string (string content,
                            GVls.Position position,
                            out GVls.Range range)
    {
        range = new GVls.SourceRange ();
        range.start = new SourcePosition.from_values (position.line, position.character);
        range.end = new SourcePosition.from_values (position.line, position.character);

        if (content == null || content == "") {
            return "";
        }

        Gee.ArrayList<int> rows = new Gee.ArrayList<int> ();

        unichar c = '\0';
        int index = 0;
        int l = 0;
        int lc = 0;
        while (l < position.line) {
            if (content.get_next_char (ref index, out c)) {
                if (c == '\n') {
                    l++;
                    rows.add (lc);
                    lc = 0;
                }
            } else {
                break;
            }
            lc++;
        }

        int p = 0;
        while (p < position.character) {
            if (content.get_next_char (ref index, out c)) {
                if (c == '\n') {
                    break;
                }
            }
            p++;
        }

        string word = "";

        int findex = index;
        while (content.get_next_char (ref findex, out c)) {
            if (c.isspace () || c == '\n'
                || c == '.'
                || c == ','
                || c == ';'
                || c == ':'
                || c == ')'
                || c == '('
                || c == ']'
                || c == '['
                || c == '>'
                || c == '<'
                || c == '='
                || c == '?'
                || c == '+'
                || c == '-')
            {
                break;
            }

            word += c.to_string ();
            range.end.character++;
        }

        int pindex = index;
        while (content.get_prev_char (ref pindex, out c)) {
            if (word.has_prefix (".") && (c.isspace () || c == '\n')) {
                if (c == '\n') {
                    range.start.line--;
                    range.start.character = rows.get (range.start.line);
                }
                continue;
            } else if (c.isspace () || c == '\n') {
                break;
            }

            if (c == ','
                || c == ';'
                || c == ':'
                || c == ')'
                || c == '('
                || c == ']'
                || c == '['
                || c == '>'
                || c == '<'
                || c == '='
                || c == '?')
            {
                break;
            }

            word = c.to_string () + word;
            range.start.character--;
        }

        if (word.has_suffix (".")) {
            word = word.substring (0, word.length - 1);
            range.end.character--;
        }

        return word;
    }

    /**
     * Calculates a word at given position and string
     * taking all on left.
     *
     * @param content text used to find string at position
     * @param position location in the text
     * @param range sets it to the range where the string was located
     * @param tab_width Tab width used to calculate position
     */
    public static string
    calculate_position_word_left (string content,
                            GVls.Position position,
                            out GVls.Range range,
                            int tab_width = 2)
    {
        range = new GVls.SourceRange ();
        range.start = new SourcePosition.from_values (position.line, position.character);
        range.end = new SourcePosition.from_values (position.line, position.character);

        if (content == null || content == "") {
            return "";
        }

        Gee.ArrayList<int> rows = new Gee.ArrayList<int> ();

        unichar c = '\0';
        int index = 0;
        int l = 0;
        int lc = 0;
        while (l < position.line) {
            if (content.get_next_char (ref index, out c)) {
                if (c == '\n') {
                    l++;
                    rows.add (lc);
                    lc = 0;
                }
            } else {
                break;
            }
            lc++;
        }

        int p = 0;
        while (p < position.character) {
            if (content.get_next_char (ref index, out c)) {
                if (c == '\n') {
                    break;
                }
                if (c == '\t') {
                    p = p + tab_width;
                    continue;
                }
            }
            p++;
        }

        string word = "";

        int pindex = index;
        while (content.get_prev_char (ref pindex, out c)) {
            if (word.has_prefix (".") && (c.isspace () || c == '\n')) {
                if (c == '\n') {
                    range.start.line--;
                    range.start.character = rows.get (range.start.line);
                }
                continue;
            } else if (c.isspace () || c == '\n') {
                break;
            }

            if (c == ','
                || c == ';'
                || c == ':'
                || c == ')'
                || c == '('
                || c == ']'
                || c == '['
                || c == '>'
                || c == '<'
                || c == '='
                || c == '?')
            {
                break;
            }

            word = c.to_string () + word;
            range.start.character--;
        }

        return word;
    }
}


