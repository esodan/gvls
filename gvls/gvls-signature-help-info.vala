/* gvls-signature-help-info.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Signature help represents the signature of something
 * callable. There can be multiple signature but only one
 * active and only one active parameter.
 */
[Version(since="0.16")]
public class GVls.SignatureHelpInfo : GLib.Object,
                                    SignatureHelp,
                                    GVo.Object
{
    [Description(nick="signatures")]
    public GVo.Container signatures { get; set; }

    [Description(nick="activeSignature")]
    public int active_signature { get; set; }

    [Description(nick="activeParameter")]
    public int active_parameter { get; set; }

    construct {
        signatures = new GVo.ContainerHashList.for_type (typeof (SignatureInformationInfo));
    }
}

