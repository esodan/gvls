/* gvls-signature-help-params.vala
 *
 * Copyright 2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

[Version(since="0.16")]
public interface GVls.SignatureHelpParams : GLib.Object,
                                TextDocumentPositionParams,
                                WorkDoneProgressParams
{
	/**
	 * The signature help context. This is only available if the client specifies
	 * to send this using the client capability  `textDocument.signatureHelp.contextSupport === true`
	 *
	 * @since 3.15.0
	 */
	[Description(nick="context")]
	public abstract SignatureHelpContext? context { get; set; }
}

