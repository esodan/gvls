/* gvlsp-client-std.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Jsonrpc;
using GLib;
using GVls;

/**
 * A {@link GVls.Client} specializing {@link ClientJsonrpc} client
 * to be used over input and output streams.
 *
 * Useful when input and output streams represent the standard input and output.
 *
 * By default only UTF-8 strings are supported; default utf-16 are not implemented
 * currently.
 */
public class GVls.ClientStream : GVls.ClientJsonrpc {
    construct {
        capabilities.general.position_encodings.add (new GVo.String.for_string ("utf-8"));
    }

    public ClientStream.from_streams (GLib.InputStream istream, GLib.OutputStream ostream) {
        connect_from_streams (istream, ostream);
    }

    /**
     * Connect a {@link GVls.ClientStream} to a server using
     * the given input ant output streams.
     *
     * Set given streams to standard input and output in order
     * to communicate using them. Standard streams can be attached
     * to a {@link GLib.Subprocess} to communicate to a
     * json rpc server.
     */
    public void connect_from_streams (GLib.InputStream istream, GLib.OutputStream ostream) {
        var ios = new GLib.SimpleIOStream (istream, ostream);
        accept_io_stream (ios);
    }
}
