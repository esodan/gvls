/* gvls-glocation.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Implementation of {@link Location}
 */
public class GVls.SourceLocation : GLib.Object, GVo.Object, Location
{

  // Location
  [Description (nick="uri")]
  public string uri { get; set;  }
  [Description (nick="range")]
  public Range range { get; set;  }

  construct {
    uri = "file://";
    range = new SourceRange ();
  }

  public SourceLocation.from_values (string uri, int start_line, int start_column, int end_line, int end_column) {
    _uri = uri;
    var start = new SourcePosition.from_values (start_line, start_column);
    var end = new SourcePosition.from_values (end_line, end_column);
    range.start = start;
    range.end = end;
  }

  [Version (since="0.16")]
  public string to_string () {
      return ("%s:%d.%d-%d.%d".printf (uri,
                                    range.start.line,
                                    range.start.character,
                                    range.end.line,
                                    range.end.character));
  }
}


/**
 * Implementation of {@link Position}
 */
public class GVls.SourcePosition : GLib.Object, GVo.Object, Position {
    [Description(nick="line")]
    public int line { get; set; }

    [Description(nick="character")]
    public int character { get; set; }

    construct {
        line = -1;
        character = -1;
    }

    public SourcePosition.from_values (int line, int character) {
        this.line = line;
        this.character = character;
    }
}

/**
 * Implementation of {@link Range}
 */
public class GVls.SourceRange : GLib.Object, GVo.Object, Range {

    [Description (nick="start")]
    public Position start { get; set; }
    [Description (nick="end")]
    public Position end { get; set; }

    construct {
        start = new SourcePosition ();
        end = new SourcePosition ();
    }

    public SourceRange.from_values (Position start, Position end) {
        this.start = start;
        this.end = end;
    }
}
