/* gvls-symbol-information-info.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Implementation of {@link SymbolInformation}
 */
public class GVls.SymbolInformationInfo : GLib.Object,
                                            GVo.Object,
                                            SymbolInformation,
                                            GVo.ContainerHashable
{
    [Description(nick="name")]
    internal string name { get; set; }

    [Description(nick="kind")]
    internal SymbolKind kind { get; set; }

    [Description(nick="deprecated")]
    internal bool deprecated { get; set; }

    [Description(nick="location")]
    internal Location location { get; set; }

    [Description(nick="containerName")]
    internal string? container_name { get; set; }

    construct {
        name = "";
        location = new SourceLocation ();
    }

    public SymbolInformationInfo.from_symbol (GVls.Symbol symbol) {
        name = symbol.name;
        if (symbol.location != null) {
            location = symbol.location;
        }
        kind = symbol.symbol_kind;
        container_name = symbol.container_name;
    }
    
    // ContainerHashable
    internal uint hash () {
        return GLib.str_hash (name+"::"+location.to_string ());
    }
    internal bool equal (GLib.Object obj) {
        if (!(obj is SymbolInformation)) {
            return false;
        }

        var inf = (SymbolInformation) obj;

        return (name == inf.name && location.to_string () == inf.location.to_string ());
    }
}
