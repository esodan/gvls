/* gvls-document-symbol.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * Represents programming constructs like variables, classes, interfaces etc. that appear in a document. Document symbols can be
 * hierarchical and they have two ranges: one that encloses its definition and one that points to its most interesting range,
 * e.g. the range of an identifier.
 */
public interface GVls.DocumentSymbol : GLib.Object, GVo.Object {
	/**
	 * The name of this symbol.
	 */
	public abstract string name { get; }

	/**
	 * More detail for this symbol, e.g the signature of a function.
	 */
	public abstract string? detail { get; }

	/**
	 * The kind of this symbol.
	 */
	public abstract SymbolKind kind { get; }

	/**
	 * Indicates if this symbol is deprecated.
	 */
	public abstract bool deprecated { get; }

	/**
	 * The range enclosing this symbol not including leading/trailing whitespace but everything else
	 * like comments. This information is typically used to determine if the clients cursor is
	 * inside the symbol to reveal in the symbol in the UI.
	 */
	public abstract Range range { get; }

	/**
	 * The range that should be selected and revealed when this symbol is being picked, e.g the name of a function.
	 * Must be contained by the `range`.
	 */
	public abstract Range selection_range { get; }

	/**
	 * Children of this symbol, e.g. properties of a class.
	 *
	 * Container's items are of type {@link DocumentSymbol}
	 */
	public abstract GVo.Container children { get; }
}

