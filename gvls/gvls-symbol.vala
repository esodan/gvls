/* gvls-symbol.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.Symbol : GLib.Object,
                            GLib.ListModel,
                            GVo.Container
{
  public abstract Location location { get; }
  public abstract Package package { get; }
  public abstract Documentation documentation { get; }
  public abstract SymbolKind symbol_kind { get; }
  /**
   * Symbol's scope where it was defined
   */
  [Version (since="0.16")]
  public abstract GVls.Range scope_range { get; }
  public abstract string name { get; }
  public abstract string full_name { get; }
  public abstract string data_type { get; }
  public abstract bool deprecated { get; }
  public abstract string container_name { get; }
  [Version(since="0.16.0")]
  public abstract string detail { get; }
  public virtual Symbol? get_child (string name) { return null; }
  public virtual void add_child (Symbol sym) {}

  /**
   * Calculates if a symbol is in an specific
   * {@link GVls.Location}
   */
  public virtual bool is_at (Location loc) {
    if (location.uri != loc.uri) {
      return false;
    }
    if (location.range.start.line < 0
        || location.range.end.line < 0
        || location.range.end.character < 0
        || location.range.end.line < 0) {
      return false;
    }
    if (loc.range.start.line < 0
        || loc.range.end.line < 0
        || loc.range.end.character < 0
        || loc.range.end.line < 0) {
      return false;
    }
    if (loc.range.start.line != location.range.start.line) {
      return false;
    }
    if (loc.range.start.character < location.range.start.character
        || loc.range.end.character > location.range.end.character) {
      return false;
    }
    return true;
  }

    /**
    * Calculates if a symbol is in an specific
    * {@link GVls.Range}
    *
    * If the given range or symbol's loation's range
    * has negative start and end
    * line or character numbers, is suppose to be
    * a project scope range, so this method returns TRUE.
    */
    public virtual bool is_at_range (GVls.Range range) {
        if (location.range.start.line < 0
            || location.range.end.line < 0
            || location.range.end.character < 0
            || location.range.end.line < 0) {

            return true;
        }

        if (range.start.line < 0
            || range.end.line < 0
            || range.end.character < 0
            || range.end.line < 0) {

            return true;
        }

        if (location.range.start.line < range.start.line) {
            return false;
        }

        if (location.range.end.line > range.end.line) {
            return false;
        }

        if (range.start.line == range.end.line) {
            if (range.start.character < location.range.start.character
                || range.end.character > location.range.end.character) {

                return false;
            }
        }

        return true;
    }


  /**
   * Search a child with the given name or path, case sensitive
   */
  public virtual GVls.Symbol? search_child (string name)
  {
    return null;
  }
  /**
   * Search a child with the given name or path, case sensitive
   */
  public virtual GVls.Symbol? search_child_rest (string name, out string? rest)
  {
    rest = "";
    return null;
  }
  /**
   * Search all children considering given string path.
   * If given, part is set to the unused suffix of the string
   */
  public virtual GVo.Container search_children (string str,
                                            out string part)
  {
    part = "";
    return new GVo.ContainerHashList ();
  }
  /**
   * Creates a List of completion items for this symbol, filtering using
   * given string
   */
  public virtual CompletionList create_completion_list (string part) {
      return new GVls.CompletionListInfo ();
  }
  /**
   * Calculate symbol's data type for unknown ones, if it is known, return
   * does nothing.
   */
  public virtual void infer_data_type (GVls.SymbolProvider? provider = null) {}
  
  /**
   * Obtain a method signature.
   *
   * If the symbol is not a method, return empty data.
   */
  public virtual SignatureInformation method_signature () {
        return new SignatureInformationInfo ();
  }

  /**
   * Obtain a hover information, provinding a
   * {@link GVls.MarkupContent}
   */
  public virtual Hover hover (GVls.SymbolProvider? resolver = null) {
        return hover_markup (resolver);
  }

  /**
   * Obtain a hover information, provinding a
   * plain text format
   */
  public virtual Hover hover_plain (GVls.SymbolProvider? resolver = null) {
        return new HoverInfo ();
  }

  /**
   * Obtain a {@link MarkedString}
   * in a hover information using CommonMark
   * specification syntax
   */
  public virtual Hover hover_markup (GVls.SymbolProvider? provider = null) {
        return new HoverInfo ();
  }

    /**
     * Calculates if the current symbol is a
     * Local Variable
     */
    public virtual bool is_local_variable () {
        return false;
    }

    /**
     * Resolve the data type referenced by
     * {@link data_type} name
     */
    public virtual void resolve_data_type (GVls.SymbolProvider provider) {}
    /**
     * Resolve the {@link GVls.Symbol} of the type this symbol belongs to
     */
    public virtual GVls.Symbol resolve_symbol_data_type (GVls.SymbolProvider provider)
    {
        return this;
    }

    /**
     * Resolve the {@link GVls.Symbol} definition of this symbol
     */
    public virtual GVls.Symbol resolve_symbol_definition (GVls.SymbolProvider provider)
    {
        return this;
    }
}
