/* gvls-server-capabilities.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.ServerCapabilities : GLib.Object, GVo.Object {
    /**
	 * The position encoding the server picked from the encodings offered
	 * by the client via the client capability `general.positionEncodings`.
	 *
	 * If the client didn't provide any position encodings the only valid
	 * value that a server can return is 'utf-16'.
	 *
	 * If omitted it defaults to 'utf-16'.
	 *
	 * @since 3.17.0
	 */
    [Version(since="21.0")]
    [Description (nick="positionEncoding")]
	public abstract string? position_encoding { get; set; }

  /**
   * Defines how text documents are synced. Is either a detailed structure defining each notification or
   * for backwards compatibility the TextDocumentSyncKind number. If omitted it defaults to `TextDocumentSyncKind.None`.
   */
  [Description (nick="textDocumentSync")]
  public abstract TextDocumentSyncOptions text_document_sync { get; set; }
  /**
   * The server provides hover support.
   */
  [Description (nick="hoverProvider")]
  [Version(since="0.16")]
  public abstract GLib.Variant hover_provider { get; set; }
  /**
   * The server provides completion support.
   */
  [Description (nick="completionProvider")]
  public abstract CompletionOptions completion_provider { get; set; }
  /**
   * The server provides signature help support.
   */
  [Description (nick="signatureHelpProvider")]
  public abstract SignatureHelpOptions signature_help_provider { get; set; }
  /**
   * The server provides goto definition support.
   */
  [Description (nick="definitionProvider")]
  public abstract bool definition_provider { get; set; }
  /**
   * The server provides Goto Type Definition support.
   *
   * Can be boolean | (TextDocumentRegistrationOptions & StaticRegistrationOptions)
   * Since 3.6.0
   */
  [Description (nick="typeDefinitionProvider")]
  public abstract bool type_definition_provider { get; set; }
  /**
   * The server provides Goto Implementation support.
   *
   * Can be boolean | (TextDocumentRegistrationOptions & StaticRegistrationOptions)
   *
   * Since 3.6.0
   */
  [Description (nick="implementationProvider")]
  public abstract bool implementation_provider { get; set; }
  /**
   * The server provides find references support.
   */
  [Description (nick="referencesProvider")]
  public abstract bool references_provider { get; set; }
  /**
   * The server provides document highlight support.
   */
  [Description (nick="documentHighlightProvider")]
  public abstract bool document_highlight_provider { get; set; }
  /**
   * The server provides document symbol support.
   */
  [Description (nick="documentSymbolProvider")]
  public abstract bool document_symbol_provider { get; set; }
  /**
   * The server provides workspace symbol support.
   */
  [Description (nick="workspaceSymbolProvider")]
  public abstract bool workspace_symbol_provider { get; set; }
  /**
   * The server provides code actions. The `CodeActionOptions` return type is only
   * valid if the client signals code action literal support via the property
   * `textDocument.codeAction.codeActionLiteralSupport`.
   *
   * Can be  boolean | CodeActionOptions
   */
  [Description (nick="codeActionProvider")]
  public abstract bool code_action_provider { get; set; }
  /**
   * The server provides code lens.
   */
  [Description (nick="codeLensProvider")]
  public abstract CodeLensOptions code_lens_provider { get; set; }
  /**
   * The server provides document formatting.
   */
  [Description (nick="documentFormattingProvider")]
  public abstract bool document_formatting_provider { get; set; }
  /**
   * The server provides document range formatting.
   */
  [Description (nick="documentRangeFormattingProvider")]
  public abstract bool document_range_formatting_provider { get; set; }
  /**
   * The server provides document formatting on typing.
   */
  [Description (nick="documentOnTypeFormattingProvider")]
  public abstract DocumentOnTypeFormattingOptions documen_on_type_formatting_provider { get; set; }
  /**
   * The server provides rename support. RenameOptions may only be
   * specified if the client states that it supports
   * `prepareSupport` in its initial `initialize` request.
   *
   * Can be boolean | RenameOptions
   */
  [Description (nick="renameProvider")]
  public abstract bool rename_provider { get; set; }
  /**
   * The server provides document link support.
   */
  [Description (nick="documentLinkProvider")]
  public abstract DocumentLinkOptions document_link_provider { get; set; }
  /**
   * The server provides color provider support.
   *
   * Can be boolean | ColorProviderOptions | (ColorProviderOptions & TextDocumentRegistrationOptions & StaticRegistrationOptions)
   *
   * Since 3.6.0
   */
  [Description (nick="colorProvider")]
  public abstract bool color_provider { get; set; }
  /**
   * The server provides folding provider support.
   *
   * Can be boolean | FoldingRangeProviderOptions | (FoldingRangeProviderOptions & TextDocumentRegistrationOptions & StaticRegistrationOptions)
   *
   * Since 3.10.0
   */
  [Description (nick="foldingRangeProvider")]
  public abstract bool folding_range_provider { get; set; }
  /**
   * The server provides execute command support.
   */
  [Description (nick="executeCommandProvider")]
  public abstract ExecuteCommandOptions execute_command_provider { get; set; }
  /**
   * Workspace specific server capabilities
   */
  [Description (nick="workspace")]
  public abstract Workspace workspace { get; set; }
  public class Workspace : GLib.Object, GVo.Object {
    /**
     * The server supports workspace folder.
     *
     * Since 3.6.0
     */
    [Description (nick="workspaceFolders")]
    public WorkspaceFolders workspace_folders { get; set; }
    public class WorkspaceFolders : GLib.Object, GVo.Object {
      /**
      * The server has support for workspace folders
      */
      [Description (nick="supported")]
      public bool supported { get; set; }
      /**
      * Whether the server wants to receive workspace folder
      * change notifications.
      *
      * If a strings is provided the string is treated as a ID
      * under which the notification is registered on the client
      * side. The ID can be used to unregister for these events
      * using the `client/unregisterCapability` request.
      *
      * Can be string as a {@link GVo.String} | boolean
      */
      [Description (nick="changeNotifications")]
      public bool change_notifications { get; set; }
    }
    construct {
      workspace_folders = new WorkspaceFolders ();
    }
  }
  /**
   * Experimental server capabilities.
   */
  [Description (nick="experimental")]
  public abstract GVo.Container experimental { get; set; }
}
