/* gvls-failure-handling-kind-object.vala
 *
 * Copyright 2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class GVls.FailureHandlingKindObject : GVls.EnumObject, GVo.Object  {
  construct {
    enum_type = typeof (FailureHandlingKind);
  }
  public FailureHandlingKindObject (FailureHandlingKind kind) {
    val = kind;
  }
  public FailureHandlingKind get_enum () { return (FailureHandlingKind) val; }
  public override string to_string () { return ((FailureHandlingKind) val).to_string (); }

  internal GLib.Variant to_variant () {
    return new GLib.Variant.string (get_enum ().to_string ());
  }

  internal void parse_variant (GLib.Variant v) {
    val = (int) FailureHandlingKind.from_string (v.get_string ());
  }
}
