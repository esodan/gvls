/* gvls-formater.vala
 *
 * Copyright 2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public errordomain GVls.FormaterError {
    INVALID_NODE_ERROR,
    INVALID_STRING_ERROR,
    FILE_ERROR,
    CONFIG_ERROR
}


/**
 * An interface to provide code formating capabilites
 */
public interface GVls.Formater : GLib.Object
{
    /**
     * Takes a file contents and returns the same
     * but formatted, according with the configuration
     */
    public virtual async string
    format_content (string content, GVls.FormattingOptions options) throws GLib.Error
    {
        return format_content_sync (content, options);
    }
    /**
     * Takes a file contents and returns the same
     * but formatted, according with the configuration
     */
    public abstract string
    format_content_sync (string content, GVls.FormattingOptions options) throws GLib.Error;
    /**
     * Takes a range in a contents and format it
     * returning the required changes to the file
     */
    public virtual async string?
    format_content_range (string content, GVls.Range range, GVls.FormattingOptions options) throws GLib.Error
    {
        return format_content_range_sync (content, range, options);
    }
    /**
     * Takes a range in a contents and format it
     * returning the required changes to the file
     */
    public virtual string?
    format_content_range_sync (string content, GVls.Range range, GVls.FormattingOptions options) throws GLib.Error
    {
        return null;
    }
}
