/* gvls-code-lens-info.vala
 *
 * Copyright 2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * A code lens represents a command that should be shown along with
 * source text, like the number of references, a way to run tests, etc.
 *
 * A code lens is _unresolved_ when no command is associated to it. For performance
 * reasons the creation of a code lens and resolving should be done in two stages.
 */
public class GVls.CodeLensInfo : GLib.Object, CodeLens, GVo.Object {
	/**
	 * The range in which this code lens is valid. Should only span a single line.
	 */
    [Description (nick="range")]
	internal Range range { get; set; }

	/**
	 * The command this code lens represents.
	 */
    [Description (nick="command")]
	internal Command? command { get; set; }

	/**
	 * A data entry field that is preserved on a code lens item between
	 * a code lens and a code lens resolve request.
	 */
    [Description (nick="data")]
	internal GVo.Object? data { get; set; }
}
