/* gvls-symbol-resolver.vala
 *
 * Copyright 2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Symbol resolver for a given {@link GLib.File}
 */
[Version (since="0.16.0")]
public interface GVls.SymbolResolver : GLib.Object
{
    /**
     * Parent {@link GVls.SymbolProvider} this resolver
     * belongs to.
     */
    public abstract GVls.SymbolProvider symbol_provider { get; construct set; }
    /**
     * Parsed file providing {@link GVls.Symbol} objects
     */
    public abstract GLib.File file { get; }
    /**
     * A list of symbols found
     */
    public abstract GVo.Container symbols ();
    /**
    * Returns all symbols in the document as a list of
    * {@link DocumentSymbol}
    */
    public abstract GVo.Container document_symbols ();
    /**
    * Returns all symbols in the document as a list of
    * {@link SymbolInformation}
    */
    public virtual GVo.Container symbol_informations () {
        var sis = new GVo.ContainerHashList.for_type (typeof (GVls.SymbolInformationInfo));
        var syms = symbols ();
        for (int i = 0; i < syms.get_n_items (); i++) {
            GVls.Symbol it = syms.get_item (i) as Symbol;
            if (it == null) {
                continue;
            }

            if (it.name.contains ("@")
                || it.name == "this"
                || it.name.has_prefix ("."))
            {
                continue;
            }

            var ds = new SymbolInformationInfo.from_symbol (it);
            sis.add (ds);
            child_information_symbols (it, sis);
        }

        return sis;
    }
    /**
     * Find a symbol with the given name.
     * 
     * Is accepted the form symbol1.child, returning
     * child of symbol1.
     */
    public virtual async GVls.Symbol? resolve (string name)
    {
        return resolve_sync (name);
    }
    /**
     * Find a symbol with the given name.
     * 
     * Is accepted the form symbol1.child, returning
     * child of symbol1.
     */
    public abstract GVls.Symbol? resolve_sync (string name);
    /**
     * Find a symbol with the given name.
     * 
     * Is accepted the form symbol1.child, returning
     * child of symbol1.
     */
    public virtual async GVls.Symbol? resolve_scope (string name, Position pos) {
        return resolve_scope_sync (name, pos);
    }
    /**
     * Find a symbol with the given name.
     * 
     * Is accepted the form symbol1.child, returning
     * child of symbol1.
     */
    public abstract GVls.Symbol? resolve_scope_sync (string name, Position pos);
    /**
     * Suggest a list of symbols based on the string and position.
     */
    public virtual async GVo.Container suggest (string str)
    {
        return suggest_sync (str);
    }
    /**
     * Suggest a list of symbols based on the string.
     */
    public abstract GVo.Container suggest_sync (string str);

    /**
     * Find the block's parent class symbol
     */
    public virtual GVls.Symbol? find_parent_at (GVls.Position pos) {
        return null;
    }

    /**
     * Find all references to a local variable, defined in an specific
     * scope, like a block.
     *
     * Returns a list of all symbols found of the type {@link GVls.Symbol}
     */
    public virtual GVo.Container
    find_local_variable_references (GVls.Symbol sym)
    {
        return new GVo.ContainerHashList ();
    }

    /**
     * Find all references to an {@link GVls.Symbol} in this symbol
     * symbol resolver scope.
     *
     * Returns a list of all symbols found of the type {@link GVls.Symbol}
     */
    public virtual GVo.Container
    find_symbol_references (GVls.Symbol sym)
    {
        return new GVo.ContainerHashList ();
    }

    // Static Members

    private static void child_information_symbols (Symbol sym, GVo.Container l) {
        for (int i = 0; i < sym.get_n_items (); i++) {
            var it = sym.get_item (i) as Symbol;
            if (it == null) {
                continue;
            }

            if (it.name.contains ("@")
                || it.name == "this"
                || it.name.has_prefix ("."))
            {
                continue;
            }

            var ds = new SymbolInformationInfo.from_symbol (it);
            l.add (ds);
            for (int j = 0; j < ((GVo.Container) it).get_n_items (); j++) {
                var itl = ((GVo.Container) it).get_item (j) as Symbol;
                if (itl == null) {
                    continue;
                }

                if (it.name.contains ("@")
                    || it.name == "this"
                    || it.name.has_prefix ("."))
                {
                    continue;
                }

                var dsl = new SymbolInformationInfo.from_symbol (itl);
                l.add (dsl);
                child_information_symbols (itl, l);
            }
        }
    }
}


