/* gvls-workspace.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.DocumentFormattingParams : GLib.Object,
                                            GVls.WorkDoneProgressParams,
                                            GVo.Object
{
  /**
   * The document to format.
   */
  [Description (nick="textDocument")]
  public abstract TextDocumentIdentifier text_document { get; set; }

  /**
   * The format options.
   */
  [Description (nick="options")]
  public abstract FormattingOptions options { get; set; }
}


public class GVls.DocumentFormattingParamsInfo : GLib.Object,
                                            GVo.Object,
                                            GVls.WorkDoneProgressParams,
                                            GVls.DocumentFormattingParams
{
    /**
     * The document to format.
     */
    [Description(nick="textDocument")]
    internal TextDocumentIdentifier text_document { get; set; }

    /**
     * The format options.
     s*/
    [Description(nick="options")]
    internal FormattingOptions options { get; set; }

    [Description (nick="workDoneToken")]
    internal GLib.Variant? work_done_token { get; set; }

    construct {
        text_document = new GVls.TextDocumentIdentifierInfo ();
        options = new GVls.FormattingOptionsInfo ();
    }
}

/**
 * Value-object describing what options formatting should use.
 */
public interface GVls.FormattingOptions : GLib.Object {
    /**
     * Size of a tab in spaces.
     */
    [Description (nick="tabSize")]
    public abstract int tab_size { get; set; }

    /**
     * Prefer spaces over tabs.
     */
    [Description (nick="insertSpaces")]
    public abstract bool insert_spaces { get; set; }
    /**
	 * Trim trailing whitespace on a line.
	 *
	 * @since 3.15.0
	 */
	[Description(nick="trimTrailingWhitespace")]
	public abstract bool trim_trailing_whitespace { get; set; }
	/**
	 * Insert a newline character at the end of the file if one does not exist.
	 *
	 * @since 3.15.0
	 */
	[Description(nick="insertFinalNewline")]
	public abstract bool insert_final_newline { get; set; }

	/**
	 * Trim all newlines after the final newline at the end of the file.
	 *
	 * @since 3.15.0
	 */
	[Description(nick="trimFinalNewlines")]
	public abstract bool trim_final_newlines { get; set; }

	/**
	 * Signature for further properties.
	 *
	 * [key: string]: boolean | number | string;
	 */
	public abstract GLib.Variant? properties { get; set; }
}

public class GVls.FormattingOptionsInfo : GLib.Object,
                                        GVo.Object,
                                        GVls.FormattingOptions
{
    /**
    * Size of a tab in spaces.
    */
	[Description(nick="tabSize")]
    internal int tab_size { get; set; }

    /**
    * Prefer spaces over tabs.
    */
	[Description(nick="insertSpaces")]
    internal bool insert_spaces { get; set; }
    /**
	 * Trim trailing whitespace on a line.
	 *
	 * @since 3.15.0
	 */
	[Description(nick="trimTrailingWhitespace")]
	internal bool trim_trailing_whitespace { get; set; }
	/**
	 * Insert a newline character at the end of the file if one does not exist.
	 *
	 * @since 3.15.0
	 */
	[Description(nick="insertFinalNewline")]
	internal bool insert_final_newline { get; set; }

	/**
	 * Trim all newlines after the final newline at the end of the file.
	 *
	 * @since 3.15.0
	 */
	[Description(nick="trimFinalNewlines")]
	internal bool trim_final_newlines { get; set; }

	/**
	 * Signature for further properties.
	 *
	 * [key: string]: boolean | number | string;
	 */
	[Description(nick="properties")]
	internal GLib.Variant? properties { get; set; }
}

public interface GVls.DocumentRangeFormattingParams : GLib.Object,
                                            GVo.Object,
                                            GVls.WorkDoneProgressParams
{
  /**
   * The document to format.
   */
  [Description(nick="textDocument")]
  public abstract TextDocumentIdentifier text_document { get; set; }

  /**
   * The range to format
   */
  [Description(nick="range")]
  public abstract Range range { get; set; }

  /**
   * The format options
   */
  [Description(nick="options")]
  public abstract FormattingOptions options { get; set; }
}


public class GVls.DocumentRangeFormattingParamsInfo : GLib.Object,
                                            GVo.Object,
                                            GVls.WorkDoneProgressParams,
                                            GVls.DocumentRangeFormattingParams
{
    /**
     * The document to format.
     */
    [Description(nick="textDocument")]
    internal TextDocumentIdentifier text_document { get; set; }

    /**
     * The range to format
     */
    [Description(nick="range")]
    internal Range range { get; set; }

    /**
     * The format options
     */
    [Description(nick="options")]
    internal FormattingOptions options { get; set; }


    [Description (nick="workDoneToken")]
	internal Variant? work_done_token { get; set; }

	construct {
	    text_document = new GVls.TextDocumentIdentifierInfo ();
	    options = new GVls.FormattingOptionsInfo ();
	    range = new GVls.SourceRange ();
	}
}

public interface GVls.DocumentOnTypeFormattingParams : GLib.Object {
  /**
   * The document to format.
   */
  public abstract TextDocumentIdentifier textDocument { get; set; }

  /**
   * The position at which this request was sent.
   */
  public abstract Position position { get; set; }

  /**
   * The character that has been typed.
   */
  public abstract string ch { get; set; }

  /**
   * The format options.
   */
  public abstract FormattingOptions options { get; set; }
}

public interface GVls.DocumentOnTypeFormattingRegistrationOptions : GLib.Object, TextDocumentRegistrationOptions {
  /**
   * A character on which formatting should be triggered, like `}`.
   */
  public abstract string firstTriggerCharacter { get; set; }
  /**
   * More trigger characters.
   *
   * Container's items are of the type {@link GVo.String}
   */
  public abstract GVo.Container more_trigger_character { get; }
}


