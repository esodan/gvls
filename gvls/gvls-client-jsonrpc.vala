/* gvlsp-client-rpc.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Jsonrpc;
using GLib;
using GVls;

/**
 * An implementation of {@link GVls.Client}
 *
 * This client provides to the server a {@link GVls.ConfigurationVala}
 * object, enabling
 *
 *  a. enable defaults
 *  a. use Meson build system support by default on server
 */

/**
 * An implementation of {@link GVls.Client} using a {@link Jsonrpc.Client}
 * to connect to servers
 */
[Version (since="20.0")]
public class GVls.ClientJsonrpc : Object, GVls.Client {
    Jsonrpc.Client rpc_client;
    bool sent_shutdown = false;

    internal GVls.Workspace workspace { get; internal set; }
    internal bool initialized { get; internal set; }
    internal bool connected { get; internal set; }
    internal Cancellable cancellable { get; set; }
    internal GVls.ClientCapabilities? capabilities { get; set; }
    internal ServerCapabilities? server_capabilities { get; set; }
    public
    GLib.HashTable<uint,WorkDoneProgress>    work_done_tokens { get; set; }
    internal GVo.Container result_tokens { get; set; }
    internal GVo.Container result_diagnostics { get; set; }
    public string                   language_id { get; construct set; }
    internal bool          use_gvariant {
      get {
          if (rpc_client != null) {
              return rpc_client.use_gvariant;
          }
          return true;
      }
      set {
          if (rpc_client != null) {
              rpc_client.use_gvariant = value;
          }
      }
    }
    internal GLib.Variant? configuration { get; set;  }

    construct
    {
        capabilities = new ClientCapabilitiesInfo ();
        capabilities.text_document.document_symbol.hierarchical_document_symbol_support = true;

        capabilities.text_document.synchronization.did_save = true;

        capabilities.workspace = new WorkspaceClientCapabilitiesInfo ();
        capabilities.workspace.configuration = true;
        capabilities.workspace.symbol = new WorkspaceClientCapabilities.Symbol ();
        capabilities.workspace.symbol.symbol_kind.value_set.add (new SymbolKindObject (UNKNOWN));
        capabilities.workspace.symbol.symbol_kind.value_set.add (new SymbolKindObject (SymbolKind.NAMESPACE));
        capabilities.workspace.symbol.symbol_kind.value_set.add (new SymbolKindObject (SymbolKind.CLASS));
        capabilities.workspace.symbol.symbol_kind.value_set.add (new SymbolKindObject (SymbolKind.METHOD));
        capabilities.workspace.symbol.symbol_kind.value_set.add (new SymbolKindObject (SymbolKind.PROPERTY));
        capabilities.workspace.symbol.symbol_kind.value_set.add (new SymbolKindObject (SymbolKind.FIELD));
        capabilities.workspace.symbol.symbol_kind.value_set.add (new SymbolKindObject (SymbolKind.ENUM));
        capabilities.workspace.symbol.symbol_kind.value_set.add (new SymbolKindObject (SymbolKind.VARIABLE));
        capabilities.workspace.symbol.symbol_kind.value_set.add (new SymbolKindObject (SymbolKind.CONSTANT));
        capabilities.workspace.symbol.symbol_kind.value_set.add (new SymbolKindObject (SymbolKind.STRING));
        capabilities.workspace.symbol.symbol_kind.value_set.add (new SymbolKindObject (SymbolKind.NUMBER));
        capabilities.workspace.symbol.symbol_kind.value_set.add (new SymbolKindObject (SymbolKind.BOOLEAN));
        capabilities.workspace.symbol.symbol_kind.value_set.add (new SymbolKindObject (SymbolKind.ENUM_MEMBER));
        capabilities.workspace.symbol.symbol_kind.value_set.add (new SymbolKindObject (SymbolKind.STRUCT));

        var pt = new MarkupKindObject (MarkupKind.PLAIN_TEXT);
        capabilities.text_document.completion.completion_item.documentation_format.add (pt);
        var md = new MarkupKindObject (MarkupKind.MARKDOWN);
        capabilities.text_document.completion.completion_item.documentation_format.add (md);
        workspace = new GVls.WorkspaceInfo ();
        rpc_client = null;
        work_done_tokens = new GLib.HashTable<uint,WorkDoneProgress> (GLib.int_hash, GLib.int64_equal);
        result_tokens = new GVo.ContainerHashList ();
        result_diagnostics = new GVo.ContainerHashList ();
    }

    public void accept_io_stream (GLib.IOStream stream) {
        rpc_client = new Jsonrpc.Client (stream);
        rpc_client.handle_call.connect ((method, id, @params)=>{
            if (method == "workspace/configuration") {
                var config = new GVls.DidChangeConfigurationParamsInfo ();
                config.settings = configuration;
                rpc_client.reply_async.begin (id,
                            config.to_variant (),
                            cancellable);
                configuration_sent ();
                return true;
            }
            return false;
        });
        rpc_client.notification.connect ((method, @params)=>{
            if (method == "textDocument/publishDiagnostics") {
                var d = new GVls.PublishDiagnosticsParamsInfo ();
                d.parse_variant (@params);
                received_diagnostics.begin (d);
            }
        });
    }

    ~ClientJsonrpc () {
        if (rpc_client != null) {
            try {
                rpc_client.close_async.begin (null);
            } catch (GLib.Error e) {
                warning (_("Error while closing RPC Client: %s"), e.message);
            }
        }
    }

    internal async void
    initialize (string? uri) throws GLib.Error
    {
        Variant response = new Variant ("ms", null);
        var pars = new InitializeParamsInfo ();
        File folder = null;
        if (uri == null) {
            folder = GLib.File.new_for_path (GLib.Environment.get_home_dir ());
        } else {
            folder = File.new_for_uri (uri);
        }
        if (!folder.query_exists ()) {
            throw new GVls.ClientError.INVALID_URI_ERROR (_("Invalid initializaion URI for root folder"));
        }
        workspace.folders.add (folder);
        pars.root_uri = folder.get_uri ();
        pars.capabilities = capabilities;
        if (configuration != null) {
            pars.initialization_options = configuration;
        }
        yield rpc_client.call_async ("initialize",  pars.to_variant (), cancellable, out response);
        server_capabilities = new ServerCapabilitiesInfo ();
        server_capabilities.parse_variant (response);
        initialized = true;
        did_initialization ();
        notify_initialized.begin ();
    }

    internal async void
    notify_initialized () throws GLib.Error
    {
        if (!initialized) {
          throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }
        if (sent_shutdown) {
          throw new GVls.ClientError.SERVER_SHUTTINGDOWN_ERROR (_("Server has been started its shutting down process"));
        }
        var n = new InitializedParamsInfo ();
        var @params = n.to_variant ();
        yield rpc_client.send_notification_async ("initialized", @params, cancellable);
        notified_initialized ();
    }

    internal async void document_open (string doc_uri, string? contents) throws GLib.Error {
        if (!initialized) {
          throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }
        if (sent_shutdown) {
          throw new GVls.ClientError.SERVER_SHUTTINGDOWN_ERROR (_("Server has been started its shutting down process"));
        }
        var f = File.new_for_uri (doc_uri);
        if (!f.query_exists () && contents == null) {
          throw new GVls.ClientError.INVALID_URI_ERROR (_("Can't load document: document doesn't exits"));
        }
        if (!f.get_uri ().has_suffix (".vala")) {
            return;
        }
        var o = new TextDocumentItemInfo ();
        o.file = f;
        o.language_id = "vala";
        o.version = 1;
        if (contents != null) {
          o.text = contents;
        } else {
          o.text = "";
        }

        var dom = new DidOpenTextDocumentParamsInfo ();
        dom.text_document = o;
        var @params = dom.to_variant ();
        yield rpc_client.send_notification_async ("textDocument/didOpen", @params, cancellable);
    }

    internal async GVo.Container symbol (string symbol) throws GLib.Error {
        if (!initialized) {
          throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }
        if (sent_shutdown) {
          throw new GVls.ClientError.SERVER_SHUTTINGDOWN_ERROR (_("Server has been started its shutting down process"));
        }
        GLib.Variant response = null;
        var s = new WorkspaceSymbolParamsInfo ();
        s.query = symbol;
        Variant vparams = s.to_variant ();
        if (vparams == null) warning ("No params!!!");
        yield rpc_client.call_async ("workspace/symbol", vparams, cancellable, out response);
        // Parse returned value
        var c = new GVo.ContainerHashList.for_type (typeof (SymbolInformationInfo));
        c.parse_variant (response);
        return c;
    }
    public async GLib.Object
    completion (string uri,
                GVls.Position position,
                GLib.Variant? partial_result_token = null,
                GLib.Variant? work_done_token = null,
                GVls.CompletionContext? context = null)
                throws GLib.Error
    {
        if (!initialized) {
            throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }

        if (sent_shutdown) {
            throw new GVls.ClientError.SERVER_SHUTTINGDOWN_ERROR (_("Server has been started its shutting down process"));
        }

        var req = new CompletionParamsInfo ();
        req.text_document.uri = uri;
        req.position = position;
        // FIXME: Get the trigger character from CompletionRegistrationOptions
        req.context = context;

        var @params = req.to_variant ();
        Variant response = null;
        yield rpc_client.call_async ("textDocument/completion", @params, cancellable, out response);
        var list = new CompletionListInfo ();
        var t = response.get_type ();
        if (t.dup_string () == "av") {
            list.items.parse_variant (response);
            return list.items;
        }

        if (t.dup_string () == "{sv}") {
            list.parse_variant (response);
        }

        return list;
    }
    internal async GVo.Container
    document_symbols (string uri) throws GLib.Error
    {
        if (!initialized) {
            throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }

        if (sent_shutdown) {
            throw new GVls.ClientError.SERVER_SHUTTINGDOWN_ERROR (_("Server has been started its shutting down process"));
        }

        var req = new DocumentSymbolParamsInfo ();
        req.text_document.uri = uri;
        var @params = req.to_variant ();
        Variant response = null;
        yield rpc_client.call_async ("textDocument/documentSymbol", @params, cancellable, out response);
        var c = new GVo.ContainerHashList.for_type (typeof (DocumentSymbolInfo));
        c.parse_variant (response);

        return c;
    }
    public async void
    document_change (string doc_uri, GVo.Container events) throws GLib.Error
    {
        if (!initialized) {
            throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }

        if (sent_shutdown) {
            throw new GVls.ClientError.SERVER_SHUTTINGDOWN_ERROR (_("Server has been started its shutting down process"));
        }

        var @params = new DidChangeTextDocumentParamsInfo (events);
        @params.text_document.uri = doc_uri;

        yield rpc_client.send_notification_async ("textDocument/didChange", @params.to_variant (), cancellable);
    }

    public async void
    did_save (string uri, string text) throws GLib.Error
    {
        var saveops = new GVls.DidSaveTextDocumentParamsInfo ();
        saveops.text_document.uri = uri;
        saveops.text = text;
        yield rpc_client.send_notification_async ("textDocument/didSave", saveops.to_variant (), cancellable);
    }

    public async string server_shutdown () throws GLib.Error {
        if (!initialized) {
            throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }

        if (sent_shutdown) {
            throw new GVls.ClientError.SERVER_SHUTTINGDOWN_ERROR (_("Server has been started its shutting down process"));
        }

        var v = new Variant.string ("shutdown");
        Variant response = null;
        debug ("Sent SHUTDOWN to the server");
        yield rpc_client.call_async ("shutdown", v, cancellable, out response);
        debug ("Got response SHUTDOWN from server");
        sent_shutdown = true;
        if (response.is_of_type (GLib.VariantType.MAYBE)) {
            return "";
        }

        return response.print (false);
    }

    public async void server_exit () throws GLib.Error
    {
        if (!initialized) {
            throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }

        var req = new GVo.ContainerHashList ();
        yield rpc_client.send_notification_async ("exit", req.to_variant (), cancellable);
    }

    public async GVo.Container? definition (string uri,
                                        Position pos)
                                        throws GLib.Error
    {
        if (!initialized) {
            throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }

        if (sent_shutdown) {
            throw new GVls.ClientError.SERVER_EXIT_ERROR (_("Server needs to be shutted down before send an exit notification"));
        }

        GVo.Container c = new GVo.ContainerHashList.for_type (typeof (GVls.SourceLocation));

        var req = new GVls.TextDocumentPositionParamsInfo ();
        req.text_document.uri = uri;
        req.position = pos;
        var @params = req.to_variant ();
        Variant response = null;
        yield rpc_client.call_async ("textDocument/definition", @params, cancellable, out response);
        if (response.is_container ()) {
            c.parse_variant (response);
        } else {
            GVls.SourceLocation loc = new GVls.SourceLocation ();
            loc.parse_variant (response);
            c.add (loc);
        }

        return c;
    }
    /**
     * Use given settings to change server's configuration
     */
    public async void change_configuration (Variant settings) throws GLib.Error
    {
        if (!initialized) {
            throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }

        if (sent_shutdown) {
            throw new GVls.ClientError.SERVER_EXIT_ERROR (_("Server needs to be shutted down before send an exit notification"));
        }

        var @params = new GVls.DidChangeConfigurationParamsInfo  ();
        @params.settings = settings;

        yield rpc_client.send_notification_async ("workspace/didChangeConfiguration", @params.to_variant (), cancellable);
        notified_configuration_change ();
    }

    internal async GVo.Container?
    document_link (string uri, GLib.Variant? work_done_token) throws GLib.Error
    {
        if (!initialized) {
            throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }
        if (sent_shutdown) {
            throw new GVls.ClientError.SERVER_SHUTTINGDOWN_ERROR (_("Server has been started its shutting down process"));
        }
        var o = new GVls.DocumentLinkParamInfo ();
        o.text_document.uri = uri;
        // FIXME: Disable until we can have full support for lens
        // work_tokens.add (o.work_done_token);
        // result_tokens.add (o.partial_result_token);
        Variant response = null;
        yield rpc_client.call_async ("textDocument/documentLink", o.to_variant (), cancellable, out response);
        var c = new GVo.ContainerHashList.for_type (typeof (GVls.DocumentLinkInfo));
        c.parse_variant (response);
        return c;
    }

    internal async GVo.Container?
    code_lens (string uri, GLib.Variant? work_done_token) throws GLib.Error
    {
        if (!initialized) {
            throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }
        if (sent_shutdown) {
            throw new GVls.ClientError.SERVER_SHUTTINGDOWN_ERROR (_("Server has been started its shutting down process"));
        }
        var o = new GVls.CodeLensParamsInfo ();
        o.text_document.uri = uri;
        // FIXME: Disable until we can have full support for lens
        // work_tokens.add (o.work_done_token);
        // result_tokens.add (o.partial_result_token);
        Variant response = null;
        yield rpc_client.call_async ("textDocument/codeLens", o.to_variant (), cancellable, out response);
        var c = new GVo.ContainerHashList.for_type (typeof (GVls.DocumentLinkInfo));
        c.parse_variant (response);
        return c;
    }

    internal async GVls.CompletionItem?
    completion_resolve (string uri, GVls.CompletionItem item) throws GLib.Error
    {
        if (!initialized) {
            throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }
        if (sent_shutdown) {
            throw new GVls.ClientError.SERVER_SHUTTINGDOWN_ERROR (_("Server has been started its shutting down process"));
        }
        Variant response = null;
        yield rpc_client.call_async ("completionItem/resolve", item.to_variant (), cancellable, out response);
        var c = new GVls.CompletionItemInfo ();
        c.parse_variant (response);
        return c;
    }

    internal async SignatureHelp? signature_help (string uri,
                            Position position,
                            GLib.Variant? work_done_token,
                            SignatureHelpContext? context = null)
                            throws GLib.Error
    {
        if (!initialized) {
            throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }
        if (sent_shutdown) {
            throw new GVls.ClientError.SERVER_SHUTTINGDOWN_ERROR (_("Server has been started its shutting down process"));
        }
        SignatureHelpParamsInfo req = new SignatureHelpParamsInfo ();
        req.text_document.uri = uri;
        req.position = position;
        req.work_done_token = work_done_token;
        if (context != null) {
            req.context = context;
        }
        Variant response = null;
        yield rpc_client.call_async ("textDocument/signatureHelp", req.to_variant (), cancellable, out response);
        var r = new GVls.SignatureHelpInfo ();
        r.parse_variant (response);
        return r;
    }

    internal async Hover? hover (string uri,
                            Position position,
                            GLib.Variant? work_done_token)
                            throws GLib.Error
    {
        if (!initialized) {
            throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }
        if (sent_shutdown) {
            throw new GVls.ClientError.SERVER_SHUTTINGDOWN_ERROR (_("Server has been started its shutting down process"));
        }
        HoverParams req = new HoverParamsInfo ();
        req.text_document.uri = uri;
        req.position = position;
        req.work_done_token = work_done_token;
        Variant response = null;
        yield rpc_client.call_async ("textDocument/hover", req.to_variant (), cancellable, out response);
        var r = new GVls.HoverInfo ();
        r.parse_variant (response);
        return r;
    }
    internal virtual async GVo.Container?
    request_format (string uri,
            GVls.FormattingOptions options,
            GLib.Variant? work_done_token) throws GLib.Error
    {
        if (!initialized) {
            throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }
        if (sent_shutdown) {
            throw new GVls.ClientError.SERVER_SHUTTINGDOWN_ERROR (_("Server has been started its shutting down process"));
        }
        GVls.DocumentFormattingParams req = new DocumentFormattingParamsInfo ();
        req.text_document.uri = uri;
        req.options = options;
        req.work_done_token = work_done_token;
        Variant response = null;
        yield rpc_client.call_async ("textDocument/formatting", req.to_variant (), cancellable, out response);
        var r = new GVo.ContainerHashList.for_type (typeof (GVls.TextEditInfo));
        r.parse_variant (response);
        return r;
    }

    internal async GVo.Container?
    request_range_format (string uri, Range range,
                        FormattingOptions options,
                        GLib.Variant? work_done_token)
                        throws GLib.Error
    {
        if (!initialized) {
            throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }
        if (sent_shutdown) {
            throw new GVls.ClientError.SERVER_SHUTTINGDOWN_ERROR (_("Server has been started its shutting down process"));
        }
        GVls.DocumentRangeFormattingParams req = new DocumentRangeFormattingParamsInfo ();
        req.text_document.uri = uri;
        req.range = range;
        req.options = options;
        req.work_done_token = work_done_token;
        Variant response = null;
        yield rpc_client.call_async ("textDocument/rangeFormatting", req.to_variant (), cancellable, out response);
        var r = new GVo.ContainerHashList.for_type (typeof (GVls.TextEditInfo));
        r.parse_variant (response);
        return r;
    }
    internal async GLib.Variant?
    rename (string uri, string new_name,
            GVls.Position position,
            GLib.Variant? work_done_token)
            throws GLib.Error
    {
        if (!initialized) {
            throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }
        if (sent_shutdown) {
            throw new GVls.ClientError.SERVER_SHUTTINGDOWN_ERROR (_("Server has been started its shutting down process"));
        }
        GVls.RenameParamsInfo req = new RenameParamsInfo ();
        req.text_document.uri = uri;
        req.position = position;
        req.work_done_token = work_done_token;
        Variant response = null;
        yield rpc_client.call_async ("textDocument/rename", req.to_variant (), cancellable, out response);

        return response;
    }

    public virtual async GVo.Container?
    references (string uri, Position pos, bool decl,
                GLib.Variant? work_done_token,
                GLib.Variant? partial_result_token) throws GLib.Error {
        if (!initialized) {
            throw new GVls.ClientError.UNINITIALIZED_ERROR (_("Server has not been initialized"));
        }

        if (sent_shutdown) {
            throw new GVls.ClientError.SERVER_SHUTTINGDOWN_ERROR (_("Server has been started its shutting down process"));
        }

        var p = new GVls.ReferenceParamsInfo ();
        p.text_document.uri = uri;
        p.position = pos;
        p.context.include_declaration = decl;
        p.work_done_token = work_done_token;
        p.partial_result_token = partial_result_token;

        GLib.Variant response;
        yield rpc_client.call_async ("textDocument/references", p.to_variant (), cancellable, out response);

        GVo.Container c = new GVo.ContainerHashList.for_type (typeof (GVls.SourceLocation));
        c.parse_variant (response);
        return c;
    }
}
