/* gvls-failure-handling-kind.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public enum GVls.FailureHandlingKind {
  UNKNOWN,
  ABORT,
  TRANSACTIONAL,
  TEXT_ONLY_TRANSACTIONAL,
  UNDO;
  public string to_string () {
    string str = "";
    switch (this) {
      case ABORT:
        str = "abort";
        break;
      case TRANSACTIONAL:
        str = "transactional";
        break;
      case TEXT_ONLY_TRANSACTIONAL:
        str = "textOnlyTransactional";
        break;
      case UNDO:
        str = "undo";
        break;
      case UNKNOWN:
        str = "unknown";
        break;
    }
    return str;
  }
  public static FailureHandlingKind from_string (string str) {
    switch (str) {
        case "abort":
            return FailureHandlingKind.ABORT;
        case "transactional":
            return FailureHandlingKind.TRANSACTIONAL;
        case "textOnlyTransactional":
            return FailureHandlingKind.TEXT_ONLY_TRANSACTIONAL;
        case "undo":
            return FailureHandlingKind.UNDO;
    }
    return FailureHandlingKind.UNKNOWN;
  }
}
