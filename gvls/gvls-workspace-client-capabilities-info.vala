/* gvls-gworkspace-client-capabilities.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GLib;

public class GVls.WorkspaceClientCapabilitiesInfo : GLib.Object,
                                                GVo.Object,
                                                WorkspaceClientCapabilities
{
    [Description (nick="applyEdit")]
    internal bool apply_edit { get; set; }
    [Description (nick="workspaceEdit")]
    internal WorkspaceClientCapabilities.WorkspaceEdit? workspace_edit { get; set; }
    [Description (nick="didChangeConfiguration")]
    internal WorkspaceClientCapabilities.DidChangeConfiguration? did_change_configuration { get; set; }
    [Description (nick="didChangeWhatchedFile")]
    internal WorkspaceClientCapabilities.DidChangeWatchedFiles? did_change_watched_files { get; set; }
    [Description (nick="symbol")]
    internal WorkspaceClientCapabilities.Symbol symbol { get; set; }
    [Description (nick="executeCommand")]
    internal WorkspaceClientCapabilities.ExecuteCommand execute_command { get; set; }
    [Description (nick="workspaceFolders")]
    internal bool workspace_folders { get; set; }
    [Description (nick="configuration")]
    internal bool configuration { get; set; }

    construct {
        workspace_edit = new WorkspaceClientCapabilities.WorkspaceEdit ();
        did_change_configuration = new WorkspaceClientCapabilities.DidChangeConfiguration ();
        did_change_watched_files = new WorkspaceClientCapabilities.DidChangeWatchedFiles ();
        symbol = new WorkspaceClientCapabilities.Symbol ();
        execute_command = new WorkspaceClientCapabilities.ExecuteCommand ();
    }
}
