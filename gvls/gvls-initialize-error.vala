/* gvls-initialize-error.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

  /**
  * Known error codes for an `InitializeError`;
  */
public enum GVls.InitializeError {
  UNKNOWN,
  /**
   * If the protocol version provided by the client can't be handled by the server.
   *
   * Deprecated: This initialize error got replaced by client capabilities. There is
   * no version handshake in version 3.0x
   */
  [Version (deprecated=true)]
  UNKNOWN_PROTOCOL_VERSION;
  public string to_string () {
    string str = "";
    switch (this) {
      case UNKNOWN_PROTOCOL_VERSION:
        str = "unknownProtocolVersion";
        break;
      case UNKNOWN:
        str = "unknown";
        break;
    }
    return str;
  }
}
