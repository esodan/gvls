/* gvls-initialize-params.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.InitializeParams : GLib.Object, GVo.Object {
  /**
   * The process Id of the parent process that started
   * the server. Is null if the process has not been started by another process.
   * If the parent process is not alive then the server should exit (see exit notification) its process.
   */
  [Description (nick="processId")]
  public abstract int process_id { get; set; }

  /**
   * The rootPath of the workspace. Is null
   * if no folder is open.
   *
   * Deprecated: in favour of rootUri.
   */
  [Version (deprecated=true)]
  [Description (nick="rootPath")]
  public abstract string? root_path { get; set; }

  /**
   * The rootUri of the workspace. Is null if no
   * folder is open. If both `rootPath` and `rootUri` are set
   * `rootUri` wins.
   */
  [Description (nick="rootUri")]
  public abstract string root_uri { get; set; }

  /**
   * User provided initialization options.
   *
   * Here are the specific configuration provided to
   * the server.
   */
  [Description (nick="initializationOptions")]
  public abstract GLib.Variant initialization_options { get; set; }

  /**
   * The capabilities provided by the client (editor or tool)
   */
  [Description (nick="capabilities")]
  public abstract ClientCapabilities capabilities { get; set; }

  /**
   * The initial trace setting. If omitted trace is disabled ('off').
   *
   * Valid values are: 'off' | 'messages' | 'verbose'
   */
  [Description (nick="trace")]
  public abstract string? trace { get; set; }

  /**
   * The workspace folders configured in the client when the server starts.
   * This property is only available if the client supports workspace folders.
   * It can be `null` if the client supports workspace folders but none are
   * configured.
   *
   * A container with objects of type {@link WorkspaceFolder}
   *
   * Since 3.6.0
   */
  [Description (nick="workspaceFloders")]
  public abstract GVo.Container? workspace_folders { get; set; }
}

