/* gvls-completion-item.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


public interface GVls.CompletionItem : GLib.Object,
                                    GVo.ContainerHashable,
                                    GVo.Object
{
  /**
   * The label of this completion item. By default
   * also the text that is inserted when selecting
   * this completion.
   */
  [Description (nick="label")]
  public abstract string label { get; set; }

  /**
   * The kind of this completion item. Based of the kind
   * an icon is chosen by the editor.
   */
  [Description (nick="kind")]
  public abstract CompletionItemKind kind { get; set; }

  /**
   * Tags for this completion item.
   *
   * A {@link GVo.Container} with {@link CompletionItemTag}
   *
   * @since 3.15.0
   *
   */
  [Description (nick="tags")]
  [Version(since="0.16")]
  public abstract GVo.Container tags { get; set; }

  /**
   * A human-readable string with additional information
   * about this item, like type or symbol information.
   */
  [Description (nick="detail")]
  public abstract string? detail { get; set; }

  /**
   * A human-readable string that represents a doc-comment.
   */
  [Description (nick="documentation")]
  public abstract MarkupContent? documentation { get; set; }

  /**
   * Indicates if this item is deprecated.
   */
  [Description (nick="deprecated")]
  public abstract GVo.Boolean deprecated { get; set; }

  /**
   * Select this item when showing.
   *
   * *Note* that only one completion item can be selected and that the
   * tool / client decides which item that is. The rule is that the *first*
   * item of those that match best is selected.
   */
  [Description (nick="preselect")]
  public abstract GVo.Boolean preselect { get; set; }

  /**
   * A string that should be used when comparing this item
   * with other items. When `falsy` the label is used.
   */
  [Description (nick="sortText")]
  public abstract string? sort_text { get; set; }

  /**
   * A string that should be used when filtering a set of
   * completion items. When `falsy` the label is used.
   */
  [Description (nick="filterText")]
  public abstract string? filter_text { get; set; }

  /**
   * A string that should be inserted into a document when selecting
   * this completion. When `falsy` the label is used.
   *
   * The `insertText` is subject to interpretation by the client side.
   * Some tools might not take the string literally. For example
   * VS Code when code complete is requested in this example `con<cursor position>`
   * and a completion item with an `insertText` of `console` is provided it
   * will only insert `sole`. Therefore it is recommended to use `textEdit` instead
   * since it avoids additional client side interpretation.
   *
   * Deprecated Use textEdit instead.
   */
  [Version (deprecated=true)]
  [Description (nick="insertText")]
  public abstract string? insert_text { get; set; }

  /**
   * The format of the insert text. The format applies to both the `insertText` property
   * and the `newText` property of a provided `textEdit`.
   */
  [Description (nick="insertTextFormat")]
  public abstract InsertTextFormatObject? insert_text_format { get; set; }

  /**
   * An edit which is applied to a document when selecting this completion. When an edit is provided the value of
   * `insertText` is ignored.
   *
   * *Note:* The range of the edit must be a single line range and it must contain the position at which completion
   * has been requested.
   */
  [Description (nick="textEdit")]
  public abstract TextEdit? text_edit { get; set; }

  /**
   * An optional array of additional text edits that are applied when
   * selecting this completion. Edits must not overlap (including the same insert position)
   * with the main edit nor with themselves.
   *
   * Additional text edits should be used to change text unrelated to the current cursor position
   * (for example adding an import statement at the top of the file if the completion item will
   * insert an unqualified type).
   *
   * Container's items are of type {@link TextEdit}
   */
  [Description (nick="additionalTextEdits")]
  public abstract GVo.Container additional_text_edits { get; set; }

  /**
   * An optional set of characters that when pressed while this completion is active will accept it first and
   * then type that character. *Note* that all commit characters should have `length=1` and that superfluous
   * characters will be ignored.
   *
   * Container's items are of type {@link GVo.String}
   */
  [Description (nick="commitCharacters")]
  public abstract GVo.Container commit_characters { get; set; }

  /**
   * An optional command that is executed *after* inserting this completion. *Note* that
   * additional modifications to the current document should be described with the
   * additionalTextEdits-property.
   */
  [Description (nick="command")]
  public abstract Command? command { get; set; }

  /**
   * An data entry field that is preserved on a completion item between
   * a completion and a completion resolve request.
   */
  [Description (nick="data")]
  public abstract GLib.Variant data { get; set; }

    /**
     * Set string as an id to {@link data}
     */
    [Version (since="0.16.0")]
    public void
    set_id (string id)
    {
        data = new GLib.Variant.string (id);
    }
    /**
     * Get item's id contained in {@link data}
     */
    [Version (since="0.16.0")]
    public string?
    get_id ()
    {
        if (data == null) {
            return null;
        }

        return data.get_string ();
    }
}

