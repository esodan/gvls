/* gvls-target.vala
 *
 * Copyright 2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A target to be built in a project
 */
[Version (since="20.0")]
public interface GVls.Target : GLib.Object
{
    /**
     * A list of {@link GVo.NamedString} objects, where
     * its name is the URI of the file and its value
     * is the content of that file.
     */
    public abstract GVo.Container                 contents { get; }
    /**
     * Compilation arguments and files required to build the target
     */
    public abstract TargetConfiguration       configuration { get; set; }
    /**
     * Target's name
     */
    public abstract string                    name { get; construct set; }
    /**
     * Symbol resolution provider
     */
    public abstract SymbolProvider  symbol_provider { get; set; }
    /**
     * Reports from {@link run_diagnostics} as a list of
     * {@link GVls.Report}
     */
    public abstract GVls.Report      diagnostics_report { get; set; }
    /**
     * Target Manager this target belongs to
     */
    public abstract unowned TargetManager     manager { get; construct set; }
    /**
     * Linter used to run diagnostics
     */
    public abstract GVls.Linter               linter { get; construct set; }
    /**
     * Flag set to TRUE if the target's building is a 
     * work in progress; useful to avoid running multiple
     * works
     */
    public abstract bool                        building { get; }

    /**
     * Emitted when a diagnostics was done
     */
    public signal void did_diagnostics ();
    /**
    * Checks the file is used to build the target
    */
    public virtual bool requires_file (GLib.File file)
    {
        if (configuration == null) {
            return false;
        }

        string uri = file.get_uri ();
        for (int i = 0; i < configuration.files.get_n_items (); i++) {
            GVo.String str = configuration.files.get_item (i) as GVo.String;
            if (str == null) {
                continue;
            }

            if (uri == str.@value) {
                return true;
            }
        }
        return false;
    }
    /**
     * Executes a diagnostics and returns a {@link GVls.Report}.
     *
     * All parsed sources are used to create {@link GVls.SymbolResolver}
     * objects, updating all symbols
     */
    public virtual async void run_diagnostics () throws GLib.Error {
        run_diagnostics_sync ();
    }
    /**
     * Executes a diagnostics for all files in the target
     */
    public abstract void run_diagnostics_sync () throws GLib.Error;
    /**
     * Executes a linter diagnostics for a given file
     */
    public virtual async void run_linter_diagnostics (GLib.File file) throws GLib.Error {
        var c = linter.run (file);
        var item = diagnostics_report.diagnostics.get (file.get_uri ());
        bool add = false;
        if (item == null) {
            add = true;
            item = new Gee.HashSet<GVls.Diagnostic> ();
        }

        for (int i = 0; i < c.get_n_items (); i++) {
            var d = c.get_item (i) as GVls.Diagnostic;
            if (d == null) {
                continue;
            }

            item.add (d);
        }

        if (add) {
            diagnostics_report.diagnostics.set (file.get_uri (), item);
        }
    }
    /**
     * Remove all diagnostics. Call this method after
     * all diagnostics have been sent to the client.
     */
    [Version (since="21.0")]
    public virtual void reset_diagnostics () {}
    /**
     * Add a file's content saving to {@link contents}
     * using file's URI as its name
     */
    public abstract void add_file_contents (string uri, string content) throws GLib.Error;
    /**
     * Find a file's content by its uri
     */
    public virtual unowned string? file_contents (string uri) {
        for (int i = 0; i < contents.get_n_items (); i++) {
            GVo.HashableNamedString s = contents.get_item (i) as GVo.HashableNamedString;
            if (s == null) {
                continue;
            }

            if (uri == s.name) {
                return s.@value;
            }
        }

        return null;
    }
    /**
     * Read file contents and save it to {@link contents}
     *
     * Returns: a string with the file contents;
     */
    public void read_file_contents (string uri) throws GLib.Error {
        var f = GLib.File.new_for_uri (uri);
        read_gfile_contents (f);
    }
    /**
     * Read file contents and save it to {@link contents}
     *
     * Returns: a string with the file contents;
     */
    public virtual void read_gfile_contents (GLib.File file) throws GLib.Error {
        if (!file.query_exists ()) {
            throw new GVls.TargetError
                        .FILE_NOT_FOUND_ERROR (_("File doesn't exists: %s"),
                                                file.get_uri ());
        }
        GLib.DataInputStream istream = new GLib.DataInputStream (file.read ());
        string text = istream.read_upto ("\0", -1 , null);
        add_file_contents (file.get_uri (), text);
    }

    /**
     * Add file and read file's contents to the {@link configuration}
     */
    public virtual async void use_file (GLib.File file) throws GLib.Error {
        use_file_sync (file);
    }

    /**
     * Add file and read file's contents to the {@link configuration}
     */
    public virtual void use_file_sync (GLib.File file) throws GLib.Error {
        read_gfile_contents (file);
        configuration.add_file (file);
    }

    /**
     * Add recursivally all files' contents found in given URI.
     *
     * If URI is a file, just one file's contents is added.
     */
    public virtual async void add_files_from (string uri) throws GLib.Error {
        add_files_from_sync (uri);
    }

    /**
     * Add recursivally all files' contents found in given URI.
     *
     * If URI is a file, just one file's contents is added.
     */
    public abstract void add_files_from_sync (string uri) throws GLib.Error;
    /**
     * Search for a given namespace and adds all packages
     * it has symbols defined in, to configuration.
     *
     * If api_version is set to null, all packages providing
     * same namespace are added. Take care, because that could
     * means errors due to redefinitions or others.
     */
    public abstract void use_namespace (string ns, string? api_version) throws GLib.Error;
    /**
     * Search for a given namespace and adds all its
     * dependencies, including the ones from a dependency
     */
    public abstract void use_package (string pkg) throws GLib.Error;
    /**
     * Find Resolvers providing given namespace and
     * included in {@link configuration}
     */
    public abstract GVo.Container find_namespace_resolvers (string ns);
    /**
     * Apply configuration from {@link GVls.TargetConfiguration}
     */
    public abstract void apply_configuration () throws GLib.Error;
    /**
     * Apply all {@link DidChangeTextDocumentParams} changes
     */
    public virtual void apply_changes (DidChangeTextDocumentParams changes)
    {
        GVo.HashableNamedString content = null;
        for (int i = 0; i < contents.get_n_items (); i++) {
            var item = contents.get_item (i) as GVo.HashableNamedString;
            if (item == null) {
                continue;
            }

            if (item.name == changes.text_document.uri) {
                content = item;
            }
        }

        if (content == null) {
            return;
        }

        GLib.StringBuilder str = new GLib.StringBuilder (content.@value);
        for (int i = 0; i < changes.content_changes.get_n_items (); i++) {
            var c = changes.content_changes.get_item (i) as TextDocumentContentChangeEvent;
            if (c == null) {
                    continue;
                }
            try {
                replace_text_at (c.range.start.line,
                            c.range.end.line,
                            c.range.start.character,
                            c.range.end.character,
                            str, c.text);
            } catch (GLib.Error e) {
                warning ("Error while replace text on server: %s", e.message);
            }
        }

        content.@value = str.str;
    }

    // Static methods
    /**
     * Replace a text in a  {@link GLib.StringBuilder} located at given position
     * range
     */
    public static void replace_text_at (int lstart, int lend, int cstart, int cend,
                                        GLib.StringBuilder str,
                                        string text)
                                        throws GLib.Error
    {
        int start = 0;
        int offset = 0;
        int ls = 0;
        int le = 0;
        int l = 0;
        if (lend > lstart) {
            ls = lstart;
            le = lend;
        } else {
            ls = lend;
            le = lstart;
        }

        unichar c = '\0';
        int index = 0;
        while (l < ls) {
            if (str.str.get_next_char (ref index, out c)) {
                start += c.to_string ().length;
                if (c == '\n') {
                    l++;
                }
            } else {
                break;
            }
        }

        int col = 0;
        while (col < cstart) {
            if (str.str.get_next_char (ref index, out c)) {
                start += c.to_string ().length;
                col++;
            } else {
                break;
            }
        }

        while (l < le) {
            if (str.str.get_next_char (ref index, out c)) {
                offset += c.to_string ().length;
                if (c == '\n') {
                    l++;
                }
            } else {
                break;
            }
        }

        col = 0;
        while (col < (cend - cstart)) {
            if (str.str.get_next_char (ref index, out c)) {
                offset += c.to_string ().length;
                col++;
            } else {
                break;
            }
        }

        str.erase (start, offset);
        str.insert (start, text);
    }
}

public errordomain GVls.TargetError {
  CONFIG_ERROR,
  FILE_NOT_FOUND_ERROR,
  PACKAGE_NOT_FOUND
}


