/* gvls-gcompletion-list-tiny.vala
 *
 * Copyright 2021 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Represents a collection of {@link CompletionItem} to be presented
 * in the editor.
 *
 * By default {@link CompletionItemTiny} are used.
 */
public class GVls.CompletionListTiny : GLib.Object,
                                    GVo.Object,
                                    CompletionList
{
  GVo.Container _items = new GVo.ContainerSortedHashList.for_type (typeof (CompletionItemTiny));

  [Description (nick="isIncomplete")]
  public bool is_incomplete { get; set; }
  [Description (nick="items")]
  public GVo.Container items { get { return _items; } }
}

