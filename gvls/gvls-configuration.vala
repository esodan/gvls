/* gvls-configuration.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GLib;

/**
 * Vala build environment.
 */
[Version (since="0.16.0")]
public interface GVls.Configuration : Object
{
    /**
     * For the server for an autoconfiguration.
     *
     * Setting this to TRUE makes ignore all
     * other configuration.
     *
     * Tries to detect the build system currently
     * in use and execute its introspection information
     * generation.
     */
    [Description (nick="autoConfigure")]
    public abstract bool autoconfigure { get; set; }
    /**
     * Add current's Vala's namespaces.
     *
     * Scope URI for  //workspace/didChangeConfiguration// is:
     * [[vala://configuration/general/]]
     * section ''default_namespaces''
     */
    [Description (nick="defaultNamespaces")]
    public abstract bool default_namespaces { get; set; }
    /**
     * Enable meson Build System.
     *
     * By using meson, server should
     *
     *  a. Skip subprojects
     *
     * Once meson files can be introspected
     *
     *  a. from targets, scan for arguments and packages
     *
     * Scope URI for  //workspace/didChangeConfiguration// is:
     * `[[vala://configuration/general/]]
     * section ''meson_build_system''
     */
    [Description (nick="mesonBuildSystem")]
    public abstract bool meson_build_system { get; set; }
    /**
     * Source files used by current build environment
     * as a list of {@link GVo.String} with the file's URI
     *
     * Scope URI for  //workspace/didChangeConfiguration// is:
     * `[[vala://configuration/general/]]
     * section ''files''
     */
    [Description (nick="files")]
    public abstract GVo.Container files { get; set; }
    /**
     * Packages used by current build environment
     * as a list of {@link GVo.String} with the package's name
     *
     * Scope URI for  //workspace/didChangeConfiguration// is:
     * `[[vala://configuration/general/]]
     * section ''packages''
     */
    [Description (nick="packages")]
    public abstract GVo.Container packages { get; set; }
    /**
     * Additional options list
     *
     * Container's items are {@link GVo.NamedString}
     * kind.
     *
     * Scope URI for  //workspace/didChangeConfiguration// is:
     * `[[vala://configuration/general/]]
     * section ''options''
     */
    [Description (nick="options")]
    public abstract GVo.Container options { get; set; }

    /**
     * Adds a file
     */
    public abstract void add_file (GLib.File file);

    /**
     * Add a package to {@link packages}
     */
    public abstract void add_package (string pkg);

    /**
     * Add an option to {@link options}
     */
    public abstract void add_option (string name, string val);
}

