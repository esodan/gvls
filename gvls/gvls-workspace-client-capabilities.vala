/* gvls-workspace-client-capabilities.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Workspace specific client capabilities.
 */
public interface GVls.WorkspaceClientCapabilities : GLib.Object, GVo.Object {
  /**
   * The client supports applying batch edits to the workspace by supporting
   * the request 'workspace/applyEdit'
   */
  [Description (nick="applyEdit")]
  public abstract bool apply_edit { get; set; }
  /**
   * Capabilities specific to `WorkspaceEdit`s
   */
  [Description (nick="workspaceEdit")]
  public abstract WorkspaceEdit? workspace_edit { get; set; }
  public class WorkspaceEdit : GLib.Object, GVo.Object {
    construct {
      resource_operations = new GVo.ContainerHashList ();
      resource_operations.add (new ResourceOperationKindObject (ResourceOperationKind.CREATE));
      resource_operations.add (new ResourceOperationKindObject (ResourceOperationKind.RENAME));
      resource_operations.add (new ResourceOperationKindObject (ResourceOperationKind.DELETE));
    }
    /**
     * The client supports versioned document changes in `WorkspaceEdit`s
     */
    [Description (nick="documentChanges")]
    public bool document_changes { get; set; }

    /**
     * The resource operations the client supports. Clients should at least
     * support 'create', 'rename' and 'delete' files and folders.
     *
     * Container hold objects of type {@link ResourceOperationKindObject}
     */
    [Description (nick="resourceOperations")]
    public GVo.Container? resource_operations { get; set; }

    /**
     * The failure handling strategy of a client if applying the workspace edit
     * fails.
     */
    [Description (nick="failureHandling")]
    public FailureHandlingKindObject failure_handling { get; set; }
  }

  /**
   * Capabilities specific to the `workspace/didChangeConfiguration` notification.
   */
  [Description (nick="didChangeConfiguration")]
  public abstract DidChangeConfiguration? did_change_configuration { get; set; }
  public class DidChangeConfiguration : GLib.Object, GVo.Object {
    /**
     * Did change configuration notification supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
  }

  /**
   * Capabilities specific to the `workspace/didChangeWatchedFiles` notification.
   */
  [Description (nick="didChangeWathedFiles")]
  public abstract DidChangeWatchedFiles? did_change_watched_files { get; set; }
  public class DidChangeWatchedFiles : GLib.Object, GVo.Object {
    /**
     * Did change watched files notification supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
  }

  /**
   * Capabilities specific to the `workspace/symbol` request.
   */
  [Description (nick="symbol")]
  public abstract Symbol symbol { get; set; }
  public class Symbol : GLib.Object, GVo.Object {
    /**
     * Symbol request supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }

    /**
     * Specific capabilities for the `SymbolKind` in the `workspace/symbol` request.
     */
    [Description (nick="symbolKind")]
    public SymbolKind symbol_kind { get; set; }

    construct {
      symbol_kind = new SymbolKind ();
    }
  }
  public class SymbolKind : GLib.Object, GVo.Object {
    /**
     * The symbol kind values the client supports. When this
     * property exists the client also guarantees that it will
     * handle values outside its set gracefully and falls back
     * to a default value when unknown.
     *
     * If this property is not present the client only supports
     * the symbol kinds from `File` to `Array` as defined in
     * the initial version of the protocol.
     *
     * Container holds items of type {@link GVls.SymbolKindObject}
     */
    [Description (nick="valueSet")]
    public GVo.Container value_set { get; set; }

    construct {
      value_set = new GVo.ContainerHashList.for_type (typeof (GVls.SymbolKindObject));
    }
  }

  /**
   * Capabilities specific to the `workspace/executeCommand` request.
   */
    [Description (nick="executeCommand")]
  public abstract ExecuteCommand execute_command { get; set; }
  public class ExecuteCommand : GLib.Object, GVo.Object {
    /**
     * Execute command supports dynamic registration.
     */
    [Description (nick="dynamicRegistration")]
    public bool dynamic_registration { get; set; }
  }

  /**
   * The client has support for workspace folders.
   *
   * Since 3.6.0
   */
  [Description (nick="workspaceFolders")]
  public abstract bool workspace_folders { get; set; }

  /**
   * The client supports `workspace/configuration` requests.
   *
   * Since 3.6.0
   */
  [Description (nick="configuration")]
  public abstract bool configuration { get; set; }
}
