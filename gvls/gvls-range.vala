/* gvls-range.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.Range : GLib.Object, GVo.Object {
    /**
     * The range's start position.
     */
    [Description (nick="start")]
    public abstract Position start { get; set; }

    /**
     * The range's end position.
     */
    [Description (nick="end")]
    public abstract Position end { get; set; }

    public virtual string to_string () {
        string str = "%d.%d-%d.%d".printf (start.line, start.character, end.line, end.character);
        return str;
    }
    /**
     * Calculates if given {@link GVls.Position}
     * is located in the range
     */
    public virtual bool is_position_in (GVls.Position pos) {
        if (pos.line > start.line && pos.line < end.line)
        {
            return true;
        }

        if (pos.line == start.line)
        {
            if (pos.character >= start.character
                && pos.character <= start.character)
            {
                return true;
            }
        }

        if (pos.line == end.line)
        {
            if (pos.character >= end.character
                && pos.character <= end.character)
            {
                return true;
            }
        }

        return false;
    }

    /**
     *
     */
    public virtual bool is_in_range (GVls.Range range)
    {
        if (start.line > range.start.line && end.line < range.end.line)
        {
            return true;
        }

        if (start.line == range.start.line && end.line < range.end.line)
        {
            return true;
        }

        if (start.line > range.start.line && end.line == range.end.line)
        {
            return true;
        }

        if (start.line == range.start.line && end.line == range.end.line)
        {
            if (start.character > range.start.character
                && end.character < range.end.character)
            {
                return true;
            }

            if (start.character == range.start.character
                && end.character < range.end.character)
            {
                return true;
            }

            if (start.character > range.start.character
                && end.character == range.end.character)
            {
                return true;
            }
        }

        return false;
    }
    /**
     * Calculates if a range is equal to other
     */
    public virtual bool is_equal (GVls.Range range) {
        if (range.start.line != start.line) {
            return false;
        }

        if (range.start.character != start.character) {
            return false;
        }

        if (range.end.line != end.line) {
            return false;
        }

        if (range.end.character != end.character) {
            return false;
        }

        return true;
    }
}
