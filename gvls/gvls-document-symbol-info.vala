/* GVls-gserver.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 public class GVls.DocumentSymbolInfo : GLib.Object, GLib.ListModel, GVo.Object,
                                    GVo.Container, DocumentSymbol
{
    private Symbol _symbol;
    private Location _location;

    // No Vala symbol
    private string _name;
    private string _detail;
    private SymbolKind _kind = SymbolKind.UNKNOWN;
    private GVo.Container _children = null;
    private bool _deprecated = false;
    private Range _range = null;
    private Range _selection_range = null;
    private GLib.Type _object_type;

    [Version (deprecated=true, since="0.14.0")]
    internal GLib.Type object_type { get { return _object_type; } }
    /**
    * The {@link Symbol} used to get data from
    */
    [Version (since="0.14.0")]
    internal Symbol symbol { get { return _symbol; } }

    construct {
    _name = "";
    _detail = "";
    _location = new SourceLocation ();
    _range = new SourceRange ();
    _selection_range = new SourceRange ();
    }


    public DocumentSymbolInfo.from_symbol (Symbol sym) {
        _symbol = sym;
        _name = sym.name;
        _detail = sym.full_name;
        if (_symbol.location != null) {
            _location = _symbol.location;
            _range = _symbol.location.range;
            _selection_range = _symbol.location.range;
        }
    }
    public DocumentSymbolInfo.from_symbol_location (Symbol sym, Location loc) {
        _symbol = sym;
        _location = loc;
    }

    public DocumentSymbolInfo.from_values (string name, string detail,
                                      SymbolKind kind, bool deprecated,
                                      Range range, Range selection_range,
                                      GVo.Container children)
    {
        _name = name;
        _detail = detail;
        _kind = kind;
        _deprecated = deprecated;
        _location.range = range;
        _children = children;
        _range = range;
        _selection_range = selection_range;
    }


    // DocumentSymbol
    internal string name {
        get {
            return _name;
        }
    }
    internal string? detail {
        get {
            return _detail;
        }
    }
    internal SymbolKind kind {
    get {
      if (_symbol != null) {
        return _symbol.symbol_kind;
      }
      return _kind;
    }
    }
    internal bool deprecated {
        get {
            if (_symbol != null) {
               return _symbol.deprecated;
            }

            return _deprecated;
        }
    }
    internal Range range {
        get {
            return _range;
        }
    }
    internal Range selection_range {
        get {
            return _selection_range;
        }
    }
    internal GVo.Container children {
        get {
            if (_children == null) {
                _children = new GVo.ContainerHashList ();
                //        if (_symbol != null) {
                //          for (int i = 0; i < _symbol.get_n_items (); i++) {
                //            var item = _symbol.get_item (i) as Symbol;
                //            if (item == null) continue;
                //            var ds = new DocumentSymbolInfo.from_symbol (item);
                //            _children.add (ds);
                //          }
                //        }
            }
            return _children;
        }
    }
    // Container
    internal void add (GLib.Object object) { _symbol.add (object); }
    internal void remove (GLib.Object object) { _symbol.remove (object); }
    internal GLib.Object? find (GLib.Object key) { return _symbol.find (key); }

    // ListModel implementation
    internal GLib.Object? get_item (uint position) { return _symbol.get_item (position); }
    internal GLib.Type get_item_type () { return typeof (Symbol); }
    internal uint get_n_items () { return _symbol.get_n_items (); }
    // Symbol
    internal Location location { get { return _location; } }
    internal Package package { get { return _symbol.package; } }
    internal Documentation documentation { get { return _symbol.documentation; } }
    internal SymbolKind symbol_kind { get { return _symbol.symbol_kind; } }
    internal string full_name { owned get { return _symbol.full_name; } }
    internal string data_type { get { return _symbol.data_type; } }

    internal GLib.Variant to_variant () {
        var b = new GLib.VariantBuilder (new GLib.VariantType ("a{sv}"));
        b.add ("{sv}", "name", new GLib.Variant.string (name != null ? name : ""));
        b.add ("{sv}", "detail", new GLib.Variant.string (detail != null ? detail : ""));
        b.add ("{sv}", "kind", kind.to_variant ());
        b.add ("{sv}", "deprecated", new GLib.Variant.boolean (deprecated));
        b.add ("{sv}", "range", range.to_variant ());
        b.add ("{sv}", "selectionRange", selection_range.to_variant ());
        b.add ("{sv}", "children", children.to_variant ());
        return b.end ();
    }
    internal void parse_variant (GLib.Variant v) {
        var dict = new GLib.VariantDict(v);
        GLib.Variant vn = dict.lookup_value ("name", GLib.VariantType.STRING);
        if (vn != null) {
            _name = vn.get_string ();
        }

        GLib.Variant dv = dict.lookup_value ("detail", GLib.VariantType.STRING);
        if (dv != null) {
            _detail = dv.get_string ();
        }

        GLib.Variant dpv = dict.lookup_value ("deprecated", GLib.VariantType.BOOLEAN);
        if (dpv != null) {
            _deprecated = dpv.get_boolean ();
        }

        GLib.Variant kv = dict.lookup_value ("kind", GLib.VariantType.INT64);
        if (kv != null) {
            _kind = SymbolKind.parse_variant (kv);
        }

        GLib.Variant rv = dict.lookup_value ("range", GLib.VariantType.DICTIONARY);
        if (rv != null) {
            _range.parse_variant (rv);
        }

        GLib.Variant rsv = dict.lookup_value("selectionRange", GLib.VariantType.DICTIONARY);
        if (rsv != null) {
            _selection_range.parse_variant (rsv);
        }

        GLib.Variant cv = dict.lookup_value("children", GLib.VariantType.ARRAY);
        if (cv != null) {
            _children = new GVo.ContainerHashList.for_type (typeof (GVls.DocumentSymbolInfo));
            _children.parse_variant (cv);
        }
    }
}
