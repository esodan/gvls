/* gvls-request-item.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GLib;

public class GVls.RequestItem : GLib.Object, GVo.ContainerHashable {
  public string method { get; set; }
  public string id { get; set; }

  public RequestItem (string method) {
    this.method = method;
    this.id = Uuid.string_random ();
  }

  public uint hash () { return GLib.str_hash (id); }
  public bool equal (GLib.Object obj) {
    if (!(obj is RequestItem)) {
        return false;
    }

    var r = obj as RequestItem;
    if (r != null) {
        if (r.id == id) {
            return true;
        }
    }

    return false;
  }
}
