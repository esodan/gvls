/* gvls-text-document-sync-options.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.TextDocumentSyncOptions : GLib.Object, GVo.Object {
	/**
	 * Open and close notifications are sent to the server.
	 */
    [Description (nick="openClose")]
    public abstract bool open_close { get; set; }
	/**
	 * Change notifications are sent to the server. See TextDocumentSyncKind.None, TextDocumentSyncKind.Full
	 * and TextDocumentSyncKind.Incremental. If omitted it defaults to TextDocumentSyncKind.None.
	 */
    [Description (nick="change")]
    public abstract TextDocumentSyncKind change { get; set; }
	/**
	 * Will save notifications are sent to the server.
	 */
    [Description (nick="willSave")]
    public abstract bool will_save { get; set; }
	/**
	 * Will save wait until requests are sent to the server.
	 */
    [Description (nick="willSaveWaitUntil")]
    public abstract bool will_save_wait_until { get; set; }
	/**
	 * Save notifications are sent to the server.
	 */
    [Description (nick="save")]
    public abstract SaveOptions save { get; set; }
}

