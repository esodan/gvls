/* gvls-completion-item-kind.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * The kind of a completion entry.
 */
public enum GVls.CompletionItemKind {
  UNKNOWN,
  TEXT,
  METHOD,
  FUNCTION,
  CONSTRUCTOR,
  FIELD,
  VARIABLE,
  CLASS,
  INTERFACE,
  MODULE,
  PROPERTY,
  UNIT,
  VALUE,
  ENUM,
  KEYWORD,
  SNIPPED,
  COLOR,
  FILE,
  REFERENCE,
  FOLDER,
  ENUM_MEMBER,
  CONSTANT,
  STRUCT,
  EVENT,
  OPERATOR,
  TYPE_PARAMETER;

  public string to_string () {
    string str = "";
    switch (this) {
      case TEXT:
        str = "Text";
        break;
      case METHOD:
        str = "Method";
        break;
      case FUNCTION:
        str = "Function";
        break;
      case CONSTRUCTOR:
        str = "Constructor";
        break;
      case FIELD:
        str = "Field";
        break;
      case VARIABLE:
        str = "Variable";
        break;
      case CLASS:
        str = "Class";
        break;
      case INTERFACE:
        str = "Interface";
        break;
      case MODULE:
        str = "Module";
        break;
      case PROPERTY:
        str = "Property";
        break;
      case UNIT:
        str = "Unit";
        break;
      case VALUE:
        str = "Value";
        break;
      case ENUM:
        str = "Enum";
        break;
      case KEYWORD:
        str = "Keyword";
        break;
      case SNIPPED:
        str = "Snippet";
        break;
      case COLOR:
        str = "Color";
        break;
      case FILE:
        str = "File";
        break;
      case REFERENCE:
        str = "Reference";
        break;
      case FOLDER:
        str = "Folder";
        break;
      case ENUM_MEMBER:
        str = "EnumMember";
        break;
      case CONSTANT:
        str = "Constant";
        break;
      case STRUCT:
        str = "Struct";
        break;
      case EVENT:
        str = "Event";
        break;
      case OPERATOR:
        str = "Operator";
        break;
      case TYPE_PARAMETER:
        str = "TypeParameter";
        break;
      case UNKNOWN:
        str = "Unknown";
        break;
    }
    return str;
  }
  public GLib.Variant to_variant () {
    return new GLib.Variant.int64 (this);
  }
  public static CompletionItemKind parse_variant (GLib.Variant v) {
    return (CompletionItemKind) v.get_int64 ();
  }
  public static CompletionItemKind from_symbol_kind (SymbolKind k) {
    CompletionItemKind ret = CompletionItemKind.UNKNOWN;
    switch (k) {
      case SymbolKind.STRING:
        ret = CompletionItemKind.TEXT;
        break;
      case SymbolKind.METHOD:
        ret = CompletionItemKind.METHOD;
        break;
      case SymbolKind.FUNCTION:
        ret = CompletionItemKind.FUNCTION;
        break;
      case SymbolKind.CONSTRUCTOR:
        ret = CompletionItemKind.CONSTRUCTOR;
        break;
      case SymbolKind.FIELD:
        ret = CompletionItemKind.FIELD;
        break;
      case SymbolKind.VARIABLE:
        ret = CompletionItemKind.VARIABLE;
        break;
      case SymbolKind.CLASS:
        ret = CompletionItemKind.CLASS;
        break;
      case SymbolKind.INTERFACE:
        ret = CompletionItemKind.INTERFACE;
        break;
      case SymbolKind.MODULE:
        ret = CompletionItemKind.MODULE;
        break;
      case SymbolKind.PROPERTY:
        ret = CompletionItemKind.PROPERTY;
        break;
      case SymbolKind.ENUM:
        ret = CompletionItemKind.ENUM;
        break;
      case SymbolKind.FILE:
        ret = CompletionItemKind.FILE;
        break;
      case SymbolKind.ENUM_MEMBER:
        ret = CompletionItemKind.ENUM_MEMBER;
        break;
      case SymbolKind.CONSTANT:
        ret = CompletionItemKind.CONSTANT;
        break;
      case SymbolKind.STRUCT:
        ret = CompletionItemKind.STRUCT;
        break;
      case SymbolKind.EVENT:
        ret = CompletionItemKind.EVENT;
        break;
      case SymbolKind.OPERATOR:
        ret = CompletionItemKind.OPERATOR;
        break;
      case SymbolKind.TYPE_PARAMETER:
      case SymbolKind.OBJECT:
      case SymbolKind.BOOLEAN:
      case SymbolKind.UNKNOWN:
      case SymbolKind.KEY:
      case SymbolKind.NAMESPACE:
      case SymbolKind.ARRAY:
      case SymbolKind.NULL:
      case SymbolKind.PACKAGE:
      case SymbolKind.NUMBER:
        ret = CompletionItemKind.UNKNOWN;
        break;
    }
    return ret;
  }
}

