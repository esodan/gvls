/* gvls-rename-params.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.RenameParams : GLib.Object,
                                    TextDocumentPositionParams,
                                    WorkDoneProgressParams,
                                    GVo.Object
{
	/**
	 * The new name of the symbol. If the given name is not valid the
	 * request must return a [ResponseError](#ResponseError) with an
	 * appropriate message set.
	 */
	[Description (nick="newName")]
	public abstract string new_name  { get; set; }
}


public class GVls.RenameParamsInfo : GVls.TextDocumentPositionParamsInfo,
                                    GVls.WorkDoneProgressParams,
                                    GVls.RenameParams
{
	/**
	 * The new name of the symbol. If the given name is not valid the
	 * request must return a [ResponseError](#ResponseError) with an
	 * appropriate message set.
	 */
	[Description (nick="newName")]
	internal string new_name  { get; set; }

    [Description (nick="workDoneToken")]
	internal GLib.Variant? work_done_token { get; set; }
}
