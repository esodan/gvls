/* gvls-workspace.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.Workspace : GLib.Object, GVo.ContainerHashable
{
  public signal void did_change_workspace_folders (GVls.RequestMessage msg);
  public signal void configuration_request (GVls.RequestMessage msg);
  public signal void whatched_files_change (GVls.RequestMessage msg);
  public signal void requrested_symbol (GVls.RequestMessage msg);
  public signal void opened_text_document (GVls.RequestMessage msg);
  public signal void changed_text_document (GVls.RequestMessage msg);
  public signal void will_save_text_document (GVls.RequestMessage msg);
  public signal void will_save_wait_until_text_document (GVls.RequestMessage msg);
  public signal void saved_text_document (GVls.RequestMessage msg);
  public signal void closed_text_document (GVls.RequestMessage msg);
  public signal void file_added (GVls.FileEvent fe);
  public signal void file_deleted (GVls.FileEvent fe);
  public signal void file_changed (GVls.FileEvent fe);
  /**
   * List of {@link GLib.File} folders watched by this workspace.
   */
  public abstract GVo.Container folders { get; }
  /**
   * List of {@link GLib.File} files watched by this workspace.
   */
  public abstract GVo.Container whatched_files { get; }
}
