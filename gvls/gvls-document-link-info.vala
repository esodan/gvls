/* gvls-document-link-info.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * A document link is a range in a text document that links to an internal or external resource, like another
 * text document or a web site.
 */
public class GVls.DocumentLinkInfo : GLib.Object,
                                    GVls.DocumentLink,
                                    GVo.Object
{
	/**
	 * The range this link applies to.
	 */
    [Description (nick="range")]
	internal Range range { get; set; }
	/**
	 * The uri this link points to. If missing a resolve request is sent later.
	 */
    [Description (nick="target")]
	internal string? target { get; set; }
	/**
	 * A data entry field that is preserved on a document link between a
	 * DocumentLinkRequest and a DocumentLinkResolveRequest.
	 */
    [Description (nick="data")]
	internal  GVo.Object? data { get; set; }

	construct {
	    range = new GVls.SourceRange ();
	    target = null;
	}
}

