/* gvls-client-capabilities.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.MarkdownClientCapabilities : GLib.Object, GVo.Object {
    /**
     * The name of the parser.
     */
    [Description (nick="parser")]
    public abstract string parser { get; set; }

    /**
     * The version of the parser.
     */
    [Description (nick="version")]
    public abstract string version { get; set; }

    /**
     * A list of HTML tags that the client allows / supports in
     * Markdown.
     *
     * A container with an array of {@link GVo.String}
     *
     * @since 3.17.0
     */
    [Description (nick="allowedTags")]
    public abstract GVo.Container? allowed_tags { get; set; }
}
