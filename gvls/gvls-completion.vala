/* gvls-completion.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.Completion : GLib.Object {
  public signal void completion_request (GVls.RequestMessage msg);
  public signal void completion_item_request (GVls.RequestMessage msg);
  public signal void completion_hover_request (GVls.RequestMessage msg);
  public signal void completion_callable_signature_request (GVls.RequestMessage msg);
  public signal void completion_goto_request (GVls.RequestMessage msg);
  public signal void completion_goto_type_request (GVls.RequestMessage msg);
  public signal void completion_goto_implementation_request (GVls.RequestMessage msg);
  public signal void completion_find_references_request (GVls.RequestMessage msg);
  public signal void text_document_symbols_request (GVls.RequestMessage msg);

  public virtual void report_completion (GVls.RequestMessage msg, CompletionParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/completion";
    msg.@params.add (@params);
    completion_request (msg);
  }
  public virtual void report_completion_item_resolve (GVls.RequestMessage msg, CompletionItem @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/resolve";
    msg.@params.add (@params);
    completion_item_request (msg);
  }
  public virtual void report_completion_hover (GVls.RequestMessage msg, TextDocumentPositionParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/hover";
    msg.@params.add (@params);
    completion_hover_request (msg);
  }
  public virtual void report_completion_callable_signature (GVls.RequestMessage msg, TextDocumentPositionParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/signatureHelp";
    msg.@params.add (@params);
    completion_callable_signature_request (msg);
  }
  public virtual void report_completion_goto (GVls.RequestMessage msg, TextDocumentPositionParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/definition";
    msg.@params.add (@params);
    completion_goto_request (msg);
  }
  public virtual void report_completion_goto_type (GVls.RequestMessage msg, TextDocumentPositionParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/typeDefinition";
    msg.@params.add (@params);
    completion_goto_type_request (msg);
  }
  public virtual void report_completion_goto_implementation (GVls.RequestMessage msg, TextDocumentPositionParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/implementation";
    msg.@params.add (@params);
    completion_goto_implementation_request (msg);
  }
  public virtual void report_completion_find_references (GVls.RequestMessage msg, TextDocumentPositionParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/references";
    msg.@params.add (@params);
    completion_find_references_request (msg);
  }
  public virtual void report_document_symbols (GVls.RequestMessage msg, DocumentSymbolParams @params)
    requires (msg.@params != null)
  {
    msg.id = GLib.Uuid.string_random ();
    msg.method = "textDocument/documentSymbol";
    msg.@params.add (@params);
    text_document_symbols_request (msg);
  }
}
