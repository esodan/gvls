/* gvls-symbol-kind.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public enum GVls.SymbolKind {
  UNKNOWN,
  FILE,
  MODULE,
  NAMESPACE,
  PACKAGE,
  CLASS,
  METHOD,
  PROPERTY,
  FIELD,
  CONSTRUCTOR,
  ENUM,
  INTERFACE,
  FUNCTION,
  VARIABLE,
  CONSTANT,
  STRING,
  NUMBER,
  BOOLEAN,
  ARRAY,
  OBJECT,
  KEY,
  NULL,
  ENUM_MEMBER,
  STRUCT,
  EVENT,
  OPERATOR,
  TYPE_PARAMETER;
  public string to_string () {
    string str = "Unknown";
    switch (this) {
      case UNKNOWN:
        break;
      case FILE:
        str = "File";
        break;
      case MODULE:
        str = "Module";
        break;
      case NAMESPACE:
        str = "Namespace";
        break;
      case PACKAGE:
        str = "Package";
        break;
      case CLASS:
        str = "Class";
        break;
      case METHOD:
        str = "Method";
        break;
      case PROPERTY:
        str = "Property";
        break;
      case FIELD:
        str = "Field";
        break;
      case CONSTRUCTOR:
        str = "Constructor";
        break;
      case ENUM:
        str = "Enum";
        break;
      case INTERFACE:
        str = "Interface";
        break;
      case FUNCTION:
        str = "Function";
        break;
      case VARIABLE:
        str = "Variable";
        break;
      case CONSTANT:
        str = "Constant";
        break;
      case STRING:
        str = "String";
        break;
      case NUMBER:
        str = "Number";
        break;
      case BOOLEAN:
        str = "Boolean";
        break;
      case ARRAY:
        str = "Array";
        break;
      case OBJECT:
        str = "Object";
        break;
      case KEY:
        str = "Key";
        break;
      case NULL:
        str = "Null";
        break;
      case ENUM_MEMBER:
        str = "EnumMember";
        break;
      case STRUCT:
        str = "Struct";
        break;
      case EVENT:
        str = "Event";
        break;
      case OPERATOR:
        str = "Operator";
        break;
      case TYPE_PARAMETER:
        str = "TypeParameter";
        break;
    }
    return str;
  }
  public static SymbolKind from_string (string str) {
    SymbolKind k = UNKNOWN;
    if (str == "Unknown") {
      k = UNKNOWN;
    }
    if (str == "File") {
      k = FILE;
    }
    if (str == "Module") {
      k = MODULE;
    }
    if (str == "Namespace") {
      k = NAMESPACE;
    }
    if (str == "Package") {
      k = PACKAGE;
    }
    if (str == "Class") {
      k = CLASS;
    }
    if (str == "Method") {
      k = METHOD;
    }
    if (str == "Property") {
      k = PROPERTY;
    }
    if (str == "Field") {
      k = FIELD;
    }
    if (str == "Constructor") {
      k = CONSTRUCTOR;
    }
    if (str == "Enum") {
      k = ENUM;
    }
    if (str == "Interface") {
      k = INTERFACE;
    }
    if (str == "Function") {
      k = FUNCTION;
    }
    if (str == "Variable") {
      k = VARIABLE;
    }
    if (str == "Constant") {
      k = CONSTANT;
    }
    if (str == "String") {
      k = STRING;
    }
    if (str == "Number") {
      k = NUMBER;
    }
    if (str == "Boolean") {
      k = BOOLEAN;
    }
    if (str == "Array") {
      k = ARRAY;
    }
    if (str == "Object") {
      k = OBJECT;
    }
    if (str == "Key") {
      k = KEY;
    }
    if (str == "Null") {
      k = NULL;
    }
    if (str == "EnumMember") {
      k = ENUM_MEMBER;
    }
    if (str == "Struct") {
      k = STRUCT;
    }
    if (str == "Event") {
      k = EVENT;
    }
    if (str == "Operator") {
      k = OPERATOR;
    }
    if (str == "TypeParameter") {
      k = TYPE_PARAMETER;
    }
    return k;
  }
  public GLib.Variant to_variant () {
    return new GLib.Variant.int64 (this);
  }
  public static SymbolKind parse_variant (GLib.Variant v)  {
    return (SymbolKind) v.get_int64 ();
  }
}
