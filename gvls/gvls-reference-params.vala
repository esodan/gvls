/* gvls-reference-params.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.ReferenceParams : GLib.Object,
                                        GVo.Object,
                                        GVls.TextDocumentPositionParams,
                                        GVls.WorkDoneProgressParams,
                                        GVls.PartialResultParams
{
	[Description (nick="context")]
    public abstract GVls.ReferenceContext context { get; set; }
}

public interface GVls.ReferenceContext : GLib.Object,
                                    GVo.Object
{
    /**
	 * Include the declaration of the current symbol.
	 */
	[Description (nick="includeDeclaration")]
	public abstract bool include_declaration { get; set; }
}


public class GVls.ReferenceContextInfo : GLib.Object,
                                    GVo.Object,
                                    GVls.ReferenceContext
{
    /**
	 * Include the declaration of the current symbol.
	 */
	[Description (nick="includeDeclaration")]
	public bool include_declaration { get; set; }
}

public class GVls.ReferenceParamsInfo : GLib.Object,
                                        GVo.Object,
                                        GVls.PartialResultParams,
                                        GVls.WorkDoneProgressParams,
                                        GVls.TextDocumentPositionParams,
                                        GVls.ReferenceParams
{
    [Description (nick="context")]
    public GVls.ReferenceContext context { get; set; }

    [Description (nick="textDocument")]
    public TextDocumentIdentifier text_document { get; set; }
    [Description (nick="position")]
    public Position position { get; set; }
    [Description (nick="workDoneToken")]
	public Variant? work_done_token { get; set; }
    [Description (nick="partialResultToken")]
	public GLib.Variant? partial_result_token { get; set; }

	construct {
	    context = new GVls.ReferenceContextInfo ();
	    context.include_declaration = true;
	    text_document = new TextDocumentIdentifierInfo ();
	    position = new GVls.SourcePosition ();
	}
}
