/* gvls-markup-kind.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Describes the content type that a client supports in various
 * result literals like `Hover`, `ParameterInfo` or `CompletionItem`.
 *
 * Please note that `MarkupKinds` must not start with a `$`. This kinds
 * are reserved for internal usage.
 */
public enum GVls.MarkupKind {
  UNKNOWN,
  /**
   * Plain text is supported as a content format
   */
  PLAIN_TEXT,

  /**
   * Markdown is supported as a content format
   */
  MARKDOWN;

  public string to_string () {
    string str = "";
    switch (this) {
      case PLAIN_TEXT:
        str = "plaintext";
        break;
      case MARKDOWN:
        str = "markdown";
        break;
      default:
        str = "unknown";
        break;
    }
    return str;
  }
  
  [Version (since="0.14.2")]
  public static MarkupKind from_string (string str) {
    switch (str.down ()) {
        case "plaintext":
            return MarkupKind.PLAIN_TEXT;
        case "markdown":
            return MarkupKind.MARKDOWN;
        default:
            return MarkupKind.UNKNOWN;
    }
  }
}
