/* gvls-parameter-information.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * Represents a parameter of a callable-signature. A parameter can
 * have a label and a doc-comment.
 */
public interface GVls.ParameterInformation : GLib.Object
{
    /**
     * The label of this parameter. Will be shown in
     * the UI.
     */
    [Description(nick="label")]
    public abstract string label { get; set; }

    /**
     * The human-readable doc-comment of this parameter. Will be shown
     * in the UI but can be omitted.
     */
    public abstract MarkupContent documentation { get; set; }
}
