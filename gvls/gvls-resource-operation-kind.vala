/* gvls-resource-operation-kind.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public enum GVls.ResourceOperationKind {
  UNKNOWN,
  CREATE,
  RENAME,
  DELETE;
  public string to_string () {
    string str = "";
    switch (this) {
      case CREATE:
        str = "create";
        break;
      case RENAME:
        str = "rename";
        break;
      case DELETE:
        str = "delete";
        break;
      default:
        str = "unknown";
        break;
    }
    return str;
  }
  public static ResourceOperationKind from_string (string str) {
    switch (str.down ()) {
        case "create":
            return ResourceOperationKind.CREATE;
        case "rename":
            return ResourceOperationKind.RENAME;
        case "delete":
            return ResourceOperationKind.DELETE;
        default:
            return ResourceOperationKind.UNKNOWN;
    }
  }
}
