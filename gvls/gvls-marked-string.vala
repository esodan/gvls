/* gvls-marked-string.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * MarkedString can be used to render human readable text. It is either a markdown string
 * or a code-block that provides a language and a code snippet. The language identifier
 * is semantically equal to the optional language identifier in fenced code blocks in GitHub
 * issues. See [[https://help.github.com/articles/creating-and-highlighting-code-blocks/#syntax-highlighting]]
 *
 * The pair of a language and a value is an equivalent to markdown:
 * {{{
 * ```${language}
 * ${value}
 * ```
 * }}}
 *
 * Note that markdown strings will be sanitized - that means html will be escaped.
* {{{@deprecated}}} use MarkupContent instead.
*/
public class GVls.MarkedString : GLib.Object, GVo.Object {
    [Description (nick="language")]
    public string language { get; set; }
    [Description (nick="value")]
    public string @value { get; set; }
}

