/* gvls-string.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A utility class providing static methods to {@link GVo.String}
 */
public class GVls.StringUtils {

    /**
     * Calculates a {@link GVls.Range}
     * covering all given string
     *
     * @param content a string to calculate the range for
     */
    public static GVls.SourceRange
    calculate_range_full (string content)
    {
        unichar c = '\0';
        int index = 0;
        int col = 0;
        int l = 0;
        if (content != "") {
            while (content.get_next_char (ref index, out c) && index < content.length) {
                col++;
                if (c == '\n') {
                    l++;
                    col = 0;
                }
            }
        }

        GVls.SourceRange range = new GVls.SourceRange ();
        range.start.line = 0;
        range.start.character = 0;
        range.end.line = l;
        range.end.character = col;
        return range;
    }



    /**
     * Returns a substring representing the given {@link GVls.Range}
     * from the given string.
     *
     * @param content a string to calculate the substring for
     * @param range the {@link GVls.Range} used to calculate the string
     */
    public static string?
    calculate_string_from_range (string content, GVls.Range range)
    {
        if (range.start.line > range.end.line
            || range.end.character > range.end.character)
        {
            return null;
        }
        unichar c = '\0';
        int index = 0;
        int cs = 0;
        int ls = 0;
        int ce = 0;
        int le = 0;
        string ret = "";
        while (ls != range.start.line) {
            if (!content.get_next_char (ref index, out c)) {
                break;
            }
            if (c == '\n') {
                ls++;
            }
        }

        while (cs != range.start.character) {
            if (!content.get_next_char (ref index, out c)) {
                break;
            }
            cs++;
        }

        le = ls;

        while (content.get_next_char (ref index, out c)) {
            ce++;
            if (le == range.end.line) {
                if (ce > range.end.character) {
                    break;
                }
            }

            if (c == '\n') {
                le++;
                ce = 0;
                if (le > range.end.line) {
                    break;
                }
            }

            ret += c.to_string ();
        }

        return ret;
    }
}

