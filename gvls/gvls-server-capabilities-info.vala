/* gvls-gserver-capabilities.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class GVls.ServerCapabilitiesInfo : GLib.Object, GVo.Object, ServerCapabilities {
    [Description (nick="positionEncoding")]
	internal string? position_encoding { get; set; }
    [Description (nick="textDocumentSync")]
    internal TextDocumentSyncOptions text_document_sync { get; set; }
    [Description (nick="hoverProvider")]
    [Version(since="0.16")]
    internal GLib.Variant hover_provider { get; set; }
    [Description (nick="completionProvider")]
    internal CompletionOptions completion_provider { get; set; }
    [Description (nick="signatureHelpProvider")]
    internal SignatureHelpOptions signature_help_provider { get; set; }
    [Description (nick="definitionProvider")]
    internal bool definition_provider { get; set; }
    [Description (nick="typeDefinitionProvider")]
    internal bool type_definition_provider { get; set; }
    [Description (nick="implementationProvider")]
    internal bool implementation_provider { get; set; }
    [Description (nick="referencesProvider")]
    internal bool references_provider { get; set; }
    [Description (nick="documentHighlightProvider")]
    internal bool document_highlight_provider { get; set; }
    [Description (nick="documentSymbolProvider")]
    internal bool document_symbol_provider { get; set; }
    [Description (nick="workspaceSymbolProvider")]
    internal bool workspace_symbol_provider { get; set; }
    [Description (nick="codeActionProvider")]
    internal bool code_action_provider { get; set; }
    [Description (nick="codeLensProvider")]
    internal CodeLensOptions code_lens_provider { get; set; }
    [Description (nick="documentFormattingProvider")]
    internal bool document_formatting_provider { get; set; }
    [Description (nick="documentRangeFormattingProvider")]
    internal bool document_range_formatting_provider { get; set; }
    [Description (nick="documentOnTypeFormattingProvider")]
    internal DocumentOnTypeFormattingOptions documen_on_type_formatting_provider { get; set; }
    [Description (nick="renameProvider")]
    internal bool rename_provider { get; set; }
    [Description (nick="documentLinkProvider")]
    internal DocumentLinkOptions document_link_provider { get; set; }
    [Description (nick="colorProvider")]
    internal bool color_provider { get; set; }
    [Description (nick="foldingRangeProvider")]
    internal bool folding_range_provider { get; set; }
    [Description (nick="executeCommandProvider")]
    internal ExecuteCommandOptions execute_command_provider { get; set; }
    [Description (nick="workspace")]
    internal ServerCapabilities.Workspace workspace { get; set; }
    [Description (nick="experimental")]
    internal GVo.Container experimental { get; set; }

    construct {
        text_document_sync = new TextDocumentSyncOptionsInfo ();
        completion_provider = new CompletionOptionsInfo ();
        signature_help_provider = new SignatureHelpOptionsInfo ();
        code_lens_provider = new CodeLensOptionsInfo ();
        documen_on_type_formatting_provider = new DocumentOnTypeFormattingOptionsInfo ();
        document_link_provider = new DocumentLinkOptionsInfo ();
        execute_command_provider = new ExecuteCommandOptionsInfo ();
        workspace = new ServerCapabilities.Workspace ();
        experimental = new GVo.ContainerHashList ();
        position_encoding = "utf-8";
    }
}

