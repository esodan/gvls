/* gvls-signature-help-context.vala
 *
 * Copyright 2020 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Additional information about the context in which a signature help request was triggered.
 *
 * @since 3.15.0
 */
[Version(since="0.16")]
public interface GVls.SignatureHelpContext : GLib.Object
{
	/**
	 * Action that caused signature help to be triggered.
	 */
	[Description(nick="triggerKind")]
	public abstract SignatureHelpTriggerKind trigger_kind { get; set; }

	/**
	 * Character that caused signature help to be triggered.
	 *
	 * This is undefined when `triggerKind !== SignatureHelpTriggerKind.TriggerCharacter`
	 */
	[Description(nick="triggerCharacter")]
	public abstract string? trigger_character { get; set; }

	/**
	 * `true` if signature help was already showing when it was triggered.
	 *
	 * Retriggers occur when the signature help is already active and can be caused by actions such as
	 * typing a trigger character, a cursor move, or document content changes.
	 */
	[Description(nick="isRetrigger")]
	public abstract bool is_retrigger { get; set; }

	/**
	 * The currently active `SignatureHelp`.
	 *
	 * The `activeSignatureHelp` has its `SignatureHelp.activeSignature` field updated based on
	 * the user navigating through available signatures.
	 */
	[Description(nick="activeSignatureHelp")]
	public abstract SignatureHelp? active_signature_help { get; set; }
}
