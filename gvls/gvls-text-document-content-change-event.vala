/* gvls-text-document-content-change-event.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * An event describing a change to a text document. If range and rangeLength are omitted
 * the new text is considered to be the full content of the document.
 */
public interface GVls.TextDocumentContentChangeEvent :GLib.Object, GVo.Object {
  /**
   * The range of the document that changed.
   */
  public abstract Range? range { get; set; }

  /**
   * The length of the range that got replaced.
   */
  public abstract int range_length { get; }

  /**
   * The new text of the range/document.
   */
  public abstract string text  { get; set; }
}

