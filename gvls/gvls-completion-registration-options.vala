/* gvls-completion-registration-options.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public interface GVls.CompletionRegistrationOptions : GLib.Object, TextDocumentRegistrationOptions {
  /**
   * Most tools trigger completion request automatically without explicitly requesting
   * it using a keyboard shortcut (e.g. Ctrl+Space). Typically they do so when the user
   * starts to type an identifier. For example if the user types `c` in a JavaScript file
   * code complete will automatically pop up present `console` besides others as a
   * completion item. Characters that make up identifiers don't need to be listed here.
   *
   * If code complete should automatically be trigger on characters not being valid inside
   * an identifier (for example `.` in JavaScript) list them in `triggerCharacters`.
   *
   * Container's items are of type {@link GVo.String}
   */
  public abstract GVo.Container trigger_characters  { get; }

  /**
   * The server provides support to resolve additional
   * information for a completion item.
   */
  public abstract bool resolve_provider { get; set; }
}

