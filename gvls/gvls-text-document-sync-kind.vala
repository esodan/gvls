/* gvls-text-document-sync-kind.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Defines how the host (editor) should sync document changes to the language server.
 */
public enum GVls.TextDocumentSyncKind {
  /**
   * Documents should not be synced at all.
   */
  NONE,

  /**
   * Documents are synced by always sending the full content
   * of the document.
   */
  FULL,

  /**
   * Documents are synced by sending the full content on open.
   * After that only incremental updates to the document are
   * send.
   */
  INCREMENTAL;
  public string to_string () {
    string str = "";
    switch (this) {
      case NONE:
        str = "None";
        break;
      case FULL:
        str = "Full";
        break;
      case INCREMENTAL:
        str = "Incremental";
        break;
    }
    return str;
  }
}
