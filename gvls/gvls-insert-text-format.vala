/* gvls-insert-text-format.vala
 *
 * Copyright 2018 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Defines whether the insert text in a completion item should be interpreted as
 * plain text or a snippet.
 */
public enum GVls.InsertTextFormat {
    UNKNOWN,
    /**
    * The primary text to be inserted is treated as a plain string.
    */
    PLAIN_TEXT,

    /**
    * The primary text to be inserted is treated as a snippet.
    *
    * A snippet can define tab stops and placeholders with `$1`, `$2`
    * and `${3:foo}`. `$0` defines the final tab stop, it defaults to
    * the end of the snippet. Placeholders with equal identifiers are linked,
    * that is typing in one will update others too.
    */
    SNIPPED;

    public string to_string () {
        string str = "";
        switch (this) {
          case PLAIN_TEXT:
            str = "PlainText";
            break;
          case SNIPPED:
            str = "Snippet";
            break;
          default:
            str = "Unknown";
            break;
        }
        return str;
    }

    [Version (since="0.16")]
    public static InsertTextFormat from_string (string str) {
        switch (str.down ()) {
            case "plaintext":
                return InsertTextFormat.PLAIN_TEXT;
            case "snippet":
                return InsertTextFormat.SNIPPED;
            default:
                return InsertTextFormat.UNKNOWN;
        }
    }
}
