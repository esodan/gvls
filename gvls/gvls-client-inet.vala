/* gvlsp-client-inet.vala
 *
 * Copyright 2018-2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Jsonrpc;
using GLib;
using GVls;

/**
 * An {@link ClientJsonrpc} to connect to servers running over TCP/IP
 * at any address.
 *
 * By default 127.0.0.1:1204 address is used.
 */
public class GVls.ClientInet : GVls.ClientJsonrpc {
    public uint16 port { get; set; default = 1024; }
    public string ip_address { get; set; default = "127.0.0.1"; }

    public new void connect () {
        connect_string (ip_address);
    }

    public void connect_string (string direction) {
        InetAddress address = new InetAddress.from_string (direction);
        connect_address (address);
        ip_address = address.to_string ();
    }

    public void connect_default () {
        InetAddress address = new InetAddress.from_bytes ({127, 0, 0, 1}, SocketFamily.IPV4);
        connect_address (address);
    }

    private void connect_address (InetAddress address) {
        SocketClient client = new SocketClient ();
        InetSocketAddress socket_address;
        SocketConnection conn = null;
        while (conn == null) {
            socket_address = new InetSocketAddress (address, port);
            try {
                conn = client.connect (socket_address);
            } catch (GLib.Error e) {
                warning (_("Error Connecting to server at address: %s : %s"),
                        address.to_string (),
                        e.message);
            }

            if (conn != null) {
                break;
            }

            port++;
            if (port > 1030) {
                break;
            }
        }
        if (conn == null) {
            warning ("No connection to the server. Aborted");
        }
        accept_io_stream (conn);
    }
}
