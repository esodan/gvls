/* gvls-completion-context-info.vala
 *
 * Copyright 2019 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Contains additional information about the context in which a completion request is triggered.
 */
public class GVls.CompletionContextInfo : GLib.Object,
                                        GVo.Object,
                                        CompletionContext
{
    [Description (nick="triggerKind")]
    internal CompletionTriggerKind trigger_kind { get; set; }
    [Description (nick="triggerCharacter")]
    internal string? trigger_character { get; set; }
    [Description (nick="word")]
    internal string? word { get; set; }
}
