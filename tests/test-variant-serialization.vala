/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * GVls Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * GVls is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GVls is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GVls;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/gvls/variant/workspace-client-capabilities",
    ()=>{
      var obj = new WorkspaceClientCapabilitiesInfo ();
      var v = obj.to_variant ();
      message ("Variant: %s", v.print (true));
      assert (v.n_children () == 8);
    });
    Test.add_func ("/gvls/variant/client-capabilities",
    ()=>{
      var obj = new ClientCapabilitiesInfo ();
      var v = obj.to_variant ();
      message ("Variant: %s", v.print (true));
    });
    Test.add_func ("/gvls/variant/completion-list",
    ()=>{
      var obj = new CompletionListInfo ();
      var item = new CompletionItemTiny.with_data ("Label", "Detail", GVls.CompletionItemKind.CLASS);
      obj.items.add (item);
      var v = obj.to_variant ();
      message ("Serialized Variant: %s", v.print (true));
      var nobj = new CompletionListInfo ();
      nobj.parse_variant (v);
      var v2 = nobj.to_variant ();
      message ("Parsed Variant: %s", v2.print (true));
    });
    return Test.run ();
  }
}
