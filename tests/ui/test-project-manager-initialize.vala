/*
 * libvdaui Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2021 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GVls;
using GLsvui;

class Tests {
    static int main (string[] args)
    {
        GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
        Test.init (ref args);
        Gtk.init (ref args);
        Test.add_func ("/gvlsui/sourceview/init",
        ()=>{
            GLib.File dir = GLib.File.new_for_path (TestConfig.BUILD_DIR+"/meson-project");
            message (dir.get_uri ());
            assert (dir.query_exists ());
            GLib.File file = GLib.File.new_for_path (TestConfig.BUILD_DIR+"/meson-project/class-a.vala");
            assert (file.query_exists ());
            var manager = new GLsvui.ProjectManager.for_meson (dir);
            var win = new Gtkt.WindowTester ();
            var w = new Gtk.SourceView ();
            w.hexpand = true;
            w.set_wrap_mode (Gtk.WrapMode.WORD);
            w.set_auto_indent (true);
            w.set_indent_on_tab (true);
            win.waiting_for_event = TestConfig.INTERACTIVE;
            var sw = new Gtk.ScrolledWindow (null, null);
            sw.add (w);
            sw.min_content_height = 200;
            sw.vexpand = true;
            win.widget = sw;
            win.add_test ("Parse pre-defined buffer", "A text on view should be parsed. Try completion a symbol");
            win.initialize.connect (()=>{
                manager.manager.server_path = TestConfig.LSP_SERVER_STDIO_PATH;
                manager.manager.initialize_stdio.begin ((obj, res)=>{
                    try {
                        var ostream = new MemoryOutputStream.resizable ();
                        ostream.splice (file.read (), OutputStreamSpliceFlags.CLOSE_SOURCE, null);
                        var t = (string) ostream.get_data ();;
                        var buf = w.get_buffer ();
                        buf.text = t;
                        manager.manager.initialize_stdio.end (res);
                        manager.set_completion_provider (w, file);
                        manager.open (w, file);
                    } catch (GLib.Error e) {
                        warning ("Error Opening File: %s", e.message);
                    }
                });
            });

            win.destroy.connect (()=>{
               manager.manager.client.server_exit.begin ();
            });

            win.show_all ();
            Gtk.main ();
        });
        return Test.run ();
    }
}

