/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * libvdaui Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2018 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GVls;
using GLsvui;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Gtk.init (ref args);
    Test.add_func ("/GLsvui/sourceview/format/symbols",
    ()=>{
      GLib.File dir = GLib.File.new_for_path (TestConfig.BUILD_DIR+"/meson-project");
      message (dir.get_uri ());
      assert (dir.query_exists ());
      GLib.File file = GLib.File.new_for_path (TestConfig.BUILD_DIR+"/meson-project/class-a.vala");
      assert (file.query_exists ());
      var win = new Gtkt.WindowTester ();
      var bs = new GLsv.BuildSystemMesonVala (dir);
      var manager = new GLsvui.ProjectManager (dir, bs);
      var w = new GLsvui.SourceView (manager);
      assert (w.buffer != null);
      w.hexpand = true;
      w.set_wrap_mode (Gtk.WrapMode.WORD);
      w.set_auto_indent (true);
      w.set_indent_on_tab (true);
      win.waiting_for_event = TestConfig.INTERACTIVE;
      var sw = new Gtk.ScrolledWindow (null,null);
      sw.add (w);
      win.widget = sw;
      win.add_test ("Syntax highligth", "keywords/types");
      win.initialize.connect (()=>{
        manager.manager.server_path = TestConfig.LSP_SERVER_STDIO_PATH;
        manager.manager.initialize_stdio.begin (()=>{
          message ("Setting Text to Buffer");
          w.buffer.text = """
public class Test {}
""";
          });
        });
      win.show_all ();
      Gtk.main ();
    });
    return Test.run ();
  }
}
