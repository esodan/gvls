/*
 * libvdaui Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2021 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GVls;
using GLsvui;

class Tests {
    static int main (string[] args)
    {
        GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
        Test.init (ref args);
        Gtk.init (ref args);
        Test.add_func ("/gvlsui/projec-manager/cache-symbols",
        ()=>{
            GLib.File dir = GLib.File.new_for_path (TestConfig.BUILD_DIR+"/meson-project");
            message (dir.get_uri ());
            assert (dir.query_exists ());
            GLib.File file = GLib.File.new_for_path (TestConfig.BUILD_DIR+"/meson-project/class-a.vala");
            assert (file.query_exists ());
            var manager = new GLsvui.ProjectManager.for_meson (dir);
            var win = new Gtkt.WindowTester ();
            var w = new GLsvui.SourceView (manager);
            w.hexpand = true;
            w.set_wrap_mode (Gtk.WrapMode.WORD);
            w.set_auto_indent (true);
            w.set_indent_on_tab (true);
            win.waiting_for_event = TestConfig.INTERACTIVE;
            var sw = new Gtk.ScrolledWindow (null, null);
            sw.add (w);
            sw.min_content_height = 200;
            sw.vexpand = true;
            win.widget = sw;
            win.add_test ("Parse pre-defined buffer", "A text on view should be parsed. Try completion a symbol");
            win.initialize.connect (()=>{
                manager.manager.server_path = TestConfig.LSP_SERVER_STDIO_PATH;
                manager.manager.initialize_stdio.begin ((obj, res)=>{
                    try {
                        manager.manager.initialize_stdio.end (res);
                        w.open_file.begin (file, (o,r)=>{
                            try {
                                w.open_file.end (r);
                                manager.request_document_symbols.begin (file.get_uri (), (o,r)=>{
                                    try {
                                        var syms = manager.request_document_symbols.end (r);
                                        assert (syms.get_n_items () > 0);
                                        var p = new GVls.SourcePosition.from_values (8, 9);
                                        GVls.Range range = null;
                                        message ("\n%s", w.get_buffer ().text);
                                        string word = GVls.SymbolProvider.calculate_position_word_left (w.get_buffer ().text, p, out range);
                                        message ("Word at position: '%s'", word);
                                        assert (word == "a");
                                        manager.completion_items_at.begin (file.get_uri (), p, w, (op, rp)=>{
                                            try {
                                                var lp = manager.completion_items_at.end (rp);
                                                assert (lp != null);
                                                assert (lp.length () > 0);
                                                var lp2 = manager.completion_items (file.get_uri (), "a");
                                                assert (lp2 != null);
                                                assert (lp2.length () > 0);
                                                message ("Requesting completion item for a word ClassB");
                                                manager.request_completion_items.begin (file.get_uri (), "Classes.ClassB", (oc,rc)=>{
                                                    try {
                                                        message ("End requesting completion item for a word Classes.ClassB");
                                                        var completion1 = manager.request_completion_items.end (rc);
                                                        assert (completion1 != null);
                                                        assert (completion1.get_n_items () > 0);
                                                        bool found1 = false;
                                                        bool found2 = false;
                                                        for (int i = 0; i < completion1.get_n_items (); i++) {
                                                            var item = completion1.get_item (i) as GVls.CompletionItem;
                                                            assert (item != null);
                                                            if (item.label == "weak_ref") {
                                                                found1 = true;
                                                            }

                                                            if (item.label == "name") {
                                                                found2 = true;
                                                            }
                                                        }
                                                        assert (found1);
                                                        assert (found2);

                                                        message ("Requiring cached completion item for a word Classes.ClassB");
                                                        var l1 = manager.completion_items (file.get_uri (), "Classes.ClassB");
                                                        assert (l1.length () > 0);
                                                        found1 = false;
                                                        found2 = false;
                                                        foreach (Gtk.SourceCompletionProposal pp in l1) {
                                                            if (pp.get_label () == "weak_ref") {
                                                                found1 = true;
                                                            }

                                                            if (pp.get_label () == "name") {
                                                                found2 = true;
                                                            }
                                                        }
                                                        assert (found1);
                                                        assert (found2);
                                                        Gtk.main_quit ();
                                                    } catch (GLib.Error e) {
                                                        warning ("Error: %s", e.message);
                                                    }
                                                });
                                            } catch (GLib.Error e) {
                                                warning ("Error: %s", e.message);
                                            }
                                        });
                                    } catch (GLib.Error e) {
                                        warning ("Error: %s", e.message);
                                    }
                                });
                            } catch (GLib.Error e) {
                                warning ("Error while opening file: %s: %s", file.get_uri (), e.message);
                            }
                        });
                    } catch (GLib.Error e) {
                        warning ("Error Opening File: %s", e.message);
                    }
                });
            });

            win.destroy.connect (()=>{
               manager.manager.client.server_exit.begin ();
            });

            win.show_all ();
            Gtk.main ();
        });
        return Test.run ();
    }
}

