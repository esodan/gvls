using Gtk;


public interface SymTest.Devices : Object {
    public abstract GLib.File file { get; set; }
}
public class SymTest.Computer : Object, Devices {
    public GLib.InputStream stream { get; set; }
}
public class SymTest.Laptop : Computer {
    public float weight { get; set; }
    public class Gaming : Object {
        public string name { get; set; }
        public void method (string str) throws GLib.Error {
            message ("String: %s", str);
        }
        
        public string to_string (int size) {
            return "a b";
        } 
    }
}
public class SymTest.App : Object {
    public void main () {
        var ostream = new MemoryOutputStream.resizable ();
        ostream.splice (file.read (), OutputStreamSpliceFlags.CLOSE_SOURCE, null);
        Laptop l = new Laptop ();
        Laptop.Gaming gl = new Laptop.Gaming ();
        gl.method ("Text");
        var str = gl.to_string ();
        var a = {"e","u","k"};
        var c = (Computer) l;
        var v = l as Computer;
        var t = typeof (Laptop.Gaming);
        var b = 1 != 0;
        double k = 3.5;
        var ag = k = 8.8;
        var cond = k > 0 ? "text" : "Tom";
        var inc = k += 1.1;
        var part = "Text"[0:2];
        var elem = "Text"[2];
        var func = ()=>{ return true; };
        var array = new int64[3];
        var tmpl = @"$k";
        int i = 0;
        var j = i++;
        var ptr = *i;
        var dptr = &i;
        var d = new Window ();
    }

    public HashMap<string, App> element { get; set; }

    public Type type_calc (Set col, App element) {
        return element.get_type ();
    }

    public void doit () {
        string text = "";
        int integer = 1;
        double d = 0.1;
        Gee.ArrayList<string> arr = new Gee.ArrayList<string> ();
        arr.add ("t");
    }

    /*
     * Copyright 2020, Yannick Inizan
     */
    static void titi (int nombre, SourceFunc delegue) {
	    string var_a = "toto";
	    double var_b = 33.3363;
    }
    /*
     * Copyright 2020, Yannick Inizan
     */
    static void toto() {
	    var nombre = 25.36536;

	    titi (33, () => {
		    var chaine = "toto";


		    return false;
	    });
	    this.element.set (chaine, new App ());
    }

    public class ModelA : GLib.Object  {
        public SymTest.Laptop.Gaming component { get; set; }

        public class ModelAB : GLib.Object {
            public SymTest.Laptop.Gaming creating_gaming () {
                return new SymTest.Laptop.Gaming ();
            }
        }

        public App create () {
            var a = new SymTest.App ();
            return a;
        }

        public string methodA (string some) {
            var app = this.create ();
            component = new SymTest.Laptop.Gaming ();
            var s = component.to_string ();
            var mg = new ModelAB ();
            var gamestr = mg.creating_gaming ().to_string ();
        }
    }
}

public class SymTest.AppT : Object {
    private SymTest.App _app;
    private SymTest.App.ModelA _modela;

    public void call () {
        var t = _modela.methodA ("Text");
    }
}

public class BaseCalc : Object {
    public int time () {
        return 0;
    }
}

public interface IBase : Object {
    public abstract double calc_time ();
}

public class Calc : BaseCalc {
    internal double calc_time () {
        return 0;
    }
    public int calc () {
        var tm = calc_time ();
        Phone ph = new Phone ();
        bool called = ph.dial (123);
    }
}

public class SubCalc : Object, IBase {
    public string tax { get; set; }


    public Calc gen_calc (int val) {
        var c = new Calc ();


        return c;
    }


    public void take_calc () {
        var cl = gen_calc (3);



        Phone ph1 = new Phone ();
        var c1 = ph1.dial (123);
    }
}
