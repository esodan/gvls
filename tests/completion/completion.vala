using Cairo;
/**
 *
 */
public class Completion.App {
  /**
   * App name to use
   * with all yuor apps
   */
  public string name { get; set; }
  public void method (string val) {}
}
/**
 * Test documentation one line
 */
public class Completion.App2 {
  /**
   * Test documentation with
   * multiple lines in the
   * help text
   */
  public void callme () {
      App p = new App ();
      p.
  }
}

/**
 * Test interface with documentation
 * in place, multiple paragrap.
 *
 * Second paragraph.
 */
public interface Completion.IfaceApp {
  public abstract void callme () {}
}

public class Main : Object {
  public static void main () {
    for (int i = 0; i < 0; i++) {
      stdout.printf ("Val: %d", i);
    }
  }
}

