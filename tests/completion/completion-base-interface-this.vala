public interface Completion.TopInterfaceINumberThis : GLib.Object
{
    public abstract int number { get; set; }
}
public interface Completion.TopInterfaceINamedThis : GLib.Object
{
    public abstract string name { get; set; }
}
publc interface Completion.TopString : GLib.Object {
    public abstract int count ();
}
public class Completion.TopClassIThis : GLib.Object
{
    public class SubClassH : Completion.TopClassG,
                            Completion.TopInterfaceINumber,
                            Completion.TopInterfaceINamed
    {
        internal int number { get; set; }
        internal string name { get; set; }
        public GLib.Object get_object () {
            return this;
        }
    }
    public class SubClassI : SubClassH {
        public string chap () { return ""; }
    }
    public class SubClassJ : SubClassI {
        public string crop () { return ""; }
    }
    public class SubClassK : SubClassJ {
        public string title () { return ""; }
    }
    public class SubClassL : SubClassK {
        internal int counting () { return 0; }
        public string paragraph () { return ""; }
    }
    public class SubClassM : SubClassL {
        public string join () {
            this.
        }
    }

    public string uri () {
        var sc = new SubClassI ();
    }
}
