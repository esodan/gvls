CREATE TABLE IF NOT EXISTS symbols (id INTEGER PRIMARY KEY AUTOINCREMENT, full_name TEXT, name TEXT, documentation TEXT, code TEXT, file TEXT, kind INTEGER, data_type TEXT, container_name TEXT, location_line_start INTEGER, location_character_start INTEGER, location_line_end INTEGER, location_character_end INTEGER, deprecated BOOLEAN, detail TEXT, selection_line_start INTEGER, selection_character_start INTEGER, selection_line_end INTEGER, selection_character_end INTEGER, UNIQUE (full_name, file));

INSERT INTO symbols (full_name, name, file, data_type, location_line_start, location_character_start, location_line_end, location_character_end) VALUES ('App.name', 'name', 'file:///', 'string', 1, 1, 1, 3);

INSERT INTO symbols (full_name, name, file, data_type, location_line_start, location_character_start, location_line_end, location_character_end) VALUES ('block::1', 'block::1', 'file:///', 'string', 1, 1, 3, 0);


INSERT INTO symbols (full_name, name, file, data_type, location_line_start, location_character_start, location_line_end, location_character_end) VALUES ('name', 'name', 'file:///', 'string', 2, 1, 2, 3);

# Posisition at Line 2, Character 3 Get Symbol at
SELECT * FROM symbols WHERE file = 'file:///' AND NOT ((location_line_start > 2 OR location_line_end < 2) || (2 = location_line_start AND 3 < location_character_start) || (2 = location_line_end AND 3 > location_character_end)) AND full_name NOT LIKE 'block::%';

# Block container of position Line 2, Character 3
SELECT * FROM symbols WHERE file = 'file:///' AND NOT ((location_line_start > 2 OR location_line_end < 2) || (2 = location_line_start AND 3 < location_character_start) || (2 = location_line_end AND 3 > location_character_end)) AND full_name LIKE 'block::%';

# All symbols with a given name and URI
SELECT * FROM symbols where name = 'name' AND file = 'file:///';


# All symbols of kind VARIABLE with the given name and URI
SELECT * FROM symbols WHERE name = 'name' AND file = 'file:///' AND kind = 13;


# Search a block containing given $line and $char position
SELECT location_line_start AS line_start, location_character_start AS char_start, location_line_end AS line_end, location_character_end AS char_end FROM symbols WHERE file = $uri AND full_name LIKE 'block::%' AND NOT ((location_line_start > $line OR location_line_end < $line) OR ($line = location_line_start AND $char < location_character_start) OR ($line = location_line_end AND $char > location_character_end))

# Search a symbol contained in a given range $line_start, $char_start, $line_end, $char_end, given $name and given $uri
SELECT * FROM symbols WHERE name = $name AND file = $uri AND NOT ((location_line_start < $line_start OR location_line_end > $line_end) OR ($line_start = location_character_start AND location_character_end < $char_start)  OR ($line_end = location_line_end AND location_character_end > $char_end))



# Search symbols with given name and URI inside a block that contains a position
WITH block AS (SELECT location_line_start AS line_start, location_character_start AS char_start, location_line_end AS line_end, location_character_end AS char_end FROM symbols WHERE file = $uri AND full_name LIKE 'block::%' AND NOT ((location_line_start > $line OR location_line_end < $line) OR ($line = location_line_start AND $char < location_character_start) OR ($line = location_line_end AND $char > location_character_end))), syms AS (SELECT * FROM symbols WHERE name = $name AND file = $uri) SELECT * FROM syms AS s JOIN block AS b ON NOT ((s.location_line_start < b.line_start OR s.location_line_end > b.line_end) OR (b.line_start = s.location_character_start AND s.location_character_end < b.char_start)  OR (b.line_end = s.location_line_end AND s.location_character_end > b.char_end));

WITH block AS (SELECT location_line_start AS line_start, location_character_start AS char_start, location_line_end AS line_end, location_character_end AS char_end FROM symbols WHERE file = $uri AND full_name LIKE 'block::%' AND NOT ((location_line_start > $line OR location_line_end < $line) || ($line = location_line_start AND $char < location_character_start) || ($line = location_line_end AND $char > location_character_end))), syms AS (SELECT * FROM symbols WHERE name = $name AND file = $uri) SELECT s.name, s.kind FROM syms AS s JOIN block AS b ON NOT ((s.location_line_start < b.line_start OR s.location_line_end > b.line_end) || (b.line_start = s.location_line_start AND s.location_character_start < b.char_start) || (b.line_end = s.location_line_end AND s.location_character_end > b.char_end));
