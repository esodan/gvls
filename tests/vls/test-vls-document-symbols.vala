/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * GVls Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2022 <esodan@gmail.com>
 *
 * GVls is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GVls is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GVls;

class GVlsTest.Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    //Test.add_func ("/gvls/lsp/stdio/completion",
    //()=>{
      var loop = new MainLoop ();
      GVlsTest.Client client = new GVlsTest.Client ();
      try {
        client.launch_server ();
      } catch (GLib.Error e) {
        warning ("Error launching server subprocess: %s", e.message);
      }

      File f = File.new_for_path (TestConfig.BUILD_DIR+"/meson-autoconfigure/class-a.vala");

      client.did_initialization.connect (()=>{
        assert (client.initialized);
        message ("End initialization...");
      });

      client.notified_initialized.connect (()=>{
        try {
          assert (client.initialized);
          message ("End initialization...");
          assert (client.initialized);
          message ("Load document");
          var istream = new GLib.DataInputStream (f.read ());
          string text = istream.read_upto ("\0", -1, null);
          client.document_open.begin (f.get_uri (), text, (object, res)=>{
            message ("End opening document...");
            try {
              client.document_open.end (res);
              client.document_symbols.begin (f.get_uri (), (obj, res)=>{
                try {
                  message ("End query document symbols");
                  var c = client.document_symbols.end (res);
                  assert (c != null);
                  assert (c.get_n_items () != 0);
                  bool found = false;
                  for (int i = 0; i < c.get_n_items (); i++) {
                    var item = c.get_item (i) as DocumentSymbol;
                    if (item == null) {
                      warning ("Error in document symbol type list");
                      break;
                    }
                    message (item.name);
                    if (item.name == "Classes") {
                      found = true;
                      message ("Classes: %s", item.to_variant ().print (true));
                      assert (item.range.start.line != -1);
                    }
                  }
                  message ("Symbols %u at %s", c.get_n_items (), f.get_uri ());
                  assert (found);

                  message ("Sending exit request");
                  client.server_exit.begin ((obj, res)=>{
                    try {
                      client.server_exit.end (res);
                      message ("Sent exit notification");
                      loop.quit ();
                    } catch (GLib.Error e) {
                      warning ("Error: %s", e.message);
                    }
                  });
                } catch (GLib.Error e) {
                  warning ("Error: %s", e.message);
                }
              });
            }  catch (GLib.Error e) {
              message ("Error: %s", e.message);
              assert_not_reached ();
            }
          });
        } catch (GLib.Error e) {
          message ("Error: %s", e.message);
          assert_not_reached ();
        }
      });
      Idle.add (()=>{
        message ("Initialize Server");
        File d = File.new_for_path (TestConfig.BUILD_DIR+"/meson-autoconfigure");
        assert (d.query_exists ());
        client.initialize.begin (d.get_uri (), (obj, res)=>{
          try {
            client.initialize.end (res);
          } catch (GLib.Error e) {
            warning ("Error: %s", e.message);
          }
        });
        return Source.REMOVE;
      });
      loop.run ();
    //});
    // return Test.run ();
    return 0;
    //
  }
}

