/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
 * GVls Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * GVls is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * GVls is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GVls;

class Tests {
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    //Test.add_func ("/gvls/lsp/stdio/completion",
    //()=>{
      var loop = new MainLoop ();
      var client = new GVlsTest.Client ();
      try {
        client.launch_server ();
      } catch (GLib.Error e) {
        warning ("Error launching server subprocess: %s", e.message);
      }
      
      client.did_initialization.connect (()=>{
        assert (client.initialized);
        message ("End initialization...");
      });

      client.notified_initialized.connect (()=>{
        message ("Sending exit request");
        client.server_exit.begin ((obj, res)=>{
          try {
            client.server_exit.end (res);
            message ("Sent exit notification");
            loop.quit ();
          } catch (GLib.Error e) {
            warning ("Error: %s", e.message);
          }
        });
      });
      Idle.add (()=>{
        message ("Initialize Server");
        File d = File.new_for_path (TestConfig.BUILD_DIR+"/meson-autoconfigure");
        assert (d.query_exists ());
        client.initialize.begin (d.get_uri (), (obj, res)=>{
          try {
            client.initialize.end (res);
          } catch (GLib.Error e) {
            warning ("Error: %s", e.message);
          }
        });
        return Source.REMOVE;
      });
      loop.run ();
    //});
    // return Test.run ();
    return 0;
    //
  }
}
