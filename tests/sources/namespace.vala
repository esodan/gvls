using GLib;

namespace MyOwn {
  public class App {
    public string name { get; set; }
    public string prop1 { get; set; }
    public string prop2 { get; set; }
    public void method (string val) {}
    public void method1 (string val) {}
    public void method2 (string val) {}
    public class SubApp {
      public void m() {}
      public enum SubApps {
        ONE, TWO
      }
    }
  }

  public class App2 {
    public void callme () {}
  }

  public interface IfaceApp {
    public virtual void callme () {}
  }
  namespace SubMyOwn {
    public class Task : Object {
      public uint time { get; set; }
      public class SubTask : Object {
        public void m_subtask() {}
        public enum Tasks {
          COMPLEX, SIMPLE
        }
      }
    }
  }
}

namespace MyTwo {
  public interface IfaceCome {
    public abstract double total { get; set; }
  }
}

class Tycom {
  public int rock { get; set; }
}
