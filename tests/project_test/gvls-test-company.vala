/*
 * GVls Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gee;

public class VTest.Company : GLib.Object {
    public string name { get; set; }
    public string ceo { get; set; }
    public Gee.ArrayList<Country> countries { get; set; }

    public void main () {
        var c = new VTest.Company ();
        c.countries = new Gee.ArrayList<Country> ();
        string t = "";
        stdout.printf ("%s", t);
        var ct = new VTest.Country ();
        ct.name = "Mexico";
        var e = new VTest.Employee ();
        c.add (e);
        e.validate ();
    }
}
