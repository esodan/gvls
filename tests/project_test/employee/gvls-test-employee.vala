/*
 * GVls Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2019 <esodan@gmail.com>
 *
 * libgda is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libgda is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class VTest.Employee : GLib.Object {
    public string name { get; set; }
    public Salary salary { get; set; }
    public SocialSecurity security { get; set; }
    /**
     * Replace employee salary and increase it
     */
    public void replace_salary (Salary sal, float amount) {
        salary.amount *= amount;
    }
    /**
     * Takes a {@link VTest.Company} and check if is valid
     * for the employee.
     *
     * Also there is more information later
     * this time.
     */
    public async bool validate (bool full, VTest.Company company) {
        return true;
    }
}
